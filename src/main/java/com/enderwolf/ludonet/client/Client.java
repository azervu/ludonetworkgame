/**
 *
 */
package com.enderwolf.ludonet.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.alee.laf.WebLookAndFeel;
import com.enderwolf.ludonet.client.gui.game.LudoWindow;
import com.enderwolf.ludonet.client.gui.lobby.LobbyWindow;
import com.enderwolf.ludonet.client.gui.login.LoginListener;
import com.enderwolf.ludonet.client.gui.login.LoginWindow;
import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.shared.ConnectionLostListener;
import com.enderwolf.ludonet.shared.I18N;
import com.enderwolf.ludonet.shared.NetworkConnection;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.PlayerColor;

/**
 * 'Client' is the main controller of the client application. It *is* the
 * application's lifecycle, and performs actions on a macro scale.
 *
 * @author !Tulingen
 */
public class Client implements LoginListener, ConnectionLostListener {

    private static final Logger LOGGER = Logger.getGlobal();
    private static boolean DBG_AUTO_REGISTER = false;
    private static boolean DBG_AUTO_FIND_GAME = false;
    private static boolean DBG_AUTO_PLAY = false;
    private static String remoteHost;

    private LoginWindow loginWindow;
    private LobbyWindow gameWindow;
    private LudoWindow ludoWindow;
    private Player localPlayer;

    public static boolean getDBGAutoPlay() {
        return Client.DBG_AUTO_PLAY;
    }

    public static String getRemoteHost() {
        return Client.remoteHost;
    }

    /**
     * Program main entry point There are a couple of debug flags: "autoreg" =
     * automatic registration of new users. "autofind" = automatic start of game
     * searching.. "autoplay" = automatic playing of the games by a really basic
     * AI.
     *
     * @param args
     */
    public static void main(String[] args) {
        Client.LOGGER.setLevel(Level.ALL);

        try (InputStream is = Client.class
                .getResourceAsStream("/com/enderwolf/ludonet/shared/config/logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (final IOException ex) {
            Client.LOGGER.setLevel(Level.INFO);
            Client.LOGGER.log(Level.WARNING, "unable to load config file", ex);
        }

        for (final String arg : args) {
            if ("autoreg".equals(arg)) {
                Client.DBG_AUTO_REGISTER = true;
            }

            if ("autofind".equals(arg)) {
                Client.DBG_AUTO_FIND_GAME = true;
            }

            if ("autoplay".equals(arg)) {
                Client.DBG_AUTO_PLAY = true;
            }
        }

        WebLookAndFeel.install();

        Client.remoteHost = JOptionPane.showInputDialog("Remote Host", "localhost");
        new Client();
    }

    /**
     * Default constructor
     */
    public Client() {
        this.loginWindow = new LoginWindow();
        this.loginWindow.setLoginDelegate(this);
        this.loginWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        if (Client.DBG_AUTO_REGISTER) {
            final Random random = new Random(System.currentTimeMillis());

            String fakeName = "";

            for (int i = 0; i < 15; i++) {
                fakeName += random.nextInt(10);
            }

            this.onRegister(fakeName, "bullshitPassword");
        }
    }

    @Override
    public void onLogin(String userName, String password) {
        this.loginToServer(userName, password, false);
    }

    @Override
    public void onRegister(String userName, String password) {
        this.loginToServer(userName, password, true);

        if (!Client.DBG_AUTO_REGISTER) {
            JOptionPane.showMessageDialog(null, I18N.getString("Login.welcomeMessage"));
        }
    }

    @Override
    public void onPlayerDisconnected(Player player) {
        if (player != null && player != this.localPlayer) {
            return;
        }

        // Oh heavens, the connection has been lost! Close the lobby and game
        // windows (if they are
        // open), and re-open the login window.
        this.loginWindow = new LoginWindow();
        this.loginWindow.setLoginDelegate(this);
        this.loginWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        if (this.gameWindow != null) {
            this.closeWindow(this.gameWindow);
            this.gameWindow = null;
        }

        if (this.ludoWindow != null) {
            this.closeWindow(this.ludoWindow);
            this.ludoWindow = null;
        }
    }

    /**
     * Connect to the server with user-supplied credentials.
     *
     * @param userName
     *            The entered username.
     * @param password
     *            The password to send to the server.
     * @param register
     *            Whether or not to request a registration on the server. The
     *            user must already exist on the server if 'register' is false,
     *            and vice versa.
     */
    private void loginToServer(String userName, String password, boolean register) {
        final ClientNetworkConnection networkConnection;
        try {
            networkConnection = new ClientNetworkConnection(InetAddress.getByName(Client.remoteHost));
        } catch (final UnknownHostException e) {
            Client.LOGGER.log(Level.WARNING, "Login UnknownHostException:", e);
            this.loginFailed(I18N.getString("Login.unableToConnect"));
            Client.remoteHost = JOptionPane.showInputDialog(I18N.getString("Popup.remoteHost"), "localhost");
            return;
        }

        final Thread loginThread = new Thread() {
            @Override
            public void run() {
                networkConnection.login(userName, password, register);

                NetworkConnection.ConnectionStatus status;

                do {
                    try {
                        Thread.sleep(100);
                    } catch (final InterruptedException ie) {
                        Thread.currentThread().interrupt();
                    }

                    status = networkConnection.updateLogin();
                } while (status == NetworkConnection.ConnectionStatus.AUTHENTICATION_PENDING);

                final boolean success = status == NetworkConnection.ConnectionStatus.CONNECTION_ESTABLISHED;
                final int playerId = networkConnection.getPlayerId();

                // Invoke the login-callback on the main thread
                SwingUtilities.invokeLater(() -> {
                    if (success) {
                        final Player player = new Player(playerId, userName);
                        player.setNetworkConnection(networkConnection);
                        Client.this.loginSuccessful(player);
                    } else {
                        Client.this.loginFailed(I18N.getString("Login.loginFailedLong"));
                    }
                });
            }
        };

        // Start the login-thread
        loginThread.start();
    }

    private void loginSuccessful(Player player) {
        this.localPlayer = player;
        this.localPlayer.setConnectionLostListener(this);

        if (this.loginWindow != null) {
            this.closeWindow(this.loginWindow);
            this.loginWindow = null;
        }

        if (this.gameWindow == null) {
            this.gameWindow = new LobbyWindow(this.localPlayer);
            this.gameWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }

        this.mapMessageCallbacks();

        Client.LOGGER.log(Level.INFO, "Logged in as " + player.getUserName() + " (" + player.getPlayerId() + ")");

        new PlayerUpdateThread(this.localPlayer).start();

        if (Client.DBG_AUTO_FIND_GAME) {
            this.gameWindow.findGame();
        }
    }

    private void loginFailed(String message) {
        JOptionPane.showMessageDialog(null, message, I18N.getString("Login.loginFailed"),
                JOptionPane.INFORMATION_MESSAGE);
    }

    private void mapMessageCallbacks() {
        this.localPlayer.setMessageListener(Message.MessageType.GAME_PLAYER_JOIN,
                (p, m) -> this.onMessageGamePlayerJoined(m));
        this.localPlayer.setMessageListener(Message.MessageType.GAME_STARTING, (p, m) -> this.onMessageGameStarting(m));

        this.localPlayer.setMessageListener(Message.MessageType.GAME_PLAYER_LEFT, (p, m) -> {
            if (this.ludoWindow != null) {
                this.ludoWindow.onMessagePlayerLeft(m);
            } else {
                Client.LOGGER.warning("Message GAME_PLAYER_LEFT went unhandled. LudoWindow is not instantiated.");
            }
        });
    }

    private void onMessageGamePlayerJoined(Message message) {
        if (this.ludoWindow == null) {
            this.ludoWindow = new LudoWindow();
            this.ludoWindow.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

            this.ludoWindow.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    Client.this.ludoWindow = null;
                }
            });
        }

        final String playerName = message.getEntry(Message.EntryType.USER_NAME);
        final int playerId = Integer.valueOf(message.getEntry(Message.EntryType.PLAYER_ID));

        // Create the player object if it is a remote ID, use "localPlayer" if
        // this is the local player.
        boolean local;
        Player player;

        if (playerId != this.localPlayer.getPlayerId()) {
            player = new Player(playerId, playerName);
            local = false;
        } else {
            player = this.localPlayer;
            local = true;
        }

        final String colorString = message.getEntry(Message.EntryType.PLAYER_COLOR);
        player.setPlayerColor(PlayerColor.valueOf(colorString));

        this.ludoWindow.addPlayer(player, local);
    }

    private void onMessageGameStarting(Message message) {
        this.ludoWindow.startGame();
    }

    /**
     * Close a window peacefully.
     *
     * @param window
     *            The window to close.
     */
    private void closeWindow(JFrame window) {
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        window.setVisible(false);
        window.dispatchEvent(new WindowEvent(this.loginWindow, WindowEvent.WINDOW_CLOSING));
    }

    private class PlayerUpdateThread extends Thread {
        private final Player player;

        public PlayerUpdateThread(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            // Sleep VERY briefly when auto-playing, 1/4th of a second when
            // playing normally.
            final int sleepMs = (Client.DBG_AUTO_PLAY) ? 10 : 250;

            try {
                while (this.player.isConnectionGood()) {
                    this.player.update();
                    Thread.sleep(sleepMs);
                }
            } catch (final InterruptedException iex) {
                Client.LOGGER.info("Player update task aborting");
            }
        }
    }
}
