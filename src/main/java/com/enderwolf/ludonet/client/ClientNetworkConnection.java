package com.enderwolf.ludonet.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.NetworkConnection;

/**
 * The client side network interface. This class encapsulates the connection and
 * communication protocol for accessing the server.
 */
public class ClientNetworkConnection extends NetworkConnection {
    private static final Logger LOG = Logger.getGlobal();

    private final InetAddress serverAddress;
    private int loggedInPlayerId = 0;

    /**
     * This constructor takes an address and tries to set up a SSL connection to
     * it.
     *
     * @param hostIp
     *            the address of the host too connect to.
     */
    public ClientNetworkConnection(InetAddress hostIp) {
        super(true);
        ClientNetworkConnection.LOG.log(Level.INFO, "CLIENT: creating network connection");
        System.setProperty("javax.net.ssl.trustStore", "mySrvKeystore");
        this.serverAddress = hostIp;
        try {
            this.connectionStatus = ConnectionStatus.IDENTIFICATION_PENDING;
            this.socket = SocketFactory.getDefault().createSocket(this.serverAddress, 23456);
            this.loginSocket = (SSLSocket) ((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket(this.socket,
                    null, this.socket.getPort(), false);
            this.writer = new BufferedWriter(new OutputStreamWriter(this.loginSocket.getOutputStream()));
            this.reader = new BufferedReader(new InputStreamReader(this.loginSocket.getInputStream()));
        } catch (final UnknownHostException e) {
            ClientNetworkConnection.LOG.log(Level.WARNING, "Unknown host: " + this.serverAddress.toString(), e);
            this.close();
        } catch (final IOException e) {
            ClientNetworkConnection.LOG.log(Level.WARNING,
                    "Connection failed / refused: " + this.serverAddress.toString(), e);
            this.close();
        }
    }

    /**
     * This function initiates the login procedure, should only be called once.
     *
     * @param userName
     *            the user name
     * @param password
     *            the users unencrypted password
     * @param createUser
     *            if this is a new or returning user.
     */
    public void login(String userName, String password, boolean createUser) {
        this.localConnectionName = userName;
        if (this.connectionStatus == ConnectionStatus.IDENTIFICATION_PENDING) {
            Message msg;
            msg = new Message(MessageType.LOGIN, userName);
            msg.addEntry(EntryType.PASSWORD, password, true);
            msg.addEntry(EntryType.CREATE_USER, String.valueOf(createUser), false);
            this.transmitMessage(msg);
            this.connectionStatus = ConnectionStatus.AUTHENTICATION_PENDING;
        } else {
            ClientNetworkConnection.LOG.log(Level.WARNING,
                    "CLIENT: login atempted when not in IDENTIFICATION_PENDING status");
        }
    }

    /**
     * updates and returns the network connection login state. should be called
     * until it returns CONNECTION_ESTABLISHED or CONNECTION_CLOSED.
     *
     * @return the status of the connection.
     */
    @Override
    public ConnectionStatus updateLogin() {
        switch (this.connectionStatus) {
            case AUTHENTICATION_FAILED:
                break;

            case AUTHENTICATION_PENDING:
                final Message msg = this.receiveMessage();
                if (msg != null && (msg.getType() == MessageType.CONNECTION_STATUS)) {
                    this.connectionStatus = ConnectionStatus.valueOf(msg.getEntry(EntryType.CONNECTION_STATUS));
                    ClientNetworkConnection.LOG.log(Level.INFO, "connectionStatus " + this.connectionStatus.toString());
                    if (this.connectionStatus == ConnectionStatus.CONNECTION_ESTABLISHED) {
                        this.loggedInPlayerId = Integer.valueOf(msg.getEntry(EntryType.PLAYER_ID));

                        try {
                            this.loginSocket.close();
                            this.loginSocket = null;
                            this.writer.close();
                            this.reader.close();
                            this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
                            this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                        } catch (final IOException e) {
                            ClientNetworkConnection.LOG.log(Level.INFO, "Connection to server closed", e);
                            this.close();
                        }

                    } else if (this.connectionStatus == ConnectionStatus.CONNECTION_CLOSED
                            || this.connectionStatus == ConnectionStatus.AUTHENTICATION_FAILED) {
                        this.close();
                    }
                }
                break;

            case CONNECTION_CLOSED:
            case CONNECTION_ESTABLISHED:
            case IDENTIFICATION_PENDING:
                break;

            default:
                break;
        }
        return this.connectionStatus;
    }

    /**
     * Following a successful login or registration, the PlayerID of the logged
     * in player can be retrieved using this interface.
     *
     * @return The PlayerID of the last logged in player, 0 if noone has logged
     *         in on this object yet.
     */
    public int getPlayerId() {
        return this.loggedInPlayerId;
    }
}