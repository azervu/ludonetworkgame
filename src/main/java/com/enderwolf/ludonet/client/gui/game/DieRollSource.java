package com.enderwolf.ludonet.client.gui.game;

/**
 * Interface declaring methods for retrieving die-rolls.
 */
public interface DieRollSource {
    /**
     * Retrieve the next die-roll value.
     *
     * @return A random value in the range [1..6] on success, 0 if the source is
     *         exhausted.
     */
    int getNextDieRollValue();
}
