package com.enderwolf.ludonet.client.gui.game;

/**
 * GameInterfaceListener declares an interface to receive notifications when the
 * physical user has performed any of the game-actions available to him;
 * "rolling the dice" and moving his game pieces.
 */
public interface GameInterfaceListener {
    /**
     * Called when the local player has clicked the button to roll his die.
     */
    void onPlayerRollingDie(int rollValue);

    /**
     * Called when the local player has clicked on any tile on the map.
     *
     * @param tileIndex
     *            The index of the tile that was clicked.
     */
    void onPlayerClickedTile(int tileIndex);

    /**
     * Called when the player has no available moves.
     */
    void onPlayerGotNoMoves();
}
