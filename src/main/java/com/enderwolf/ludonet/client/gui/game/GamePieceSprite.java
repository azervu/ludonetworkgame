package com.enderwolf.ludonet.client.gui.game;

import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.enderwolf.ludonet.shared.GamePiece;
import com.enderwolf.ludonet.shared.GameTrail;

/**
 * CircleImage is used to draw the game pieces on the board.
 */
public class GamePieceSprite extends ImageComponent {
    private static final long serialVersionUID = 5973591822756585155L;

    /**
     * circle.png is found in the *.resources packgage. See ImageComponent for
     * details. The color of the game piece is inserted at the start of this
     * name, resulting in "RED" + CIRCLE_IMAGE, "BLUE" + CIRCLE_IMAGE, etc.
     */
    private static final String CIRCLE_IMAGE = "circle.png";

    private Dimension imageSize;
    private GamePiece gamePiece;

    /**
     * Construct a GamePieceSprite related to a GamePiece.
     *
     * @param gamePiece
     *            The GamePiece to relate this sprite to.
     */
    public GamePieceSprite(GamePiece gamePiece) {
        try {
            super.loadImage(gamePiece.getPlayerColor().toString() + GamePieceSprite.CIRCLE_IMAGE);
        } catch (final IOException ioe) {
            Logger.getGlobal().log(Level.WARNING, "CircleImage unable to load image " + GamePieceSprite.CIRCLE_IMAGE,
                    ioe);
            return;
        }

        final Image img = this.getImage();
        this.imageSize = new Dimension(img.getWidth(null), img.getHeight(null));
    }

    /**
     * Getter for the tile coordinate.
     *
     * @return The coordinate.
     */
    public GameTrail.Point getTileCoordinates() {
        final int position = this.gamePiece.getPosition();
        return GameTrail.getTileCoord(position);
    }

    @Override
    public Dimension getMinimumSize() {
        return this.imageSize;
    }

    @Override
    public Dimension getPreferredSize() {
        return this.imageSize;
    }

}
