package com.enderwolf.ludonet.client.gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * ImageComponent draws an image in it's area.
 */
public class ImageComponent extends JComponent {
    private static final long serialVersionUID = 3516323478441031899L;

    private Image image;

    /**
     * Construct an ImageComponent from an existing image.
     *
     * @param image
     *            An initialized image.
     */
    public ImageComponent(Image image) {
        this.setImage(image);
    }

    /**
     * Construct an ImageComponent from an image file contained in the
     * "com.enderwolf.ludonet.resources" package.
     *
     * @param fileName
     *            Name of a file in the "resources" package.
     * @throws IOException
     *             If the file cannot be opened, is not an image or other error.
     */
    public ImageComponent(String fileName) throws IOException {
        this.loadImage(fileName);
    }

    /**
     * Constructor to be used by subclasses for delayed initialization. Call
     * ImageComponent#loadImage asap.
     */
    protected ImageComponent() {

    }

    protected void loadImage(String fileName) throws IOException {
        final InputStream is = this.getClass().getResourceAsStream("/com/enderwolf/ludonet/resources/" + fileName);
        this.image = ImageIO.read(is);
    }

    protected void setImage(Image image) {
        this.image = image;
    }

    protected Image getImage() {
        return this.image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (this.image != null) {
            final Dimension size = this.getSize();
            g.setColor(Color.RED);
            g.drawImage(this.image, 0, 0, size.width, size.height, null);
        } else {
            super.paintComponent(g);
        }
    }
}
