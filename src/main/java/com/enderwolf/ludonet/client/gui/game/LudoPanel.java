package com.enderwolf.ludonet.client.gui.game;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.enderwolf.ludonet.shared.GamePiece;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.GameTrail;
import com.enderwolf.ludonet.shared.Player;

/**
 * Visual representation of the LUDO board.
 */
public class LudoPanel extends JPanel {
    private static final long serialVersionUID = -4883365548662612035L;

    private ImageComponent gameBoard;
    private List<TileButton> tileButtons;
    private final List<GamePieceSprite> circles;

    /**
     * LudoPanel default constructor. Multiple images are required by this
     * panel, and an IOException is thrown if any of the images fail to load.
     *
     * @param size
     *            The size this panel will take.
     * @throws IOException
     *             upon failure to load either of the required images.
     */
    public LudoPanel(Dimension size) throws IOException {
        this.circles = new ArrayList<>();

        this.setSize(size);
        this.setupUI();
    }

    /**
     * Setts the listener for the board tiles.
     *
     * @param listener
     *            The listener to give the tiles.
     */
    public void setGameInterfaceListener(GameInterfaceListener listener) {
        for (final TileButton t : this.tileButtons) {
            t.setGameInterfaceListener(listener);
        }
    }

    @Override
    public void setSize(Dimension size) {
        super.setSize(size);

        if (this.gameBoard != null) {
            this.gameBoard.setSize(size);
            this.gameBoard.setMinimumSize(size);
            this.gameBoard.setPreferredSize(size);
            this.gameBoard.setMaximumSize(size);
        }
    }

    /**
     * Getter for the tile size.
     *
     * @return The tile size.
     */
    public Dimension getTileSize() {
        final Dimension boardSize = this.gameBoard.getSize();
        final Dimension tileSize = new Dimension();

        // The board is 15x15 tiles. It's here assumed that there are no borders
        // along
        // any of the edges.
        tileSize.width = boardSize.width / 15;
        tileSize.height = boardSize.height / 15;

        return tileSize;
    }

    /**
     * Update the positions of the game pieces.
     */
    public void updateGamePieces(GameState gameState) {
        /*
         * Currently, the existing pieces are removed completely and recreated.
         * This is obviously sub-par, but it may prove to not impact the
         * performance in any significant manner. Time will show.
         */
        final Player[] players = gameState.getPlayers();

        for (final GamePieceSprite ci : this.circles) {
            ci.getParent().remove(ci);
        }

        this.circles.clear();

        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.CENTER;

        for (final Player p : players) {
            if (p == null) {
                continue;
            }

            for (int i = 0; i < 4; i++) {
                final GamePiece piece = p.getGamePiece(i);
                if (piece == null) {
                    continue;
                }

                final int position = piece.getPosition();
                final GamePieceSprite sprite = new GamePieceSprite(piece);
                this.circles.add(sprite);

                this.addTileComponent(sprite, position);
            }
        }

        this.gameBoard.revalidate();
        this.gameBoard.repaint();
    }

    /**
     * Highlight a specified tile.
     *
     * @param tileIndex
     *            The index of the tile to highlight.
     */
    public void highlightTile(int tileIndex) {
        try {
            final InputStream is = this.getClass()
                    .getResourceAsStream("/com/enderwolf/ludonet/resources/highlight.png");
            final Image image = ImageIO.read(is);
            this.tileButtons.get(tileIndex).setImage(image);
        } catch (final IOException ioe) {
            Logger.getGlobal().log(Level.WARNING, "Unable to load highlight.png", ioe);
        }

        this.gameBoard.revalidate();
        this.gameBoard.repaint();
    }

    /**
     * Remove all highlighting done by LudoPanel#highlightTile(I).
     */
    public void unhighlightAll() {
        for (final TileButton t : this.tileButtons) {
            t.setImage(null);
        }

        this.gameBoard.revalidate();
        this.gameBoard.repaint();
    }

    /**
     * Builds the graphical user interface.
     *
     * @throws IOException
     *             Upon failure to load either of the required images.
     */
    private void setupUI() throws IOException {
        final GridBagLayout gbl = new GridBagLayout();
        this.gameBoard = new ImageComponent("ludoboard.png");
        this.gameBoard.setLayout(gbl);
        this.add(this.gameBoard);

        // Force the size of gameBoard to be updated to "my" size.
        this.setSize(this.getSize());

        this.createTileButtons();

        // Forcefully override the dimensions of the grid-cells to match the
        // tile sizes.
        // This is likely to break oh so hard if "gameBoard" parents any objects
        // at grid-coordinates >= [15,15].
        final Dimension tileSize = this.getTileSize();
        gbl.rowHeights = new int[15];
        gbl.columnWidths = new int[15];

        for (int i = 0; i < 15; i++) {
            gbl.rowHeights[i] = tileSize.width;
            gbl.columnWidths[i] = tileSize.height;
        }
    }

    private void createTileButtons() {
        final Dimension tileSize = this.getTileSize();
        this.tileButtons = new ArrayList<>();

        final int count = GameTrail.getNumberOfTiles();
        for (int i = 0; i < count; i++) {
            final GameTrail.Point btnCoord = GameTrail.getTileCoord(i);

            // Create the button early (we need to know it's minimum size)
            final TileButton btn = new TileButton(i);
            this.tileButtons.add(btn);

            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = (int) btnCoord.getX();
            gbc.gridy = (int) btnCoord.getY();
            gbc.anchor = GridBagConstraints.CENTER;

            if (i >= GameTrail.FIRST_SPAWN_TILE && i <= GameTrail.LAST_SPAWN_TILE) {
                /*
                 * Spawn tiles lies between two tiles, and requires special
                 * treatment
                 */
                final Dimension minSize = btn.getMinimumSize();
                gbc.gridwidth = 2;
                gbc.gridheight = 2;
                gbc.fill = GridBagConstraints.NONE;
                gbc.ipadx = (tileSize.width) - minSize.width;
                gbc.ipady = (tileSize.height) - minSize.height;
            } else {
                gbc.fill = GridBagConstraints.BOTH;
            }

            this.gameBoard.add(btn, gbc);
        }
    }

    private void addTileComponent(Component component, int tileIndex) {
        final GameTrail.Point point = GameTrail.getTileCoord(tileIndex);

        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = (int) point.getX();
        gbc.gridy = (int) point.getY();

        if (tileIndex < GameTrail.FIRST_SPAWN_TILE + 16) {
            gbc.gridwidth = 2;
            gbc.gridheight = 2;
        } else {
            gbc.gridwidth = 1;
            gbc.gridheight = 1;
        }

        this.gameBoard.add(component, gbc);
    }
}
