package com.enderwolf.ludonet.client.gui.game;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.I18N;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.PlayerColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The window the LUDO games happens in.
 */
public class LudoWindow extends JFrame implements MoveListener, DieRollSource {
    private static final long serialVersionUID = -2656927744524431345L;
    private static final Logger LOGGER = Logger.getGlobal();

    private static final ImageIcon ludoIcon;

    /**
     * The size of the game board. The width must match the height.
     */
    private static final Dimension GAME_BOARD_SIZE = new Dimension(600, 600);

    private LudoPanel ludoPanel;
    private List<PlayerGamePanel> playerPanels;
    private final Player[] players = new Player[4];
    private Player localPlayer;
    private GameState gameState;
    private MoveController moveController;
    private final List<Integer> dieRolls = new ArrayList<>();

    /**
     * Default constructor
     */
    public LudoWindow() {
        super();

        if (LudoWindow.GAME_BOARD_SIZE.width != LudoWindow.GAME_BOARD_SIZE.height) {
            LudoWindow.LOGGER.log(Level.WARNING, "The size of the game board is rectangular. Change "
                    + "LudoWindow#GAME_BOARD_SIZE's values so it's a square.");
        }

        this.setIconImage(LudoWindow.ludoIcon.getImage());
        this.setTitle(I18N.getString("Window.gameTitle"));

        this.setupUI();
        this.pack();
        this.setMinimumSize(this.getSize());
        this.setMaximumSize(this.getSize());
        this.setVisible(true);

        // Add self as a WindowAdapter
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // If a localPlayer exists, flag him as "not in a game"
                if (LudoWindow.this.localPlayer != null) {
                    LudoWindow.this.localPlayer.testAndSetInGame(false);
                }
            }
        });
    }

    /**
     * Add a Player object to the game session. If a Player object with the same
     * PlayerColor as 'player' already exists, the old instance is removed under
     * the assumption that whoever called this method is in direct contact with
     * the server.
     *
     * @param player
     *            The new Player to add to the game.
     * @param isLocal
     *            Indicator of this Player being the local user or a remote
     *            player.
     */
    public void addPlayer(Player player, boolean isLocal) {
        final int idx = player.getPlayerColor().getValue();
        if (idx < 0) {
            return;
        }

        this.players[idx] = player;
        this.playerPanels.get(idx).setPlayer(player, isLocal);

        if (isLocal) {
            this.localPlayer = player;

            // Map Message callbacks. All messages arrive in Player, but must be
            // handled externally.
            // Let's be the external the Player deserves.
            player.setMessageListener(Message.MessageType.GAME_MOVE, (p, m) -> this.onMessageMove(m));
            player.setMessageListener(Message.MessageType.GAME_ACTIVE_PLAYER, (p, m) -> this.onMessageActivePlayer(m));
            player.setMessageListener(Message.MessageType.GAME_OVER, (p, m) -> this.onMessageGameOver(m));
        }
    }

    /**
     * Message handler method for GAME_PLAYER_LEFT. This message is registered
     * in Client, but dispatched to the LudoWindow (if it exists).
     *
     * @param msg
     *            The message to handle.
     */
    public void onMessagePlayerLeft(Message msg) {
        final int playerId = Integer.valueOf(msg.getEntry(Message.EntryType.PLAYER_ID));
        final Player player = this.findPlayer(playerId);

        if (player == null) {
            Logger.getGlobal().log(Level.WARNING,
                    "LudoWindow#onMessagePlayerLeft: Player with ID " + playerId + " not found!");
            return;
        }

        final int idx = this.findPlayerIndex(playerId);

        if (this.gameState != null) {
            // We are mid-game. Prompt the user accordingly.
            this.gameState.playerLeft(player.getPlayerColor());
            this.playerPanels.get(idx).onPlayerDisconnected(I18N.getString("Ludo.playerDC"));
        } else {
            // We are waiting for more players, and one of the guys waiting with
            // us left.
            this.playerPanels.get(idx).onPlayerDisconnected(I18N.getString("Ludo.waitingForConnection"));
        }

        Logger.getGlobal().log(Level.INFO, "Player " + player.getUserName() + " left the session.");
    }

    /**
     * Starts the game.
     */
    public void startGame() {
        // There is no point in flagging "inGame" for any of the remote players,
        // they are only contained
        // inside LudoWindow anyway.
        this.localPlayer.testAndSetInGame(true);

        this.gameState = new GameState(this.players);
        this.moveController = new MoveController(this, this.ludoPanel, this.localPlayer, this.gameState);
        this.ludoPanel.setGameInterfaceListener(this.moveController);

        for (final PlayerGamePanel pgp : this.playerPanels) {
            pgp.setGameInterfaceListener(this.moveController);
            pgp.setGameState(this.gameState);
        }

        for (int i=0; i<4; i++) {
            if (this.players[i] == null) {
                this.playerPanels.get(i).onPlayerDisconnected("");
            }
        }

        this.updateLudoPanel();
    }

    /**
     * Updates LudoPanel. LudoPanel moves the GamePieces to their correct
     * positions.
     */
    public void updateLudoPanel() {
        SwingUtilities.invokeLater(() -> this.ludoPanel.updateGamePieces(this.gameState));
    }

    @Override
    public void onPlayerIssuedMoveCommand(Player player, int pieceIndex, int moves) {
        if (pieceIndex >= 0 && moves >= 0) {
            final Message msg = new Message(Message.MessageType.GAME_MOVE);

            msg.addEntry(Message.EntryType.PLAYER_ID, player.getPlayerId());
            msg.addEntry(Message.EntryType.NUM_MOVES, moves);
            msg.addEntry(Message.EntryType.GAME_PIECE, pieceIndex);

            player.sendMessage(msg);
        }

        if (!this.dieRolls.isEmpty()) {
            // Another roll for this guy
            this.playerPanels.get(this.localPlayer.getPlayerColor().getValue()).setActiveTurn();
        } else {
            final Message msg = new Message(Message.MessageType.GAME_TURN_OVER);
            player.sendMessage(msg);
            this.moveController.setActive(false);
        }
    }

    @Override
    public int getNextDieRollValue() {
        int value = 0;

        if (!this.dieRolls.isEmpty()) {
            value = this.dieRolls.get(0);
            this.dieRolls.remove(0);
        }

        return value;
    }

    private void onMessageMove(Message msg) {
        final int playerId = Integer.valueOf(msg.getEntry(Message.EntryType.PLAYER_ID));
        final int moves = Integer.valueOf(msg.getEntry(Message.EntryType.NUM_MOVES));
        final int pieceIdx = Integer.valueOf(msg.getEntry(Message.EntryType.GAME_PIECE));

        final Player player = this.findPlayer(playerId);
        if (player == null) {
            LudoWindow.LOGGER.warning("LudoWindow#onMessageMove: Player with ID " + playerId + " not found!");
            return;
        }

        final String moveDesc = String.format("Player %s move piece %d %d times", playerId, pieceIdx, moves);

        if (!this.gameState.performMove(player, pieceIdx, moves)) {
            LudoWindow.LOGGER.warning("LudoWindow#onMessageMove: Attempted illegal move - " + moveDesc);
            return;
        }

        this.updateLudoPanel();

        LudoWindow.LOGGER.info("LudoWindow#onMessageMove: " + moveDesc);
    }

    private void onMessageActivePlayer(Message msg) {
        final int playerId = Integer.valueOf(msg.getEntry(Message.EntryType.PLAYER_ID));
        final int idx = this.findPlayerIndex(playerId);

        if (idx < 0) {
            LudoWindow.LOGGER.warning("LudoWindow#onMessageActivePlayer: Unable to find player " + playerId);
            return;
        }

        this.dieRolls.clear();
        this.gameState.setActivePlayer(this.players[idx].getPlayerColor());

        final boolean isLocal = playerId == this.localPlayer.getPlayerId();
        this.moveController.setActive(isLocal);

        if (isLocal) {
            final String[] dice = msg.getArray(Message.EntryType.NUM_MOVES);
            for (final String d : dice) {
                this.dieRolls.add(Integer.valueOf(d));
            }

            // The maximum number of turns that can be taken sequentially is 3,
            // given that the first two are 6.
            int turns = 1;
            if (this.dieRolls.get(0) == 6) {
                turns++;
                if (this.dieRolls.get(1) == 6) {
                    turns++;
                }
            }

            while (this.dieRolls.size() > turns) {
                this.dieRolls.remove(this.dieRolls.size() - 1);
            }
        }

        for (int i = 0; i < 4; i++) {
            if (idx == i) {
                this.playerPanels.get(i).setActiveTurn();
            } else {
                this.playerPanels.get(i).setInactiveTurn();
            }
        }
    }

    private void onMessageGameOver(Message msg) {
        final int playerId = Integer.valueOf(msg.getEntry(Message.EntryType.PLAYER_ID));
        final int idx = this.findPlayerIndex(playerId);

        if (idx < 0) {
            LudoWindow.LOGGER.warning("LudoWindow#onMessageGameOver: Unable to locate winner " + playerId);
            return;
        }

        for (int i = 0; i < 4; i++) {
            if (i == idx) {
                this.playerPanels.get(i).setWinner();
            } else {
                this.playerPanels.get(i).setLoser();
            }
        }

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private Player findPlayer(int playerId) {
        for (final Player p : this.players) {
            if (p != null && p.getPlayerId() == playerId) {
                return p;
            }
        }

        return null;
    }

    private int findPlayerIndex(int playerId) {
        for (int i = 0; i < 4; i++) {
            if (this.players[i] != null && this.players[i].getPlayerId() == playerId) {
                return i;
            }
        }

        return -1;
    }

    private void displayLoadErrorDialog() {
        final String message = I18N.getString("Ludo.failedToLoad");
        final String title = I18N.getString("error");
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    private void setupUI() {
        try {
            this.setLayout(new GridBagLayout());
            GridBagConstraints gbc;

            this.ludoPanel = new LudoPanel(LudoWindow.GAME_BOARD_SIZE);
            gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.NORTHWEST;
            gbc.fill = GridBagConstraints.BOTH;
            this.add(this.ludoPanel, gbc);
        } catch (final IOException e) {
            LudoWindow.LOGGER.log(Level.WARNING, "Connection closed", e);
            this.displayLoadErrorDialog();
        }

        this.createPlayerGamePanels();
    }

    private void createPlayerGamePanels() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;

        final JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        this.add(panel, gbc);

        this.playerPanels = new ArrayList<>();

        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.VERTICAL;

        for (int i = 0; i < 4; i++) {
            final PlayerGamePanel gp = new PlayerGamePanel(this, LudoWindow.GAME_BOARD_SIZE.height / 4);
            panel.add(gp, gbc);
            this.playerPanels.add(gp);

            gp.setPlayerColor(PlayerColor.values()[i]);

            gbc.gridy++;
        }
    }

    static {

        final URL lobbyURL = LudoWindow.class.getResource("/com/enderwolf/ludonet/resources/LUDO.png");
        ludoIcon = new ImageIcon(lobbyURL, I18N.getString("Icon.LUDO"));
    }
}
