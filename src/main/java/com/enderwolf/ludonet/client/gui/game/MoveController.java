package com.enderwolf.ludonet.client.gui.game;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.enderwolf.ludonet.client.Client;
import com.enderwolf.ludonet.shared.GamePiece;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.GameTrail;
import com.enderwolf.ludonet.shared.Player;

/**
 * MoveController governs potential moves attempted by the player. The class
 * listens for clicks on the board tiles, and considers whether or not this is a
 * valid move. As soon as a valid movement sequence has been entered, the
 * MoveController notifies its MoveListener.
 */
public class MoveController implements GameInterfaceListener {
    private static final Logger LOGGER = Logger.getGlobal();

    private final MoveListener moveListener;
    private final LudoPanel ludoPanel;
    private final Player player;
    private final GameState gameState;

    /*
     * The index of the selected tile clicked by the player. Holds -1 if
     * undefined.
     */
    private int selectedPieceIdx;

    /*
     * All user interaction is ignored and highlighting removed if the
     * controller is inactive.
     */
    private boolean active;

    /* The die roll of the player. */
    private int dieRoll;

    /**
     * Default constructor.
     *
     * @param listener
     *            The delegate to receive MoveListener-updates.
     * @param panel
     *            The LudoPanel. MoveController will highlight the moves on the
     *            game board.
     * @param player
     *            The local player.
     */
    public MoveController(MoveListener listener, LudoPanel panel, Player player, GameState gameState) {
        this.moveListener = listener;
        this.ludoPanel = panel;
        this.player = player;
        this.gameState = gameState;

        this.reset();
    }

    /**
     * Set the MoveController as active or inactive. When active, user
     * interaction with the game bord is checked for movement-sequences, and the
     * game-board is highlighted. In inactive state, all interaction is ignored,
     * and the board is not highlighter.
     */
    public void setActive(boolean flag) {
        if (flag == this.active) {
            return;
        }

        this.active = flag;

        if (this.active) {
            this.dieRoll = 0;
            this.selectedPieceIdx = -1;
        } else {
            this.ludoPanel.unhighlightAll();
        }
    }

    @Override
    public void onPlayerRollingDie(int rollValue) {
        if (!this.active) {
            return;
        }

        this.dieRoll = rollValue;

        this.highlightMovablePieces();

        if (Client.getDBGAutoPlay()) {
            boolean anyMove = false;

            // Shuffle the order of the pieces and move the first movable piece.
            final List<Integer> pieces = Arrays.asList(0, 1, 2, 3);
            Collections.shuffle(pieces);

            for (final int i : pieces) {
                final GamePiece piece = this.player.getGamePiece(i);
                if (piece.isFinished()) {
                    continue;
                }

                if (this.gameState.isMoveLegal(this.player, i, rollValue)) {
                    this.moveListener.onPlayerIssuedMoveCommand(this.player, i, rollValue);
                    anyMove = true;
                    break;
                }
            }

            if (!anyMove) {
                this.moveListener.onPlayerIssuedMoveCommand(this.player, -1, -1);
            }

            this.reset();
        }
    }

    @Override
    public void onPlayerClickedTile(int tileIndex) {
        if (!this.active) {
            return;
        }

        if (this.selectedPieceIdx < 0) {
            final int pieceIdx = this.getPieceIndexAtTile(tileIndex);
            final int dest = this.getDestinationTile(pieceIdx, this.dieRoll);

            if (pieceIdx >= 0 && dest >= 0) {
                this.selectedPieceIdx = pieceIdx;

                this.ludoPanel.unhighlightAll();
                this.ludoPanel.highlightTile(this.player.getGamePiece(this.selectedPieceIdx).getPosition());
                this.ludoPanel.highlightTile(dest);
            }
        } else {
            final int dest = this.getDestinationTile(this.selectedPieceIdx, this.dieRoll);

            if (tileIndex == dest) {
                if (!this.gameState.isMoveLegal(this.player, this.selectedPieceIdx, this.dieRoll)) {
                    MoveController.LOGGER.warning("MoveController#onPlayerClickedTile: Attempted move is illegal. "
                            + "Will still attempt to perform move, but it will most likely fail.");
                }

                this.moveListener.onPlayerIssuedMoveCommand(this.player, this.selectedPieceIdx, this.dieRoll);
                this.reset();
            } else if (this.getPieceIndexAtTile(tileIndex) >= 0) {
                // A piece was already selected, and the user selected another
                // one. Recurse!
                this.selectedPieceIdx = -1;
                this.onPlayerClickedTile(tileIndex);
            } else {
                // The user clicked something random. Unhighlight all and
                // highlight the movables.
                this.ludoPanel.unhighlightAll();
                this.highlightMovablePieces();
            }
        }
    }

    @Override
    public void onPlayerGotNoMoves() {
        this.moveListener.onPlayerIssuedMoveCommand(this.player, -1, -1);
        this.reset();
    }

    /**
     * Attempt to find a tile belonging to "player" at the tile.
     *
     * @param tile
     *            The tile position to check.
     * @return The index of a tile belonging to "player", if one exists at this
     *         tile, -1 otherwise.
     */
    private int getPieceIndexAtTile(int tile) {
        for (int i = 0; i < 4; i++) {
            final GamePiece gp = this.player.getGamePiece(i);

            if (gp.getPosition() == tile) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Get the destination of the GamePiece if it were to be moved "moves" tiles
     * ahead.
     *
     * @param pieceIdx
     *            The index of the game piece to move.
     * @param moves
     *            The number of tiles to move said tile.
     * @return The destination of the game piece resulting in such a move, -1 if
     *         the piece cannot be moved for any reason.
     */
    private int getDestinationTile(int pieceIdx, int moves) {
        if (pieceIdx < 0 || pieceIdx >= 4) {
            return -1;
        }

        final GamePiece piece = this.player.getGamePiece(pieceIdx);

        if (piece.isFinished()) {
            return -1;
        }

        if (!this.gameState.isMoveLegal(this.player, pieceIdx, moves)) {
            return -1;
        }

        final GameTrail.Trail trail = GameTrail.getPlayerTrail(piece.getPlayerColor());
        return trail.getNextPosition(piece.getPosition(), moves);
    }

    private void highlightMovablePieces() {
        for (int i = 0; i < 4; i++) {
            // Only highlight tiles with valid moves
            if (!this.gameState.isMoveLegal(this.player, i, this.dieRoll)) {
                continue;
            }

            if (this.getDestinationTile(i, this.dieRoll) > 0) {
                final GamePiece gp = this.player.getGamePiece(i);
                final int tilePos = gp.getPosition();

                this.ludoPanel.highlightTile(tilePos);
            }
        }
    }

    private void reset() {
        this.ludoPanel.unhighlightAll();
        this.selectedPieceIdx = -1;
        this.dieRoll = 0;
    }
}
