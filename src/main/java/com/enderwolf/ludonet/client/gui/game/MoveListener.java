package com.enderwolf.ludonet.client.gui.game;

import com.enderwolf.ludonet.shared.Player;

/**
 * MoveListener declares an interface to be notified when the user has issued a
 * command to move his game piece. The callback is NOT an indicator that the
 * move has actually been performed, as the move is not executed locally until
 * the server tells us that the move did in fact happen.
 */
public interface MoveListener {
    /**
     * Called when the user has issued a "move my game piece please" command.
     *
     * @param player
     *            The Player-instance that was told to move.
     * @param pieceIndex
     *            The index of the game-piece (range [0..3]) that was issued a
     *            move command on. Holds -1 if no move was available, the
     *            listener must react accordingly.
     * @param moves
     *            The number of tiles the player wants to move. Holds -1 if no
     *            move was available, the listener must react accordingly.
     */
    void onPlayerIssuedMoveCommand(Player player, int pieceIndex, int moves);
}
