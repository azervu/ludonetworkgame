package com.enderwolf.ludonet.client.gui.game;

import com.enderwolf.ludonet.client.Client;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.I18N;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.PlayerColor;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

/**
 * PlayerGamePanel displays very basic information on the side of the
 * LudoWindow.
 */
public class PlayerGamePanel extends JPanel {
    private static final long serialVersionUID = 5541741040886633053L;
    private static final Logger LOGGER = Logger.getGlobal();

    private Player player;
    private boolean isLocal;
    private final DieRollSource dieRollSource;
    private GameInterfaceListener interfaceListener;
    private GameState gameState;

    private final Dimension prefSize;
    private JLabel titleLabel;
    private JLabel descLabel;
    private JLabel rollLabel;
    private JButton rollButton;
    private JButton noMovesButton;

    /**
     * Default constructor.
     *
     * @param dieSource
     *            The source of the die-rolls.
     * @param height
     *            The height this panel should take (in pixels).
     */
    public PlayerGamePanel(DieRollSource dieSource, int height) {
        this.dieRollSource = dieSource;

        this.prefSize = new Dimension(200, height);
        this.createUI();

        // We are inactive until told otherwise.
        this.setInactiveTurn();
    }

    /**
     * Set the GameInterfaceListener.
     *
     * @param listener
     *            The listener that will be notified upon game actions.
     */
    public void setGameInterfaceListener(GameInterfaceListener listener) {
        this.interfaceListener = listener;
    }

    /**
     * Set the GameState of this PlayerGamePanel. The game state is required to
     * check if the player has any valid moves to take, given a certain die roll
     * value.
     *
     * @param gameState
     *            The GameState of this session.
     */
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Set the Player this PlayerGamePanel represents.
     *
     * @param player
     *            The Player instance to represent.
     * @param isLocal
     *            If this Player is local or remote.
     */
    public void setPlayer(Player player, boolean isLocal) {
        this.player = player;
        this.isLocal = isLocal;

        this.titleLabel.setText(player.getUserName());

        this.setPlayerColor(player.getPlayerColor());
    }

    /**
     * Called when the associated player departs. The external is responsible
     * for coming up with a good message to prompt the user with when this
     * happens.
     *
     * @param labelText
     *            The new text to display that will explain what happened to the
     *            previous guy.
     */
    public void onPlayerDisconnected(String labelText) {
        this.titleLabel.setText(labelText);
        this.setBackground(Color.LIGHT_GRAY);
    }

    /**
     * Set the background color and style of this panel.
     *
     * @param color
     *            The color to use.
     */
    public void setPlayerColor(PlayerColor color) {
        this.setBackground(color.getBrightColor());
    }

    /**
     * It's this Player-instance's turn to roll and make a move.
     */
    public void setActiveTurn() {
        if (this.isLocal) {
            this.descLabel.setText(I18N.getString("Ludo.yourTurn"));
            this.rollButton.setVisible(true);
            this.rollButton.setEnabled(true);

            if (Client.getDBGAutoPlay()) {
                this.rollButton.doClick();

                if (this.noMovesButton.isVisible()) {
                    this.noMovesButton.doClick();
                }
            }
        } else {
            this.descLabel.setText(I18N.getString("Ludo.waitingForMove"));
        }

        this.noMovesButton.setVisible(false);
        this.descLabel.setVisible(true);
    }

    /**
     * It's no longer this Player-instance's turn to roll.
     */
    public void setInactiveTurn() {
        this.descLabel.setVisible(false);
        this.rollButton.setVisible(false);
        this.rollLabel.setVisible(false);
        this.noMovesButton.setVisible(false);
    }

    /**
     * The player represented by this panel won.
     */
    public void setWinner() {
        this.setInactiveTurn();
        this.descLabel.setText(I18N.getString("Ludo.winner"));
        this.descLabel.setVisible(true);
    }

    /**
     * The player represented by this panel did not win.
     */
    public void setLoser() {
        this.setInactiveTurn();
        this.descLabel.setText(I18N.getString("Ludo.loser"));
        this.descLabel.setVisible(true);
    }

    @Override
    public Dimension getMinimumSize() {
        return this.prefSize;
    }

    @Override
    public Dimension getPreferredSize() {
        return this.prefSize;
    }

    private void createUI() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.titleLabel = new JLabel();
        final Font f = this.titleLabel.getFont();
        this.titleLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
        this.titleLabel.setText(I18N.getString("Ludo.waitingForConnection"));
        this.add(this.titleLabel);

        this.rollButton = new JButton();
        this.rollButton.setText(I18N.getString("Ludo.roll"));
        this.rollButton.addActionListener(e -> this.onRollButtonClicked());

        this.add(this.rollButton);

        this.descLabel = new JLabel();
        this.add(this.descLabel);

        this.rollLabel = new JLabel();
        this.add(this.rollLabel);

        this.noMovesButton = new JButton();
        this.noMovesButton.setText(I18N.getString("Ludo.noMoves"));
        this.noMovesButton.addActionListener(e -> {
            this.interfaceListener.onPlayerGotNoMoves();
        });
        this.add(this.noMovesButton);
        this.noMovesButton.setVisible(false);
    }

    private void onRollButtonClicked() {
        if (this.interfaceListener == null) {
            PlayerGamePanel.LOGGER.warning("PlayerGamePanel has no GameInterfaceListener!");
            return;
        }

        final int die = this.dieRollSource.getNextDieRollValue();
        if (die > 0) {
            if (!this.gameState.anyMoveLegal(this.player, die)) {
                this.noMovesButton.setVisible(true);
            } else {
                this.interfaceListener.onPlayerRollingDie(die);
            }

            this.rollButton.setEnabled(false);
            this.rollLabel.setText(I18N.getString("Ludo.rolled") + " " + die);
            this.rollLabel.setVisible(true);
        } else {
            PlayerGamePanel.LOGGER.warning("PlayerGamePanel#{rollButtonLambda}: die roll is 0");
        }
    }
}
