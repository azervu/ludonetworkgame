package com.enderwolf.ludonet.client.gui.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;

import com.enderwolf.ludonet.shared.GameTrail;

/**
 * Buttons representing squares on the LUDO board.
 */
public class TileButton extends JButton {
    private static final long serialVersionUID = 3085292304219373300L;
    private static final Logger LOGGER = Logger.getGlobal();

    private final int tileIndex;
    private GameInterfaceListener interfaceListener;
    private Image image;

    /**
     * Default constructor
     *
     * @param tileIndex
     *            The index to represent on the board.
     */
    public TileButton(int tileIndex) {
        this.tileIndex = tileIndex;

        final GameTrail.Point tileCoord = GameTrail.getTileCoord(this.tileIndex);
        if (tileCoord == null) {
            TileButton.LOGGER.log(Level.INFO, "TileButton created with bad tile index");
            return;
        }

        this.addActionListener(e -> this.interfaceListener.onPlayerClickedTile(tileIndex));
    }

    public void setGameInterfaceListener(GameInterfaceListener listener) {
        this.interfaceListener = listener;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void paint(Graphics g) {
        if (this.image != null) {
            g.drawImage(this.image, 0, 0, 40, 40, new Color(0, 0, 0, 0), null);
        }
    }
}
