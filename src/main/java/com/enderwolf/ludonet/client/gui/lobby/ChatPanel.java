/**
 *
 */
package com.enderwolf.ludonet.client.gui.lobby;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.enderwolf.ludonet.client.lobby.LobbyModel;
import com.enderwolf.ludonet.shared.I18N;

/**
 * Visual Representation of the entire chat area.
 *
 * @author !Tulingen
 */
public class ChatPanel extends JPanel {
    private static final long serialVersionUID = -3275320544535128972L;
    private static final ImageIcon exitIcon;

    /**
     * Default constructor.
     *
     * @param chatModel
     *            The model for the lobby.
     */
    public ChatPanel(LobbyModel chatModel) {
        super();

        this.setLayout(new BorderLayout());

        final JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        this.add(tabbedPane, BorderLayout.CENTER);

        final JPanel lowerPanel = new JPanel();
        lowerPanel.setMinimumSize(new Dimension(200, 24));
        lowerPanel.setMaximumSize(new Dimension(300, 24));

        this.add(lowerPanel, BorderLayout.SOUTH);
        lowerPanel.setLayout(new BoxLayout(lowerPanel, BoxLayout.X_AXIS));

        final JLabel userName = new JLabel(chatModel.getPlayer().getUserName());
        lowerPanel.add(userName);
        
        final JButton sendButton = new JButton(I18N.getString("Chat.send"));
        sendButton.setBorderPainted(false);
        lowerPanel.add(sendButton);

        final JTextField chatField = new JTextField();
        lowerPanel.add(chatField);
        chatField.setColumns(10);

        final JButton exitRoomButton = new JButton(I18N.getString("Chat.exitRoom"));
        exitRoomButton.setIcon(ChatPanel.exitIcon);
        exitRoomButton.setBorderPainted(false);
        lowerPanel.add(exitRoomButton);

        chatModel.setTabbedPane(tabbedPane);

        sendButton.addActionListener(e -> {
            chatModel.sendMessage(chatField.getText());
            chatField.setText("");
        });

        chatField.addActionListener(e -> {
            chatModel.sendMessage(chatField.getText());
            chatField.setText("");
        });

        exitRoomButton.addActionListener(e -> chatModel.leaveRoom());
    }

    static {
        final URL addURL = ChatPanel.class.getResource("/com/enderwolf/ludonet/resources/CLOSE.png");
        exitIcon = new ImageIcon(addURL, I18N.getString("Icon.CLOSE"));
    }
}
