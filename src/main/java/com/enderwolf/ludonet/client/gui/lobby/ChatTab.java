/**
 *
 */
package com.enderwolf.ludonet.client.gui.lobby;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.enderwolf.ludonet.client.lobby.ChatRoomModel;
import com.enderwolf.ludonet.client.lobby.PlayerRefrence;

/**
 * The visual component of a chat room
 *
 * @author !Tulingen
 */
public class ChatTab extends JPanel {
    private static final long serialVersionUID = -7568017345182140822L;

    private final ChatRoomModel model;
    private final JTextArea textArea;
    private final JList<PlayerRefrence> userList;

    /**
     * Default constructor
     *
     * @param model
     *            The model to use.
     */
    public ChatTab(ChatRoomModel model) {
        super();

        this.setLayout(new BorderLayout(3, 3));

        this.model = model;
        this.textArea = new JTextArea(this.model.getDocument());
        this.textArea.setEditable(false);

        this.add(new JScrollPane(this.textArea), BorderLayout.CENTER);

        this.userList = new JList<>(model);
        this.add(new JScrollPane(this.userList), BorderLayout.EAST);
    }

    /**
     * Gets the ChatRoomModel
     *
     * @return The ChatRoomModel
     */
    public ChatRoomModel getModel() {
        return this.model;
    }
}
