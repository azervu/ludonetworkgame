/**
 *
 */
package com.enderwolf.ludonet.client.gui.lobby;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;

import com.enderwolf.ludonet.client.lobby.ChatRoom;
import com.enderwolf.ludonet.client.lobby.FriendListModel;
import com.enderwolf.ludonet.client.lobby.LobbyModel;
import com.enderwolf.ludonet.client.lobby.PlayerRefrence;
import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.I18N;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.Scoreboard;
import com.enderwolf.ludonet.shared.Scoreboard.DeserializeException;
import com.enderwolf.ludonet.shared.Scoreboard.Entry;
import com.enderwolf.ludonet.shared.Scoreboard.SCOREBOARD_TYPE;

/**
 * The visual part of the main window. This window contains chat, chat rooms,
 * friend list and menu.
 *
 * @author !Tulingen
 */
public class LobbyWindow extends JFrame {
    private static final long serialVersionUID = 5864139201865312225L;
    private static final Logger LOGGER = Logger.getGlobal();

    private static final ImageIcon aboutIcon;
    private static final ImageIcon addIcon;
    private static final ImageIcon removeIcon;
    private static final ImageIcon inviteIcon;
    private static final ImageIcon challengeIcon;
    private static final ImageIcon scoreIcon;

    private static final String rules;

    private Player player;

    /**
     * Default constructor.
     *
     * @param player
     *            The player object to attach listeners to.
     */
    public LobbyWindow(Player player) {
        this.player = player;

        final DefaultListModel<ChatRoom> lobbyListModel = new DefaultListModel<>();
        final FriendListModel friendListModel = new FriendListModel(this.player);
        final LobbyModel lobbyModel = new LobbyModel(this.player, lobbyListModel);

        this.setUpMenu(lobbyModel);
        this.setIconImage(LobbyWindow.challengeIcon.getImage());
        this.setTitle(I18N.getString("Window.lobbyTitle"));

        final JSplitPane splitPane = new JSplitPane();
        this.add(splitPane, BorderLayout.CENTER);

        final ChatPanel chatPanel = new ChatPanel(lobbyModel);
        splitPane.setRightComponent(chatPanel);

        final JSplitPane leftSideBar = new JSplitPane();
        leftSideBar.setMinimumSize(new Dimension(240, 300));
        leftSideBar.setResizeWeight(0.5);
        leftSideBar.setPreferredSize(new Dimension(240, 300));
        splitPane.setLeftComponent(leftSideBar);
        leftSideBar.setOrientation(JSplitPane.VERTICAL_SPLIT);

        final JPanel friendArea = new JPanel(new BorderLayout());
        leftSideBar.setRightComponent(friendArea);

        final JPanel friendButtons = new JPanel();
        final JButton addFriend = new JButton(I18N.getString("FriendPanel.add"), LobbyWindow.addIcon);
        final JButton removeFriend = new JButton(I18N.getString("FriendPanel.remove"), LobbyWindow.removeIcon);
        final JButton inviteFriend = new JButton(I18N.getString("FriendPanel.invite"), LobbyWindow.inviteIcon);

        friendButtons.add(addFriend);
        friendButtons.add(removeFriend);
        friendButtons.add(inviteFriend);

        friendArea.add(friendButtons, BorderLayout.SOUTH);

        final JList<PlayerRefrence> friendList = new JList<>(friendListModel);
        friendArea.add(new JScrollPane(friendList), BorderLayout.CENTER);
        friendList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        addFriend.addActionListener(e -> {
            final String result = JOptionPane.showInputDialog(I18N.getString("FriendDialog.addMessage"));

            if ("".equals(result)) {
                JOptionPane.showMessageDialog(null, I18N.getString("FriendDialog.invalidNameMessage"),
                        I18N.getString("FriendDialog.invalidNameWindowTitle"), JOptionPane.ERROR_MESSAGE);
                return;
            }

            final Message message = new Message(MessageType.ADD_FRIEND);
            message.addEntry(EntryType.USER_NAME, result);

            player.sendMessage(message);
        });

        removeFriend.addActionListener(e -> {
            final List<PlayerRefrence> selection = friendList.getSelectedValuesList();

            if (selection.size() == 0) {
                JOptionPane.showMessageDialog(null, I18N.getString("FriendDialog.invalidFriendSelectionMin"),
                        I18N.getString("FriendDialog.removeFriendErrorTitle"), JOptionPane.ERROR_MESSAGE);
                return;
            }

            final String[] names = new String[selection.size()];

            for (int i = 0; i < names.length; i++) {
                names[i] = selection.get(i).getPlayerName();
            }

            final Message message = new Message(MessageType.REMOVE_FRIEND);
            message.addArray(EntryType.USER_NAME, names);

            player.sendMessage(message);
        });

        inviteFriend.addActionListener(e -> {
            final List<PlayerRefrence> selection = friendList.getSelectedValuesList();

            if (selection.size() == 0) {
                JOptionPane.showMessageDialog(null, I18N.getString("FriendDialog.invalidFriendSelectionMin"),
                        I18N.getString("FriendDialog.inviteFriendErrorTitle"), JOptionPane.ERROR_MESSAGE);
                return;
            } else if (selection.size() > 3) {
                JOptionPane.showMessageDialog(null, I18N.getString("FriendDialog.invalidFriendSelectionMax"),
                        I18N.getString("FriendDialog.inviteFriendErrorTitle"), JOptionPane.ERROR_MESSAGE);
                return;
            }

            final String[] names = new String[selection.size()];

            for (int i = 0; i < names.length; i++) {
                names[i] = selection.get(i).getPlayerName();
            }

            final Message message = new Message(MessageType.INVITE_PLAYERS);
            message.addArray(EntryType.USER_NAME, names);

            player.sendMessage(message);
        });

        final JList<ChatRoom> lobbyList = new JList<>(lobbyListModel);
        leftSideBar.setLeftComponent(new JScrollPane(lobbyList));

        lobbyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lobbyList.addListSelectionListener(e -> {
            if (e.getValueIsAdjusting()) {
                return;
            }

            lobbyModel.joinRoom(lobbyList.getSelectedValue());
        });

        player.setMessageListener(Message.MessageType.INVITE, (p, m) -> this.onInvite(m));
        player.setMessageListener(Message.MessageType.INVITE_CANCELLED, (p, m) -> this.onInviteCancelled(m));

        this.setVisible(true);
        this.pack();
    }

    /**
     * Find a game. This method is called when the menu-option "Find game" is
     * clicked, and can also be called "manually" to automate the process.
     */
    public void findGame() {
        if (!this.player.getInGame()) {
            this.player.sendMessage(new Message(MessageType.FIND_GAME));
        } else {
            JOptionPane.showMessageDialog(null, I18N.getString("Menu.alreadyInGameDesc"),
                    I18N.getString("Menu.alreadyInGameTitle"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void onInviteCancelled(Message message) {

        JOptionPane.showMessageDialog(null, I18N.getString("Menu.inviteCancelled"));

    }

    private void onInvite(Message message) {

        final int option = JOptionPane.showConfirmDialog(null, I18N.getString("Menu.invite"),
                I18N.getString("Menu.inviteBy") + message.getEntry(EntryType.USER_NAME), JOptionPane.YES_NO_OPTION);

        final Message response = new Message(MessageType.INVITE_RESPONSE);
        if (option == JOptionPane.YES_OPTION) {
            response.addEntry(EntryType.ACCEPT_INVITE, String.valueOf(true));
        } else {
            response.addEntry(EntryType.ACCEPT_INVITE, String.valueOf(false));
        }

        this.player.sendMessage(response);
    }

    private void setUpMenu(LobbyModel lobbyModel) {
        final JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        final JMenu mnLudonet = new JMenu(I18N.getString("App.title"));
        menuBar.add(mnLudonet);

        final JMenuItem mntmQuit = new JMenuItem(I18N.getString("Menu.quit"));
        mnLudonet.add(mntmQuit);
        mntmQuit.addActionListener(e -> System.exit(0));

        final JMenu mnLobby = new JMenu(I18N.getString("Menu.lobby"));
        menuBar.add(mnLobby);

        final JMenuItem mntNewLobby = new JMenuItem(I18N.getString("Menu.newRoom"));
        mnLobby.add(mntNewLobby);
        mntNewLobby.addActionListener(e -> {
            final String name = JOptionPane.showInputDialog(I18N.getString("Menu.roomName"));
            if ("".equals(name)) {
                return;
            }

            final Message message = new Message(MessageType.CHAT_ROOM_CREATE);
            message.addEntry(EntryType.CHAT_ROOM_NAME, name);

            this.player.sendMessage(message);
        });

        final JMenuItem mntRefreshLobby = new JMenuItem(I18N.getString("Menu.refreshRoom"));
        mnLobby.add(mntRefreshLobby);
        mntRefreshLobby.addActionListener(e -> {
            final Message message = new Message(MessageType.LIST_CHAT_ROOMS);
            this.player.sendMessage(message);
        });

        final JMenuItem mntJoinLobby = new JMenuItem(I18N.getString("Menu.joinRoom"));
        mnLobby.add(mntJoinLobby);
        mntJoinLobby.addActionListener(e -> {
            final String name = JOptionPane.showInputDialog(I18N.getString("Menu.roomName"));
            if ("".equals(name)) {
                return;
            }

            final Message message = new Message(MessageType.CHAT_ROOM_JOIN);
            message.addEntry(EntryType.CHAT_ROOM_NAME, name);

            this.player.sendMessage(message);
        });

        final JMenuItem mntLeaveLobby = new JMenuItem(I18N.getString("Menu.leaveRoom"));
        mnLobby.add(mntLeaveLobby);
        mntLeaveLobby.addActionListener(e -> lobbyModel.leaveRoom());

        final JMenu mnGame = new JMenu(I18N.getString("Menu.game"));
        menuBar.add(mnGame);

        final JMenuItem mntFindGame = new JMenuItem(I18N.getString("Menu.findGame"));
        mnGame.add(mntFindGame);
        mntFindGame.addActionListener(e -> this.findGame());

        final JMenuItem mntHighScoreList = new JMenuItem(I18N.getString("Menu.showHighScore"));
        mnGame.add(mntHighScoreList);

        mntHighScoreList.addActionListener(e -> {
            final Message message = new Message(MessageType.LIST_SCOREBOARD);

            message.addEntry(EntryType.SCOREBOARD_TYPE, SCOREBOARD_TYPE.TOTAL_WINS);
            message.addEntry(EntryType.SCOREBOARD_INDEX, 0);
            message.addEntry(EntryType.SCOREBOARD_COUNT, 10);

            this.player.sendMessage(message);
        });

        this.player.setMessageListener(MessageType.SCOREBOARD, (p, m) -> {

            final String serial = m.getEntry(EntryType.SCOREBOARD_SERIALIZED);
            Scoreboard scores = null;

            try {
                scores = new Scoreboard(serial);
                final List<Entry> data = scores.getEntries();
                final JList<Entry> list = new JList<>(new Vector<>(data));

                JOptionPane.showMessageDialog(null, new JScrollPane(list), I18N.getString("Popup.scoreboardTitle"),
                        JOptionPane.INFORMATION_MESSAGE, LobbyWindow.scoreIcon);

            } catch (final DeserializeException e) {
                LobbyWindow.LOGGER.log(Level.WARNING, "Failed to deserialize scoreboard data", e);
            }
        });

        final JMenu mnHelp = new JMenu(I18N.getString("Menu.help"));
        menuBar.add(mnHelp);

        final JMenuItem mntmAbout = new JMenuItem(I18N.getString("Menu.about"));
        mnHelp.add(mntmAbout);
        mntmAbout.addActionListener(e -> JOptionPane.showMessageDialog(null, I18N.getString("Popup.aboutContent"),
                I18N.getString("Popup.aboutTitle"), JOptionPane.INFORMATION_MESSAGE, LobbyWindow.aboutIcon));

        final JMenuItem mntmRules = new JMenuItem(I18N.getString("Menu.rules"));
        mnHelp.add(mntmRules);
        mntmRules.addActionListener(e -> JOptionPane.showMessageDialog(null, LobbyWindow.rules,
                I18N.getString("Popup.rulesTitle"), JOptionPane.INFORMATION_MESSAGE, LobbyWindow.aboutIcon));
    }

    static {

        final URL aboutURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/HELP.png");
        aboutIcon = new ImageIcon(aboutURL, "About");

        final URL addURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/ADD.png");
        addIcon = new ImageIcon(addURL, I18N.getString("Icon.ADD"));

        final URL removeURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/REMOVE.png");
        removeIcon = new ImageIcon(removeURL, I18N.getString("Icon.REMOVE"));

        final URL inviteURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/LUDO.png");
        inviteIcon = new ImageIcon(inviteURL, I18N.getString("Icon.INVITE"));

        final URL lobbyURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/CHALLENGE.png");
        challengeIcon = new ImageIcon(lobbyURL, I18N.getString("Icon.LUDO"));

        final URL scoreURL = LobbyWindow.class.getResource("/com/enderwolf/ludonet/resources/SCORE.png");
        scoreIcon = new ImageIcon(scoreURL, "Score");

        final StringBuilder rulesBuilder = new StringBuilder("");

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                LobbyWindow.class.getResourceAsStream("/com/enderwolf/ludonet/shared/i18n/rules.html"), "UTF16"))) {
            String tmp = null;

            while ((tmp = bufferedReader.readLine()) != null) {
                rulesBuilder.append(tmp);
            }

        } catch (final IOException ioe) {
            LobbyWindow.LOGGER.log(Level.WARNING, "Failed to load help page", ioe);
        }

        rules = rulesBuilder.toString();
    }
}
