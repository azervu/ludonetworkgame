package com.enderwolf.ludonet.client.gui.login;

import javax.swing.JTextField;

public class CredentialsVerifier {
    public enum Status {
        OK,
        INVALID_USERNAME,
        INVALID_PASSWORD,
        UNEQUAL_PASSWORD,
    }

    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 16;
    public static final int PASSWORD_MIN_LENGTH = 4;

    private final JTextField userNameField;
    private final JTextField passwordField;
    private JTextField passwordRepeatField;

    public CredentialsVerifier(JTextField userName, JTextField password) {
        this.userNameField = userName;
        this.passwordField = password;
        this.passwordRepeatField = null;
    }

    public CredentialsVerifier(JTextField userName, JTextField password, JTextField passwordRepeat) {
        this.userNameField = userName;
        this.passwordField = password;
        this.passwordRepeatField = passwordRepeat;
    }

    public void setPasswordRepeatField(JTextField repField) {
        this.passwordRepeatField = repField;
    }

    /**
     * Check if the contents of the defined JTextFields contain valid values.
     * The user name is checked to be of the minimum length and only contain
     * valid characters. The password field is verified be of minimum length. If
     * a password-repeat-field is defined, it's content must match the password
     * field.
     *
     * @return True if all the conditions are met, false otherwise.
     */
    public Status verifyCredentials() {
        Status status;

        status = this.verifyUserName();
        if (status != Status.OK) {
            return status;
        }

        status = this.verifyPassword();
        if (status != Status.OK) {
            return status;
        }

        return Status.OK;
    }

    private Status verifyUserName() {
        if (this.userNameField == null) {
            return Status.INVALID_USERNAME;
        }

        final String name = this.userNameField.getText();

        if (name.length() < CredentialsVerifier.USERNAME_MIN_LENGTH
                || name.length() > CredentialsVerifier.USERNAME_MAX_LENGTH) {
            return Status.INVALID_USERNAME;
        }

        for (int i = 0; i < name.length(); i++) {
            if (!Character.isLetterOrDigit(name.charAt(i))) {
                return Status.INVALID_USERNAME;
            }
        }

        return Status.OK;
    }

    private Status verifyPassword() {
        if (this.passwordField == null) {
            return Status.INVALID_PASSWORD;
        }

        final String pass = this.passwordField.getText();

        if (pass.length() < CredentialsVerifier.PASSWORD_MIN_LENGTH) {
            return Status.INVALID_PASSWORD;
        }

        if (this.passwordRepeatField != null) {
            if (!pass.equals(this.passwordRepeatField.getText())) {
                return Status.UNEQUAL_PASSWORD;
            }
        }

        return Status.OK;
    }
}
