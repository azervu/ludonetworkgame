package com.enderwolf.ludonet.client.gui.login;

/**
 * The LoginListener the callback object of LoginPanel. The listener declares
 * interfaces for when the panel wants to log in to the game server or register
 * a new user account.
 */
public interface LoginListener {
    /**
     * Called when the login-interface wants to log in.
     *
     * @param userName
     *            The user-name of the user in plain text. The string is
     *            guaranteed to be compliant with the defined user-name
     *            requirements.
     * @param password
     *            The password of the user hashed to MD5. The original string is
     *            guaranteed to be compliant with the defined password
     *            requirements.
     */
    void onLogin(String userName, String password);

    /**
     * Called when the login-interface wants to register a user.
     *
     * @param userName
     *            The user-name of the user in plain text. The string is
     *            guaranteed to be compliant with the defined user-name
     *            requirements.
     * @param password
     *            The password of the user hashed to MD5. The original string is
     *            guaranteed to be compliant with the defined password
     *            requirements.
     */
    void onRegister(String userName, String password);
}
