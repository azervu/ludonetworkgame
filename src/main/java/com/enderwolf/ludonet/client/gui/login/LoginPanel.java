package com.enderwolf.ludonet.client.gui.login;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.enderwolf.ludonet.shared.I18N;
import com.enderwolf.ludonet.shared.MD5;

/**
 * The LoginPanel can be used for both logging in and registering a user. The
 * difference between the two types is an additional JTextField (for password
 * repeating during registration) and the text of the JButton.
 */
public class LoginPanel extends JPanel {
    private static final long serialVersionUID = -1134019406077987848L;

    public enum Type {
        LOGIN,
        REGISTER
    };

    private final LoginListener delegate;
    private final LoginPanel.Type loginType;
    private JTextField[] textFields;
    private JButton button;
    private JLabel errorLabel;
    private CredentialsVerifier verifier;

    /**
     * Create a LoginPanel of a specific type (LOGIN or REGISTER) with a
     * specified LoginDelegate.
     *
     * @param delegate
     *            The delegate to be notified when the user performs an action.
     * @param type
     *            LOGIN or REGISTER. Defines how the user interface will be
     *            constructed.
     */
    public LoginPanel(LoginListener delegate, LoginPanel.Type type) {
        this.delegate = delegate;
        this.loginType = type;

        this.setupUI();
    }

    /**
     * Retrieve the login-type.
     *
     * @return Type.LOGIN or Type.REGISTER. This is the parameter defined in the
     *         constructor.
     */
    public LoginPanel.Type getLoginType() {
        return this.loginType;
    }

    /**
     * Create the graphical user interface. Should only be called one.
     */
    private void setupUI() {
        this.setLayout(new GridBagLayout());

        this.createUIComponents();

        if (this.loginType == Type.LOGIN) {
            this.button.setText(I18N.getString("Login.login"));
            this.verifier = new CredentialsVerifier(this.textFields[0], this.textFields[1]);
        } else {
            this.button.setText(I18N.getString("Login.register"));
            this.verifier = new CredentialsVerifier(this.textFields[0], this.textFields[1], this.textFields[2]);
        }

        this.button.addActionListener(e -> this.onButtonClick());
    }

    /**
     * Create the GUI-widgets that will be contained within the panel.
     */
    private void createUIComponents() {
        final int numTextFields = (this.loginType == Type.LOGIN) ? 2 : 3;
        this.textFields = new JTextField[numTextFields];

        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;

        final String[] labelTexts = new String[] {
                I18N.getString("Login.userName"),
                I18N.getString("Login.password"),
                I18N.getString("Login.repeatPassword")
        };

        for (int i = 0; i < numTextFields; i++) {
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.ipadx = 300;
            final JLabel label = new JLabel();
            label.setText(labelTexts[i]);
            this.add(label, gbc);
            gbc.gridy++;

            if (i == 0) {
                this.textFields[i] = new JTextField();
            } else {
                this.textFields[i] = new JPasswordField();
            }

            gbc.anchor = GridBagConstraints.CENTER;
            gbc.ipadx = 0;
            this.add(this.textFields[i], gbc);
            gbc.gridy++;
        }

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.ipadx = 0;
        this.button = new JButton();
        this.add(this.button, gbc);
        gbc.gridy++;

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.ipadx = 0;
        this.errorLabel = new JLabel();
        this.errorLabel.setForeground(Color.RED);
        this.add(this.errorLabel, gbc);
    }

    /**
     * Called when the "login" or "register" button is clicked. The credentials
     * are verified for correctness. If they are in fact valid, the
     * LoginDelegate will be notified about the action. If any errors has
     * occurred, the error label will display which actions can be taken to
     * resolve the error.
     */
    private void onButtonClick() {
        CredentialsVerifier.Status status;
        status = this.verifier.verifyCredentials();

        switch (status) {
            case OK:
                this.errorLabel.setText(null);
                final String userName = this.textFields[0].getText();
                final String passHash = this.hashPassword();

                if (this.loginType == Type.LOGIN) {
                    this.delegate.onLogin(userName, passHash);
                } else {
                    this.delegate.onRegister(userName, passHash);
                }
                break;

            case INVALID_USERNAME:
                this.errorLabel.setText(I18N.getString("Login.invalidUsername"));
                break;

            case INVALID_PASSWORD:
                this.errorLabel.setText(I18N.getString("Login.invalidPassword"));
                break;

            case UNEQUAL_PASSWORD:
                this.errorLabel.setText(I18N.getString("Login.unequalPassword"));
                break;
        }

        // Encase the JLabel text in HTML-tags to allow multiple lines
        String text = this.errorLabel.getText();
        if (text != null && text.length() != 0) {
            text = "<html><body width='250px'>" + text + "</body></html>";
            this.errorLabel.setText(text);
        }
    }

    /**
     * Attempt to hash the password-field's text to MD5. The password is
     * contained within LoginPanel#textFields[1]'s text attribute.
     *
     * @return The hashed password on success, null on failure.
     */
    private String hashPassword() {
        if (this.textFields[1] == null || this.textFields[1].getText().length() == 0) {
            return null;
        }

        return MD5.hash(this.textFields[1].getText());
    }
}
