package com.enderwolf.ludonet.client.gui.login;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.enderwolf.ludonet.shared.I18N;

/**
 * The LoginWindow is the initial window in the client application. It presents
 * the user with an interface to log in to the game server (given an existing
 * account) or register a new account if one does not exist. The LoginWindow
 * does not itself perform any logging in - a LoginDelegate can be assigned to
 * the LoginWindow to receive event callbacks when the user wants to perform an
 * action.
 */
public class LoginWindow extends JFrame implements LoginListener {
    private static final long serialVersionUID = -7385106277404888853L;
    private static final Logger LOGGER = Logger.getGlobal();

    private LoginPanel loginPanel;
    private JPanel rootPanel;
    private JLabel promptLabel;
    private JButton toggleButton;
    private LoginListener loginDelegate;

    /**
     * Default constructor. Sets up the window with a login-interface, which can
     * be toggled to show the registration interface instead.
     */
    public LoginWindow() {
        this.setSize(new Dimension(600, 450));
        this.setMinimumSize(this.getSize());
        this.setLocation(300, 200);

        this.setupUI();

        this.setVisible(true);
    }

    public void setLoginDelegate(LoginListener delegate) {
        this.loginDelegate = delegate;
    }

    @Override
    public void onLogin(String userName, String password) {
        LoginWindow.LOGGER.log(Level.INFO, "Login: " + userName + ", " + password);

        if (this.loginDelegate != null) {
            this.loginDelegate.onLogin(userName, password);
        }
    }

    @Override
    public void onRegister(String userName, String password) {
        LoginWindow.LOGGER.log(Level.INFO, "Register: " + userName + ", " + password);

        if (this.loginDelegate != null) {
            this.loginDelegate.onRegister(userName, password);
        }
    }

    private void setupUI() {
        this.rootPanel = new JPanel(new GridBagLayout());
        this.add(this.rootPanel);

        this.createToggleButton();
        this.setLoginPanel();
    }

    /**
     * Create the button that toggles between the login and registration
     * interfaces.
     */
    private void createToggleButton() {
        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.NONE;

        this.promptLabel = new JLabel();
        this.rootPanel.add(this.promptLabel, gbc);
        gbc.gridy++;

        this.toggleButton = new JButton();
        this.rootPanel.add(this.toggleButton, gbc);

        this.toggleButton.addActionListener(e -> this.toggleLoginPanelType());
    }

    /**
     * Toggle the currently displayed panel. If the login interface is visible,
     * the registration interface will be shown instead (and vice versa).
     */
    private void toggleLoginPanelType() {
        if (this.loginPanel != null) {
            if (this.loginPanel.getLoginType() == LoginPanel.Type.LOGIN) {
                this.setRegisterPanel();
            } else {
                this.setLoginPanel();
            }
        } else {
            this.setRegisterPanel();
        }
    }

    /**
     * Set the currently displayed interface to the login interface.
     */
    private void setLoginPanel() {
        this.promptLabel.setText(I18N.getString("Login.registerPrompt"));
        this.toggleButton.setText(I18N.getString("Login.register"));
        this.setLoginPanel(new LoginPanel(this, LoginPanel.Type.LOGIN));
    }

    /**
     * Set the currently displayed interface to the registration interface.
     */
    private void setRegisterPanel() {
        this.promptLabel.setText(I18N.getString("Login.loginPrompt"));
        this.toggleButton.setText(I18N.getString("Login.login"));
        this.setLoginPanel(new LoginPanel(this, LoginPanel.Type.REGISTER));
    }

    /**
     * Set the main panel to be displayed.
     *
     * @param panel
     *            The panel to replace the existing mainPanel.
     */
    private void setLoginPanel(LoginPanel panel) {
        if (this.loginPanel != null && this.loginPanel.getParent() == this.rootPanel) {
            this.rootPanel.remove(this.loginPanel);
        }

        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.f;
        gbc.weighty = 1.f;

        this.loginPanel = panel;
        this.rootPanel.add(panel, gbc);

        this.pack();
    }
}
