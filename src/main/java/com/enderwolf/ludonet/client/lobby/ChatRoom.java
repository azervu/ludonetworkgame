/**
 *
 */
package com.enderwolf.ludonet.client.lobby;

/**
 * An abstract reference to a chat room client side.
 *
 * @author !Tulingen
 */
public class ChatRoom {

    private final int id;
    private final String name;

    /**
     * Default constructor.
     *
     * @param id
     *            Id of the room reference.
     * @param name
     *            Name of the room reference.
     */
    public ChatRoom(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Get the id of this chat room.
     *
     * @return The id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Gets the name of the chat room.
     *
     * @return The name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the string representation of this room, the name.
     *
     * @return The string representation of this room.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Checks if another object is equal to this.
     *
     * @return True if the same, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ChatRoom)) {
            return false;
        } else {
            return this.id == ((ChatRoom) obj).id;
        }
    }

    /**
     * "Calculates" the hash of this room.
     *
     * @return The hash as an integer.
     */
    @Override
    public int hashCode() {
        return this.id;
    }
}
