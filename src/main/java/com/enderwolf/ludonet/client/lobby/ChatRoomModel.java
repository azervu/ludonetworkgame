/**
 *
 */
package com.enderwolf.ludonet.client.lobby;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * Controller / model for handling a chat room.
 *
 * @author !Tulingen
 */
public class ChatRoomModel extends DefaultListModel<PlayerRefrence> {
    private static final long serialVersionUID = -135534636077833277L;
    private static Logger LOG = Logger.getGlobal();

    /**
     * A wrapped PlainDocument used for representing the chat text in the chat
     * room.
     *
     * @author !Tulingen
     */
    public class ChatDocument extends PlainDocument {
        private static final long serialVersionUID = -385031024740596501L;

        void addChatLine(String message, AttributeSet style) {

            try {
                this.insertString(this.getLength(), message, style);
            } catch (final BadLocationException e) {
                ChatRoomModel.LOG.log(Level.SEVERE, "Failed to insert string ad end of document", e);
            }
        }

        void addChatLine(String message) {
            this.addChatLine(message, null);
        }
    }

    private ChatDocument document = null;
    private ChatRoom chatRoom = null;

    /**
     * Default constructor.
     *
     * @param chatRoom
     *            The chat room to be a model for.
     */
    public ChatRoomModel(ChatRoom chatRoom) {
        this.document = new ChatDocument();
        this.chatRoom = chatRoom;
    }

    /**
     * Gets the chat room.
     *
     * @return The chat room.
     */
    public ChatRoom getChatRoom() {
        return this.chatRoom;
    }

    /**
     * Gets the chat document.
     *
     * @return The document.
     */
    public ChatDocument getDocument() {
        return this.document;
    }
}
