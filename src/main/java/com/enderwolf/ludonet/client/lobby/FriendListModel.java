/**
 *
 */
package com.enderwolf.ludonet.client.lobby;

import javax.swing.DefaultListModel;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.Player;

/**
 * The model for the friend list. Is also responsible for handling messages
 * related to the friend list.
 *
 * @author !Tulingen
 */
public class FriendListModel extends DefaultListModel<PlayerRefrence> {
    private static final long serialVersionUID = -1263712495497626726L;
    private Player player = null;

    /**
     * Default constructor.
     *
     * @param player
     *            The player object to attach listeners to.
     */
    public FriendListModel(Player player) {
        super();

        this.player = player;

        this.player.setMessageListener(MessageType.FRIEND_LIST_UPDATE, (p, m) -> this.onFriendListUpdate(m));

        final Message friendListRequest = new Message(MessageType.LIST_FIRENDS);

        friendListRequest.addEntry(EntryType.PLAYER_ID, player.getPlayerId());

        this.player.sendMessage(friendListRequest);
    }

    /**
     * Message handler for FRIEND_LIST_UPDATE
     *
     * @param message
     *            The message
     */
    private void onFriendListUpdate(Message message) {
        this.clear();

        final String[] ids = message.getArray(EntryType.PLAYER_ID);
        final String[] names = message.getArray(EntryType.USER_NAME);

        if (ids != null && names != null) {
            for (int i = 0; i < names.length; i++) {
                this.addElement(new PlayerRefrence(Integer.valueOf(ids[i]), names[i]));
            }
        }
    }
}
