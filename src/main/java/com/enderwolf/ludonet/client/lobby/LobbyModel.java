package com.enderwolf.ludonet.client.lobby;

import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JTabbedPane;

import com.enderwolf.ludonet.client.gui.lobby.ChatTab;
import com.enderwolf.ludonet.client.lobby.ChatRoomModel.ChatDocument;
import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.Player;

/**
 * The model responsible for almost all important functionality in the lobby
 * window.
 *
 * @author !Tulingen
 */
public class LobbyModel {

    private JTabbedPane tabbedPane = null;
    private final DefaultListModel<ChatRoom> lobbyListModel;

    private Map<ChatRoom, ChatRoomModel> chatModels = null;
    private Map<ChatRoom, ChatTab> chatTabs = null;
    private final Player player;

    /**
     * Default constructor
     *
     * @param player
     *            The player object to attach listeners to.
     * @param lobbyListModel
     *            The model for the list showing all the chat rooms.
     */
    public LobbyModel(Player player, DefaultListModel<ChatRoom> lobbyListModel) {
        this.chatModels = new HashMap<>();
        this.chatTabs = new HashMap<>();
        this.lobbyListModel = lobbyListModel;

        this.player = player;

        this.player.setMessageListener(MessageType.CHAT, (p, m) -> this.onChat(m));

        this.player.setMessageListener(MessageType.CHAT_ROOM_MEMBER_UPDATE, (p, m) -> this.onChatRoomMemberUpdate(m));

        this.player.setMessageListener(MessageType.CHAT_ROOMS, (p, m) -> this.onChatRooms(m));

        this.player.setMessageListener(MessageType.CHAT_ROOM_LEAVE, (p, m) -> {
            final int roomId = Integer.valueOf(m.getEntry(EntryType.CHAT_ROOM_ID));
            final ChatRoom room = new ChatRoom(roomId, null);
            this.lobbyListModel.removeElement(room);
            this.leaveRoom(room, false);
        });

        final Message message = new Message(Message.MessageType.LIST_CHAT_ROOMS);
        this.player.sendMessage(message);
    }

    /**
     * Setter for tabbedPane
     *
     * @param tabbedPane
     */
    public void setTabbedPane(JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
    }

    /**
     * Getter for player
     * 
     * @return  The player
     */
    public Player getPlayer() {
        return this.player;
    }
    
    /**
     * Sends a chat message to the server for processing.
     *
     * @param text
     */
    public void sendMessage(String text) {
        if ("".equals(text) || this.tabbedPane.getTabCount() == 0) {
            return;
        }

        final ChatTab from = (ChatTab) this.tabbedPane.getSelectedComponent();

        final Message message = new Message(MessageType.CHAT);
        message.addEntry(EntryType.CHAT_TEXT, text);
        message.addEntry(EntryType.CHAT_ROOM_ID, from.getModel().getChatRoom().getId());
        message.addEntry(EntryType.USER_NAME, this.player.getUserName());
        this.player.sendMessage(message);
    }

    /**
     * Asks the server to join a specific room.
     *
     * @param room
     *            the room to join.
     */
    public void joinRoom(ChatRoom room) {
        if (this.chatModels.containsKey(room)) {
            return;
        }

        final Message message = new Message(MessageType.CHAT_ROOM_JOIN);

        message.addEntry(EntryType.CHAT_ROOM_ID, room.getId());

        this.player.sendMessage(message);
    }

    /**
     * Leaves to room and notify the server if sendLeaveNotif is true;
     *
     * @param room
     *            The room to leave.
     * @param sendLeaveNotif
     *            If the client should notify the server.
     */
    public void leaveRoom(ChatRoom room, boolean sendLeaveNotif) {
        if (!this.chatModels.containsKey(room)) {
            return;
        }

        if (sendLeaveNotif) {
            final Message message = new Message(MessageType.CHAT_ROOM_LEAVE);

            message.addEntry(EntryType.CHAT_ROOM_ID, room.getId());

            this.player.sendMessage(message);
        }

        this.chatModels.remove(room);
        this.tabbedPane.remove(this.chatTabs.get(room));
        this.chatTabs.remove(room);
    }

    /**
     * Leaves to room and notify the server.
     *
     * @param room
     *            the room to leave.
     */
    public void leaveRoom(ChatRoom room) {
        this.leaveRoom(room, true);
    }

    /**
     * Leaves the currently opened room.
     */
    public void leaveRoom() {
        this.leaveRoom(((ChatTab) this.tabbedPane.getSelectedComponent()).getModel().getChatRoom());
    }

    /**
     * Method for handling CHAT messages
     *
     * @param message
     */
    private void onChat(Message message) {
        final int roomId = Integer.valueOf(message.getEntry(EntryType.CHAT_ROOM_ID));
        final String text = message.getEntry(EntryType.CHAT_TEXT);
        final String user = message.getEntry(EntryType.USER_NAME);

        final ChatDocument doc = this.chatModels.get(new ChatRoom(roomId, null)).getDocument();
        doc.addChatLine(user + ": ", null);
        doc.addChatLine(text + "\n");
    }

    /**
     * Method for handling CHAT_ROOM_MEMBER_UPDATE messages
     *
     * @param message
     */
    private void onChatRoomMemberUpdate(Message message) {
        final int id = Integer.valueOf(message.getEntry(EntryType.CHAT_ROOM_ID));
        final String name = message.getEntry(EntryType.CHAT_ROOM_NAME);
        ChatRoom room = null;

        if (!this.chatModels.containsKey(new ChatRoom(id, null))) {
            room = this.createRoom(id, name);

            if (!this.lobbyListModel.contains(room)) {
                this.lobbyListModel.addElement(room);
            }
        } else {
            room = new ChatRoom(id, name);
        }

        final String[] usernames = message.getArray(EntryType.USER_NAME);
        final String[] userids = message.getArray(EntryType.PLAYER_ID);
        final String[] inRoom = message.getArray(EntryType.IN_CHAT_ROOM);

        for (int i = 0; i < inRoom.length; i++) {
            final PlayerRefrence ref = new PlayerRefrence(Integer.valueOf(userids[i]), usernames[i]);
            final ChatRoomModel currentModel = this.chatModels.get(room);

            final boolean exists = currentModel.contains(ref);
            final boolean shoudBeThere = Boolean.valueOf(inRoom[i]);

            if (exists && !shoudBeThere) {
                currentModel.removeElement(ref);
            } else if (!exists && shoudBeThere) {
                currentModel.addElement(ref);
            }
        }
    }

    /**
     * Method for handling CHAT_ROOMS messages
     *
     * @param message
     */
    private void onChatRooms(Message message) {
        final String[] ids = message.getArray(EntryType.CHAT_ROOM_ID);
        final String[] names = message.getArray(EntryType.CHAT_ROOM_NAME);

        this.lobbyListModel.clear();
        for (int i = 0; i < ids.length; i++) {
            final ChatRoom room = new ChatRoom(Integer.valueOf(ids[i]), names[i]);
            this.lobbyListModel.addElement(room);

            if (room.getId() == 1 && !this.chatTabs.containsKey(room)) {
                final Message globalChatRequest = new Message(MessageType.CHAT_ROOM_JOIN);
                globalChatRequest.addEntry(EntryType.CHAT_ROOM_ID, 1);
                this.player.sendMessage(globalChatRequest);
            }
        }
    }

    /**
     * Creates the visual representation of a room and all related elements.
     *
     * @param id
     *            id of the room to show
     * @param name
     *            name of the room to show
     * @return the room reference
     */
    private ChatRoom createRoom(Integer id, String name) {

        final ChatRoom room = new ChatRoom(id, name);
        final ChatRoomModel model = new ChatRoomModel(room);

        final ChatTab textArea = new ChatTab(model);
        this.chatModels.put(room, model);
        this.tabbedPane.addTab(name, null, textArea, null);
        this.chatTabs.put(room, textArea);

        return room;
    }
}
