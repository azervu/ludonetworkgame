/**
 *
 */
package com.enderwolf.ludonet.client.lobby;

/**
 * @author !Tulingen A data class representing a player.
 */
public class PlayerRefrence {
    private final int playerId;
    private final String playerName;

    /**
     * Default constructor.
     *
     * @param playerId
     * @param playerName
     */
    public PlayerRefrence(int playerId, String playerName) {
        this.playerId = playerId;
        this.playerName = playerName;
    }

    /**
     * Gets the player id of this representation.
     *
     * @return The id
     */
    public int getPlayerId() {
        return this.playerId;
    }

    /**
     * Gets the user name of this representation.
     *
     * @return The name
     */
    public String getPlayerName() {
        return this.playerName;
    }

    /**
     * Returns the string representation of this player representation, the
     * name.
     *
     * @return The string representation of this player representation.
     */
    @Override
    public String toString() {
        return this.playerName;
    }

    /**
     * Checks if another object is equal to this.
     *
     * @return True if the same, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PlayerRefrence)) {
            return false;
        } else {
            return ((PlayerRefrence) obj).playerId == this.playerId;
        }
    }

    /**
     * "Calculates" the hash of this representation.
     *
     * @return The hash as an integer.
     */
    @Override
    public int hashCode() {
        return this.playerId;
    }
}
