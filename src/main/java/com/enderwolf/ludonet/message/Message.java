package com.enderwolf.ludonet.message;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.soap.MessageFactory;

/**
 * An abstract class encapsulating the data sent between clients and server. It,
 * with the MessageFactory class provides the utilities for turning a string
 * into a Message object understood by the system.
 *
 * @see MessageFactory
 */
public class Message {
    private static final Logger LOGGER = Logger.getGlobal();

    private class Entry {
        boolean escapeString = false;
        boolean isArray = false;
        String[] values;
    }

    private static enum EntryOption {
        ARRAY,
        ESCAPE;

    }

    /**
     * The different types of messages.
     */
    public static enum MessageType {

        CHAT,
        LOGIN,
        CONNECTION_STATUS,
        LIST_CHAT_ROOMS,
        CHAT_ROOMS,
        CHAT_ROOM_MEMBER_UPDATE,
        CHAT_ROOM_CREATE,
        CHAT_ROOM_JOIN,
        CHAT_ROOM_LEAVE,
        ADD_FRIEND,
        REMOVE_FRIEND,
        LIST_FIRENDS,
        FRIEND_LIST_UPDATE,
        FIND_GAME,
        INVITE_PLAYERS,
        INVITE,
        INVITE_RESPONSE,
        INVITE_CANCELLED,
        GAME_PLAYER_JOIN,
        GAME_PLAYER_LEFT,
        GAME_STARTING,
        GAME_ACTIVE_PLAYER,
        GAME_MOVE,
        GAME_TURN_OVER,
        GAME_OVER,
        LIST_SCOREBOARD,
        SCOREBOARD,
    }

    /**
     * The different types of entries a message can have.
     */
    public static enum EntryType {

        CHAT_TEXT,
        MESSAGE_TYPE,
        CONNECTION_STATUS,
        SENDER,
        PASSWORD,
        CREATE_USER,
        ACCEPT_INVITE,
        CHAT_ROOM_ID,
        CHAT_ROOM_NAME,
        IN_CHAT_ROOM,
        PLAYER_COLOR,
        NUM_MOVES,
        GAME_PIECE,
        USER_NAME,
        PLAYER_ID,
        SCOREBOARD_INDEX,
        SCOREBOARD_COUNT,
        SCOREBOARD_TYPE,
        SCOREBOARD_SERIALIZED,
    }

    public static Message createMessage(String messageString) {
        final Message msg = new Message(messageString);

        try {
            msg.getType();
        } catch (final IllegalArgumentException e) {
            Message.LOGGER.log(Level.WARNING, "Invalid parameter", e);
            return null;
        }

        return msg;
    }

    protected static String deEscapeString(String stringInput) {
        if (stringInput == null) {
            return stringInput;
        }

        String deEscapedString = stringInput;
        deEscapedString = Message.deEscapeSubString(deEscapedString, "([^#]|^)(##)*(#s)", 2, ",");
        deEscapedString = Message.deEscapeSubString(deEscapedString, "([^#]|^)(##)*(#c)", 2, ":");
        deEscapedString = Message.deEscapeSubString(deEscapedString, "([^#]|^)(##)*(#n)", 2, "\n");
        deEscapedString = Message.deEscapeSubString(deEscapedString, "([^#]|^)(##)*(#t)", 2, "\t");
        deEscapedString = Message.deEscapeSubString(deEscapedString, "^[^#]*##", 2, "#");

        return deEscapedString;
    }

    private static String deEscapeSubString(String stringInput, String match, int remove, String replace) {
        final Pattern pattern = Pattern.compile(match);
        String searchString = stringInput;
        Matcher matcher = pattern.matcher(searchString);
        String deEscapedString = "";

        while (matcher.find()) {
            deEscapedString += searchString.substring(0, matcher.end() - remove) + replace;
            searchString = searchString.substring(matcher.end());
            matcher = pattern.matcher(searchString);
        }

        deEscapedString += searchString;
        return deEscapedString;
    }

    protected static String escapeString(String stringInput) {
        if (stringInput == null) {
            return stringInput;
        }

        String escapedString = stringInput.replaceAll("#", "##");
        escapedString = escapedString.replaceAll("\n", "#n");
        escapedString = escapedString.replaceAll("\t", "#t");
        escapedString = escapedString.replaceAll(",", "#s");
        escapedString = escapedString.replaceAll(":", "#c");

        return escapedString;
    }

    protected TreeMap<EntryType, Entry> valueMap;

    /**
     * Constructor, only detailing the type of message, sender will default to
     * "null".
     *
     * @param type
     *            the type of message to be sent
     */
    public Message(MessageType type) {
        this.valueMap = new TreeMap<EntryType, Entry>();
        this.addEntry(EntryType.MESSAGE_TYPE, type.toString(), false);
        this.addEntry(EntryType.SENDER, "null", true);
    }

    /**
     * Constructor detailing the type of message and who is sending it.
     *
     * @param type
     *            the type of message to be sent
     * @param sender
     *            the sender of the message
     */
    public Message(MessageType type, String sender) {
        this.valueMap = new TreeMap<EntryType, Entry>();
        this.addEntry(EntryType.MESSAGE_TYPE, type.toString(), false);
        this.addEntry(EntryType.SENDER, sender, true);
    }

    /**
     * Adds data to the message
     *
     * @param header
     *            what kind of data you are sending, may only send one of each
     *            type per message.
     * @param values
     *            an Object array of the values you are sending. Relies on
     *            toString()
     * @param escapeValue
     *            if the values need to be escaped, this is necessary if they
     *            may contain : , \t \n
     */
    public void addArray(EntryType header, Object[] values, boolean escapeValue) {
        final Entry entry = new Entry();
        entry.isArray = true;
        entry.escapeString = escapeValue;
        entry.values = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                entry.values[i] = null;
            } else {
                entry.values[i] = values[i].toString();
            }
        }
        this.valueMap.put(header, entry);
    }

    public void addArray(EntryType header, Object[] values) {
        this.addArray(header, values, true);
    }

    /**
     * Adds data to the message
     *
     * @param header
     *            what kind of data you are sending, may only send one of each
     *            type per message.
     * @param value
     *            an Object of the values you are sending. Relies on toString()
     * @param escapeValueif
     *            the value need to be escaped, this is necessary if they may
     *            contain : , \t \n
     */
    public void addEntry(EntryType header, Object value, boolean escapeValue) {
        final Entry entry = new Entry();
        entry.escapeString = escapeValue;
        entry.values = new String[1];
        entry.values[0] = value.toString();
        this.valueMap.put(header, entry);
    }

    public void addEntry(EntryType header, Object value) {
        this.addEntry(header, value, true);
    }

    /**
     * Gets data from the message
     *
     * @param header
     *            what kind of data you want.
     * @return an array of the values in the message, if this was added with
     *         addEntry you will get an array with one item.
     */
    public String[] getArray(EntryType header) {
        final Entry entry = this.valueMap.get(header);

        if (!entry.isArray) {
            Message.LOGGER.log(Level.WARNING, "getArray called on single entry: " + header);
        }

        if (entry.values.length == 1 && entry.values[0].isEmpty()) {
            return new String[0];
        }

        return entry.values;
    }

    /**
     * Gets data from the message
     *
     * @param header
     *            what kind of data you want.
     * @return the value in the message, if this was added with addArray you
     *         will get only get the first entry.
     */
    public String getEntry(EntryType header) {
        final Entry entry = this.valueMap.get(header);

        if (entry.isArray) {
            Message.LOGGER.log(Level.WARNING, "getEntry called on array: " + header);
        }

        return entry.values[0];
    }

    /**
     * returns the sender of the message.
     *
     * @return the sender of the message.
     */
    public String getSender() {
        return this.getEntry(EntryType.SENDER);
    }

    /**
     * returns the type of the message.
     *
     * @return the type of the message.
     */
    public MessageType getType() {
        return MessageType.valueOf(this.getEntry(EntryType.MESSAGE_TYPE));
    }

    /**
     * The function for turning a message into a String which can be sent over a
     * network.
     *
     * @return a string containing the data for the message.
     */
    @Override
    public String toString() {
        String tmp = "";

        for (final Map.Entry<EntryType, Entry> entry : this.valueMap.entrySet()) {

            tmp += entry.getKey().toString() + ":";

            for (int i = 0; i < entry.getValue().values.length - 1; i++) {
                if (entry.getValue().escapeString) {
                    tmp += Message.escapeString(entry.getValue().values[i]);
                } else {
                    tmp += entry.getValue().values[i];
                }
                tmp += ",";
            }

            if (entry.getValue().values.length > 0) {
                if (entry.getValue().escapeString) {
                    tmp += Message.escapeString(entry.getValue().values[entry.getValue().values.length - 1]);
                } else {
                    tmp += entry.getValue().values[entry.getValue().values.length - 1];
                }
            }

            if (entry.getValue().escapeString) {
                tmp += ":" + EntryOption.ESCAPE.toString();
            }

            if (entry.getValue().isArray) {
                tmp += ":" + EntryOption.ARRAY.toString();
            }

            tmp += "\t";
        }

        tmp += "\n";
        return tmp;
    }

    /**
     * The constructor used by createMessage, not recommended to be used
     * manually.
     *
     * @param headers
     *            the names of values to be sent by the message
     * @param values
     *            the actual values to be sent by the message
     */
    protected Message(String messageString) {
        this.valueMap = new TreeMap<EntryType, Entry>();

        final String[] textFields = messageString.split("\n")[0].split("\t");
        for (final String str : textFields) {
            final String[] messageFields = str.split(":");
            final EntryType header = EntryType.valueOf(messageFields[0]);
            String[] values = new String[1];
            values[0] = messageFields[1];
            boolean escape = false;
            boolean isArray = false;

            int index = 2;
            while (index < messageFields.length) {
                switch (EntryOption.valueOf(messageFields[index])) {
                    case ESCAPE:
                        escape = true;
                        break;
                    case ARRAY:
                        isArray = true;
                        break;
                    default:
                        break;
                }
                index++;
            }

            if (isArray) {
                values = values[0].split(",");
            }
            if (escape) {
                for (int i = 0; i < values.length; i++) {
                    values[i] = Message.deEscapeString(values[i]);
                }
            }
            if (isArray) {
                this.addArray(header, values, escape);
            } else {
                this.addEntry(header, values[0], escape);
            }

        }

    }
}