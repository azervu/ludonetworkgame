--------------------------------------------------------------------
--  This script is run for initialization of the database LudoDB  --
--------------------------------------------------------------------


-- Data which directly relate to the player, and ONLY the player, are stored in
-- this table. Player entries are for the most part on the receiving end of a reference.
-- The "ludo_color" column refers to the 'home' quadrant of the player, and will always
-- hold a value in the range [0,3] (inclusive). This ensures that the players take their
-- turn in the correct order and that the board is displayed equally across all clients.
CREATE TABLE Player (
    id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    name VARCHAR(40) NOT NULL UNIQUE,
    pass VARCHAR(40) NOT NULL,
    tot_games INT NOT NULL DEFAULT 0,
    tot_wins INT NOT NULL DEFAULT 0,

    CONSTRAINT player_pk PRIMARY KEY (id)
);


-- Player realationships. Any player can befriend any other player. 'player_a' must always
-- have a lower value than 'player_b'.
CREATE TABLE Friends (
    player_a   INT NOT NULL,
    player_b   INT NOT NULL,

    CONSTRAINT friends_pk PRIMARY KEY (player_a, player_b),
    CONSTRAINT friends_player_fk_a FOREIGN KEY (player_a) REFERENCES Player(id),
    CONSTRAINT friends_player_fk_b FOREIGN KEY (player_b) REFERENCES Player(id),
    CHECK (player_a < player_b)
);


-- A chat room can contain multiple players. The messages posted in a chat room are stored
-- in the ChatMessage-table. The connected players are stored in the ChatRoomPlayerConnection table.
CREATE TABLE ChatRoom (
    id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    name VARCHAR(40) NOT NULL,
    hidden INT NOT NULL DEFAULT 1,

    CONSTRAINT room_pk PRIMARY KEY (id)
);

-- A message sent from a player to a specified chat room. The recipients of the messages can
-- be derived from the ChatRoomPlayerConnection table.
CREATE TABLE ChatMessage (
    id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    room_id INT NOT NULL,
    player_id INT NOT NULL,
    msg VARCHAR(256),

    CONSTRAINT cmsg_pk PRIMARY KEY (id),
    CONSTRAINT cmsg_room_fk FOREIGN KEY (room_id) REFERENCES ChatRoom(id),
    CONSTRAINT cmsg_player_fk FOREIGN KEY (player_id) REFERENCES Player(id)
);
