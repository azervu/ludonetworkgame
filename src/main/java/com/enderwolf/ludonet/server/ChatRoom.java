package com.enderwolf.ludonet.server;

import java.util.ArrayList;
import java.util.List;

import com.enderwolf.ludonet.shared.Player;

/**
 *
 */
public class ChatRoom {
    private final int roomId;
    private final String roomName;
    private final List<Player> playerList;
    private final boolean hidden;

    public ChatRoom(int roomId, String roomName, boolean hidden) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.hidden = hidden;
        this.playerList = new ArrayList<>();
    }

    /**
     * Retrieve the unique ID (as defined in the database) of this chat room.
     *
     * @return The unique ID of this chat room.
     */
    public int getRoomId() {
        return this.roomId;
    }

    /**
     * Retrieve the human-readable name of this chat room.
     *
     * @return The name of this chat room.
     */
    public String getRoomName() {
        return this.roomName;
    }

    /**
     * Retrieve the list of Players in the chat room.
     *
     * @return The list of members in this chat room.
     */
    public List<Player> getPlayerList() {
        return this.playerList;
    }

    /**
     * Check if the room is hidden. If the room is hidden, the room should never
     * be listed to players not in the room.
     *
     * @return Whether or not the room is hidden.
     */
    public boolean isHidden() {
        return this.hidden;
    }

    /**
     * Remove a player from the chat room. Only call this method from Database,
     * otherwise it will fall out of sync.
     *
     * @param player
     *            The player to remove. Should already be a member of this chat
     *            room.
     */
    void removePlayer(Player player) {
        this.playerList.remove(player);
    }

    /**
     * Add a player to the chat room. Only call this method from Database,
     * otherwise it will fall out of sync.
     *
     * @param player
     *            The player to add. Should NOT already be a member of this chat
     *            room.
     */
    void addPlayer(Player player) {
        if (!this.playerList.contains(player)) {
            this.playerList.add(player);
        }
    }

    @Override
    public String toString() {
        return this.roomName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ChatRoom)) {
            return false;
        } else {
            return ((ChatRoom) obj).roomId == this.roomId;
        }
    }

    @Override
    public int hashCode() {
        return this.roomId;
    }
}
