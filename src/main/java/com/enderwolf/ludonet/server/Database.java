package com.enderwolf.ludonet.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ibatis.jdbc.ScriptRunner;

import sun.awt.Mutex;

import com.enderwolf.ludonet.shared.MD5;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.Scoreboard;

/**
 * The primary interface for retrieving and storing data persistently. The class
 * is designed to allow for multiple instances to access the data across
 * threads. Exclusive access is enforced by acquiring a statically shared mutex
 * in all PUBLIC methods, and *never* calling other public methods from within
 * public methods. Private methods therefore needn't acquire the mutex.
 *
 * TODO (consider): This class is a love-song to Boilerplate.
 */
public class Database {
    /**
     * The ChatRoom cache will mostly be accessed in bulk, and the ChatRooms
     * themselves are frequently changed within the Database class. Access must
     * therefore be locked on a bulk-basis. Before accessing ANY ChatRoom data,
     * lock the ChatRoomCache. When done reading, altering or whatever, unlock
     * the ChatRoomCache.
     */
    public static class ChatRoomCache {
        private static Mutex mutex = new Mutex();

        // The ChatRooms are mapped to their unique IDs. The IDs are hidden from
        // the public interface. The Map is
        // assigned in the Database constructor.
        static Map<Integer, ChatRoom> chatRooms = null;

        private boolean lock = false;

        /**
         * Lock the ChatRoomCache. This prevents access from Database or other
         * holders of a ChatRoomCache instance. The class should stay locked
         * until ALL ChatRoom-related processing is done. In other words, if you
         * need to store ANY data retrieved from a ChatRoom for a longer period
         * of time, make a copy of the data and unlock the cache.
         */
        public void lock() {
            if (!this.lock) {
                ChatRoomCache.mutex.lock();
                this.lock = true;
            }
        }

        /**
         * Unlock the ChatRoomCache. This allows Database and other holders of a
         * ChatRoomCache instance to access the data. Unlock the class once
         * you're done with all ChatRoom related processing.
         */
        public void unlock() {
            if (this.lock) {
                ChatRoomCache.mutex.unlock();
                this.lock = false;
            }
        }

        /**
         * Retrieve the ChatRooms.
         *
         * @return The list of ChatRooms. If the class is not locked, null is
         *         returned (rtfm).
         */
        public List<ChatRoom> getChatRooms() {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            return new ArrayList<>(ChatRoomCache.chatRooms.values());
        }

        /**
         * Retrieve a ChatRoom given the ID of the room.
         *
         * @param chatRoomId
         *            The ID of the room to retrieve.
         * @return The chat room if the ID exists and the class is locked, null
         *         otherwise.
         */
        public ChatRoom getChatRoom(int chatRoomId) {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            return ChatRoomCache.chatRooms.get(chatRoomId);
        }

        /**
         * Retrieve the map of ChatRooms.
         *
         * @return The map of chat rooms if the class is locked, null otherwise
         *         (rtfm).
         */
        Map<Integer, ChatRoom> getChatRoomMap() {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            return ChatRoomCache.chatRooms;
        }

        /**
         * Adds a chat room to the chat room map.
         *
         * @param room
         *            The room to be added to the map.
         * @return True if the room did not exist and the class is locked. False
         *         if the class is not locked (rtfm), or the room already exists
         *         in the cache.
         */
        boolean addChatRoom(ChatRoom room) {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            if (ChatRoomCache.chatRooms.containsKey(room.getRoomId())) {
                return false;
            }

            ChatRoomCache.chatRooms.put(room.getRoomId(), room);
            return true;
        }

        /**
         * Remove a chat room form the cache.
         *
         * @param chatRoomId
         *            The room to be removed.
         * @return True if the room exists and the class is locked, false
         *         otherwise (rtfm).
         */
        boolean removeChatRoom(int chatRoomId) {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            if (!ChatRoomCache.chatRooms.containsKey(chatRoomId)) {
                return false;
            }

            ChatRoomCache.chatRooms.remove(chatRoomId);
            return true;
        }

        /**
         * Clear the cache. All entries are removed.
         */
        void clearCache() {
            if (!this.lock) {
                throw new ConcurrentModificationException();
            }

            ChatRoomCache.chatRooms.clear();
        }
    }

    /*
     * DO NOT USE DIRECTLY! Retrieve the name of the database from
     * Database#getDatbaseName() instead.
     */
    protected static final String DB_NAME = "LudoDB";
    private static final String DB_INIT_SCRIPT_PATH = "/com/enderwolf/ludonet/resources/LudoDB.sql";

    private static boolean INIT = false;
    private static final Logger LOGGER = Logger.getGlobal();
    private static Mutex dbMutex = new Mutex();

    // Player lookups from the database are cached for faster lookup in the
    // future. The key is the PlayerID.
    // Must only ever be accessed within a lock of dbMutex.
    protected static Map<Integer, Player> playerCache = new HashMap<>();

    // Username lookups for players are cached and mapped to that player's
    // PlayerID. Must only ever be accessed
    // within a lock of dbMutex.
    protected static Map<String, Integer> userNameCache = new HashMap<>();

    // ChatRoom cache.
    protected static ChatRoomCache chatRoomCache = new ChatRoomCache();

    /**
     * Create a Database instance. The Derby-Database will be created if it does
     * not already exist.
     */
    public Database() {
        this.initialize();
    }

    /**
     * Constructor for delayed instantiation. Do not use under "real"
     * circumstances, this constructor only exists to accommodate test needs.
     *
     * @param initialize
     *            Whether or not to initialize right here and now.
     */
    protected Database(boolean initialize) {
        if (initialize) {
            this.initialize();
        }
    }

    /**
     * Verify the username and password combination of a potentially existing
     * user.
     *
     * @param userName
     *            The name of the user in plain text
     * @param password
     *            The password of the user
     * @return True if the credentials exists and match.
     */
    public boolean verifyLogin(String userName, String password) {
        Database.dbMutex.lock();
        boolean verified = false;

        try {
            final Connection con = this.connectToDatabase();

            final String query = "SELECT COUNT(*) as count " + "FROM Player " + "WHERE name=? AND pass=?";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, userName);
            stmt.setString(2, MD5.hash(password));

            final ResultSet result = stmt.executeQuery();
            while (result.next()) {
                final int count = result.getInt("count");
                verified = (count == 1);
            }

            result.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "SQLError in Database#verifyLogin(String,String)", sqle);
            verified = false;
        }

        Database.dbMutex.unlock();
        return verified;
    }

    /**
     * Register a new user in the database. A user with the specified name can
     * not already exist.
     *
     * @param userName
     *            The name of the user.
     * @param password
     *            The password of the user.
     * @return True on success, false if the user exists or upon error.
     */
    public boolean registerUser(String userName, String password) {
        Database.dbMutex.lock();
        boolean success = false;

        if (!this.doesUserExist(userName)) {
            try {
                final Connection con = this.connectToDatabase();

                final String query = "INSERT INTO Player(name, pass) " + "VALUES(?, ?)";
                final PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, userName);
                stmt.setString(2, MD5.hash(password));
                stmt.execute();

                con.close();
                stmt.close();

                success = true;
            } catch (final SQLException sqle) {
                Database.LOGGER.log(Level.SEVERE, "SQLError in Database#verifyLogin(String,String)", sqle);
            }
        } else {
            Database.LOGGER.log(Level.INFO, "Unable to create user '" + userName + "': user already exists");
        }

        Database.dbMutex.unlock();
        return success;
    }

    /**
     * Get a Player-instance given a user name.
     *
     * @param userName
     *            The username of the client.
     * @return A valid Player object if the username is valid, null otherwise.
     */
    public Player getPlayer(String userName) {
        Database.dbMutex.lock();
        final Player player = this.constructPlayer(userName);
        Database.dbMutex.unlock();

        return player;
    }

    /**
     * Increment Player.tot_games by 1. If the given player also won the game,
     * increment Player.tot_wins by as well.
     *
     * @param player
     *            The player to give points to.
     * @param playerDidWin
     *            Indicator if the player won the game or not.
     * @return True on success, false on error.
     */
    public boolean awardPoints(Player player, boolean playerDidWin) {
        Database.dbMutex.lock();
        boolean success = false;

        try {
            final Connection con = this.connectToDatabase();

            String query = "UPDATE Player SET tot_games = tot_games + 1 ";
            if (playerDidWin) {
                query += ", tot_wins = tot_wins + 1 ";
            }

            query += "WHERE id = ?";

            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, player.getPlayerId());

            final int affected = stmt.executeUpdate();
            success = (affected == 1);

        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.WARNING, "Unable to increment score values of player " + player.getUserName(),
                    sqle);
        }

        Database.dbMutex.unlock();
        return success;
    }

    /**
     * Retrieve a chunk of the high score list for the total number of games
     * played.
     *
     * @param firstIndex
     *            The first entry to retrieve.
     * @param count
     *            The number of entries to retrieve.
     * @return A Scoreboard containing the desired entries on success, null on
     *         error / out of bounds.
     */
    public Scoreboard getTotalGamesScoreboard(int firstIndex, int count) {
        return this.getScoreboard(firstIndex, count, "tot_games");
    }

    /**
     * Retrieve a chunk of the high score list for the number of wins.
     *
     * @param firstIndex
     *            The first entry to retrieve.
     * @param count
     *            The number of entries to retrieve.
     * @return A Scoreboard containing the desired entries on success, null on
     *         error / out of bounds.
     */
    public Scoreboard getTotalWinsScoreboard(int firstIndex, int count) {
        return this.getScoreboard(firstIndex, count, "tot_wins");
    }

    /**
     * Create a chat room with a given name. The created chat room will not be
     * hidden.
     *
     * @param name
     *            The name of the chat room.
     * @return The ID of the newly created chat room, 0 on error.
     * @see Database#createChatRoom(String,boolean)
     */
    public int createChatRoom(String name) {
        return this.createChatRoom(name, false);
    }

    /**
     * Create a new chat room. The name of the chat room does not need to be
     * unique.
     *
     * @param name
     *            The name of the new chat room. The name must be at least 3
     *            characters long and not contain trailing or leading
     *            whitespace.
     * @param hidden
     *            Whether or not this room is hidden. Hidden rooms are not
     *            exposed when listing rooms.
     * @return The ID of the newly created chat room, 0 if the room could not be
     *         created.
     */
    public int createChatRoom(String name, boolean hidden) {
        int roomId = 0;

        final String roomName = name.trim();
        if (roomName.length() < 3) {
            return 0;
        }

        Database.dbMutex.lock();

        try {
            final Connection con = this.connectToDatabase();
            final String query = "INSERT INTO ChatRoom(name, hidden) VALUES(?, ?)";
            final PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, name);
            stmt.setInt(2, hidden ? 0 : 1);

            final int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating chat room failed, no affected rows");
            }

            final ResultSet res = stmt.getGeneratedKeys();
            while (res.next()) {
                roomId = res.getInt(1);
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.INFO, "Unable to create chat room '" + name + "'", sqle);
        }

        if (roomId != 0) {
            Database.chatRoomCache.lock();
            final ChatRoom room = this.getChatRoom(roomId);
            if (room != null) {
                Database.chatRoomCache.addChatRoom(room);
            }
            Database.chatRoomCache.unlock();
        }

        Database.dbMutex.unlock();
        return roomId;
    }

    /**
     * Join a player into a chat room. The player is added to the cached
     * ChatRoom instance and the database.
     *
     * @param chatRoomId
     *            The ID of the chat room to be joined.
     * @param player
     *            The player to join the chat room.
     * @return True if the player joined successfully, false if the player
     *         already was in the room or one of the IDs are invalid - or upon
     *         other errors.
     */
    public boolean joinChatRoom(int chatRoomId, Player player) {
        boolean success = false;

        if (this.isUserInChatRoom(chatRoomId, player)) {
            return false;
        }

        Database.chatRoomCache.lock();

        final ChatRoom chatRoom = Database.chatRoomCache.getChatRoom(chatRoomId);

        Database.LOGGER.info(chatRoom.getRoomName() + "\tSize:" + chatRoom.getPlayerList().size());

        if (chatRoom != null) {
            chatRoom.addPlayer(player);
            success = true;
        }

        Database.LOGGER.info(chatRoom.getRoomName() + "\tSize:" + chatRoom.getPlayerList().size());

        Database.chatRoomCache.unlock();

        return success;
    }

    /**
     * Detach a Player from a chat room. This call updates both the cached
     * ChatRooms and the database.
     *
     * @param chatRoomId
     *            The ID of the chat room to leave.
     * @param player
     *            The player to leave the chat room.
     * @return True if the Player was in the chat room and has now departed,
     *         false if he was not in the room or upon other error.
     */
    public boolean leaveChatRoom(int chatRoomId, Player player) {
        boolean success = false;

        if (!this.isUserInChatRoom(chatRoomId, player)) {
            return false;
        }

        Database.chatRoomCache.lock();

        final ChatRoom chatRoom = Database.chatRoomCache.getChatRoom(chatRoomId);

        if (chatRoom != null) {
            chatRoom.removePlayer(player);
            success = true;
        }

        Database.chatRoomCache.unlock();

        return success;
    }

    /**
     * Create a new instance of a ChatRoomCache. Read the docs of ChatRoomCache
     * carefully!
     *
     * @return A fresh instance of a ChatRoomCache.
     */
    public ChatRoomCache getChatRoomCache() {
        return new ChatRoomCache();
    }

    /**
     * Return a list of the player IDs in a specified chat room. The results are
     * freshly generated each call by taking a copy of the list found in the
     * ChatRoomCache.
     *
     * @param chatRoomId
     *            The chat room to return the member list of.
     * @return List of the connected Players in the chatroom, null if the room
     *         does not exist.
     */
    public List<Player> getPlayersInChatRoom(int chatRoomId) {
        List<Player> players = null;

        Database.chatRoomCache.lock();

        final ChatRoom chatRoom = Database.chatRoomCache.getChatRoom(chatRoomId);
        if (chatRoom != null) {
            final List<Player> originalList = chatRoom.getPlayerList();
            if (originalList != null) {
                players = new ArrayList<>(originalList);
            }
        }

        Database.chatRoomCache.unlock();

        return players;
    }

    /**
     * Insert a chat message into the database. The message is trimmed down to
     * 256 characters in length if it exceeds this limit.
     *
     * @param player
     *            The player who said something.
     * @param chatRoomId
     *            The room in which something was said.
     * @param message
     *            What was being said.
     * @return True if the message was inserted successfully, false if something
     *         went wrong.
     */
    public boolean addChatMessage(Player player, int chatRoomId, String message) {
        Database.dbMutex.lock();
        boolean success = false;

        if (this.isUserInChatRoom(chatRoomId, player)) {
            try {
                final Connection con = this.connectToDatabase();
                final String query = "INSERT INTO ChatMessage(room_id, player_id, msg) " + "VALUES(?, ?, ?) ";
                final PreparedStatement stmt = con.prepareStatement(query);
                stmt.setInt(1, chatRoomId);
                stmt.setInt(2, player.getPlayerId());
                stmt.setString(3, message.substring(0, Math.min(255, message.length())));

                final int affected = stmt.executeUpdate();
                if (affected == 1) {
                    success = true;
                }

                con.close();
                stmt.close();
            } catch (final SQLException sqle) {
                Database.LOGGER.log(Level.SEVERE, "Unable to add chat message", sqle);
            }
        }

        Database.dbMutex.unlock();
        return success;
    }

    /**
     * Bond a friendship. The two players will be joined in holy friendship.
     *
     * @param p1
     *            One of the players.
     * @param p2
     *            The other player.
     * @return True if the players were not already friends, are not the same
     *         object and the players now are joined in jolly cooperation. False
     *         otherwise.
     */
    public boolean makeFriends(Player p1, Player p2) {
        if (p1 == null || p2 == null || p1.getPlayerId() == p2.getPlayerId()) {
            return false;
        }

        boolean success = false;

        // The first playerID is required to be lower than the second playerID.
        // Se LudoDB.sql#Friends for more details.
        final int id1 = Math.min(p1.getPlayerId(), p2.getPlayerId());
        final int id2 = Math.max(p1.getPlayerId(), p2.getPlayerId());

        Database.dbMutex.lock();

        try {
            final Connection con = this.connectToDatabase();
            final String query = "INSERT INTO Friends(player_a, player_b) VALUES(?, ?)";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id1);
            stmt.setInt(2, id2);

            final int affected = stmt.executeUpdate();
            if (affected == 1) {
                success = true;
            }

            con.close();
            stmt.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to join two players in jolly cooperation!", sqle);
        }

        Database.dbMutex.unlock();
        return success;
    }

    /**
     * Break a bond of friendship.
     *
     * @param p1
     *            Half of the friendship.
     * @param p2
     *            The other half of the friendship (given that they both give
     *            equally, who are we to judge?).
     * @return True if the players WERE friends, and are now split. False if
     *         they were not friends or the database is acting up.
     */
    public boolean unmakeFriends(Player p1, Player p2) {
        if (p1 == null || p2 == null || p1.getPlayerId() == p2.getPlayerId()) {
            return false;
        }

        boolean success = false;

        // The first playerID is required to be lower than the second playerID.
        // Se LudoDB.sql#Friends for more details.
        final int id1 = Math.min(p1.getPlayerId(), p2.getPlayerId());
        final int id2 = Math.max(p1.getPlayerId(), p2.getPlayerId());

        Database.dbMutex.lock();

        try {
            final Connection con = this.connectToDatabase();
            final String query = "DELETE FROM Friends " + "WHERE player_a = ? " + "AND player_b = ? ";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id1);
            stmt.setInt(2, id2);

            final int affected = stmt.executeUpdate();
            if (affected > 0) {
                success = true;
            }

            con.close();
            stmt.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to break apart friends!", sqle);
        }

        Database.dbMutex.unlock();
        return success;
    }

    /**
     * Retrieve the friends of a player.
     *
     * @param player
     *            The Player whose friends list we are retrieving.
     * @return A list containing all friends of this player.
     */
    public List<Player> getFriends(Player player) {
        Database.dbMutex.lock();
        final List<Player> friends = new ArrayList<>();

        final int playerId = player.getPlayerId();

        try {
            final Connection con = this.connectToDatabase();
            final String query = "SELECT player_a, player_b FROM Friends " + "WHERE player_a = ? OR player_b = ? ";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, playerId);
            stmt.setInt(2, playerId);

            final ResultSet res = stmt.executeQuery();
            while (res.next()) {
                final int id1 = res.getInt(1);
                final int id2 = res.getInt(2);

                final int otherId = (id1 == playerId) ? id2 : id1;
                final Player friend = this.constructPlayer(otherId);
                if (friend != null) {
                    friends.add(friend);
                }
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER
                    .log(Level.SEVERE, "Unable to retrieve friends list of '" + player.getUserName() + "'", sqle);
        }

        Database.dbMutex.unlock();
        return friends;
    }

    /**
     * Clear the cache of Player-instances.
     */
    public void clearCache() {
        Database.userNameCache.clear();
        Database.playerCache.clear();

        Database.chatRoomCache.lock();
        Database.chatRoomCache.clearCache();
        Database.chatRoomCache.unlock();
    }

    /**
     * Initialize the instance. The database is created on disk if it doesn't
     * yet exist, and the ChatRoomCache is populated with the contents of the
     * database. This is the ONLY non-public method which utilizes the dbMutex.
     */
    protected void initialize() {
        if (Database.INIT) {
            return;
        }

        Database.dbMutex.lock();

        if (!this.createIfNeeded()) {
            Database.LOGGER.log(Level.SEVERE, "Database: Unable to create database or the tables therein");
        }

        this.fillChatRoomCache();
        Database.dbMutex.unlock();

        Database.chatRoomCache.lock();
        final ChatRoom global = Database.chatRoomCache.getChatRoom(1);
        Database.chatRoomCache.unlock();

        if (global == null) {
            this.createChatRoom("Global Chat");
        }

        Database.INIT = true;
    }

    /**
     * Connect to the database. The returned Connection instance must be closed
     * as soon as the caller is done with it, and it's life-cycle must *ALWAYS*
     * be within a scope locked by the static mutex.
     *
     * @return A valid database connection on success, null on error.
     */
    protected Connection connectToDatabase() {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection("jdbc:derby:" + this.getDatabaseName() + ";create=true;");
            conn.setAutoCommit(true);
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to establish database connection to " + this.getDatabaseName(),
                    sqle);
        }

        return conn;
    }

    /**
     * Retrieve the name of the Database.
     *
     * @return The name of the database.
     */
    protected String getDatabaseName() {
        return Database.DB_NAME;
    }

    /**
     * Retrieve the class-global static mutex used for accessing the database.
     *
     * @return The mutex used to limit access to the databse.
     */
    protected Mutex getDatabaseMutex() {
        return Database.dbMutex;
    }

    /**
     * Check if a user exists.
     *
     * @param userName
     *            The name of the potentially existing user.
     * @return True if the user exists, false if user does not exist or upon
     *         error.
     */
    private boolean doesUserExist(String userName) {
        boolean exists = false;

        try {
            final Connection con = this.connectToDatabase();

            final String query = "SELECT COUNT(*) AS count " + "FROM Player " + "WHERE name=?";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, userName);
            final ResultSet result = stmt.executeQuery();

            while (result.next()) {
                final int count = result.getInt("count");
                exists = (count == 1);
            }

            result.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "SQLError in Database#verifyLogin(String,String)", sqle);
            exists = false;
        }

        return exists;
    }

    /**
     * Check if a player is connected to a chat room.
     *
     * @param chatRoomId
     *            The ID of the chat room.
     * @param player
     *            The Player whose connectivity we are interested in.
     * @return True if the player is in the chat room, false on error or if he
     *         is not connected.
     */
    private boolean isUserInChatRoom(int chatRoomId, Player player) {
        boolean connected = false;

        Database.chatRoomCache.lock();

        final ChatRoom room = Database.chatRoomCache.getChatRoom(chatRoomId);
        if (room != null) {
            connected = room.getPlayerList().contains(player);
        }

        Database.chatRoomCache.unlock();

        return connected;
    }

    /**
     * Construct a Player object from a user name. If the player object is
     * already cached, the cached instance is returned. Otherwise, a new
     * instance is created and added to the cache.
     *
     * @param userName
     *            The user name of the player.
     * @return Instantiated Player object on success, null on error.
     */
    private Player constructPlayer(String userName) {
        Player player = this.getCachedPlayer(userName);
        if (player != null) {
            return player;
        }

        try {
            final Connection con = this.connectToDatabase();

            final String query = "SELECT id FROM Player WHERE name=?";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, userName);

            final ResultSet res = stmt.executeQuery();
            while (res.next()) {
                final int id = res.getInt("id");
                player = new Player(id, userName);
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.INFO, "Unable to construct Player object '" + userName + "'", sqle);
        }

        this.addPlayerToCache(player);

        return player;
    }

    /**
     * Construct a Player object from a player ID. If the player object is
     * already cached, the cached instance is returned. Otherwise, a new
     * instance is created and added to the cache.
     *
     * @param playerId
     *            The ID of the player.
     * @return Instantiated Player object on success, null on error.
     */
    private Player constructPlayer(int playerId) {
        Player player = this.getCachedPlayer(playerId);
        if (player != null) {
            return player;
        }

        try {
            final Connection con = this.connectToDatabase();

            final String query = "SELECT name FROM Player WHERE id=?";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, playerId);

            final ResultSet res = stmt.executeQuery();
            while (res.next()) {
                final String name = res.getString(1);
                player = new Player(playerId, name);
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.INFO, "Unable to construct Player object '" + playerId + "'", sqle);
        }

        this.addPlayerToCache(player);

        return player;
    }

    /**
     * Retrieve a cached Player-instance given a username.
     *
     * @param userName
     *            The user name of the player.
     * @return The cached instance if one exists, null otherwise.
     */
    private Player getCachedPlayer(String userName) {
        if (Database.userNameCache.containsKey(userName)) {
            return this.getCachedPlayer(Database.userNameCache.get(userName));
        }

        return null;
    }

    /**
     * Retrieve a cached Player-instance given a PlayerID.
     *
     * @param playerId
     *            The ID of the player.
     * @return The cached instance if one exists, null otherwise.
     */
    private Player getCachedPlayer(int playerId) {
        return Database.playerCache.get(playerId);
    }

    /**
     * Add a player instance to 'userNameCache' and 'playerCache' for faster
     * lookup in the future. If the player instance is null or already exists in
     * the cache, no work is done.
     *
     * @param player
     *            The Player instance to store in the cache.
     */
    private void addPlayerToCache(Player player) {
        if (player == null) {
            return;
        }

        if (!Database.userNameCache.containsKey(player.getUserName())) {
            Database.userNameCache.put(player.getUserName(), player.getPlayerId());
        }

        if (!Database.playerCache.containsKey(player.getPlayerId())) {
            Database.playerCache.put(player.getPlayerId(), player);
        }
    }

    /**
     * Fill the static List chatRoomCache with all the chat rooms defined in the
     * database. The existing cache is cleared by this method and all ChatRooms
     * recreated.
     */
    private void fillChatRoomCache() {
        final List<Integer> roomIds = new ArrayList<>();

        try {
            final Connection con = this.connectToDatabase();
            final String query = "SELECT id FROM ChatRoom";
            final PreparedStatement stmt = con.prepareStatement(query);

            final ResultSet res = stmt.executeQuery();
            while (res.next()) {
                roomIds.add(res.getInt(1));
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to retrieve list of chat rooms", sqle);
        }

        Database.chatRoomCache.lock();

        // Retrieve the actual chat rooms
        ChatRoomCache.chatRooms = new HashMap<Integer, ChatRoom>();

        for (final int id : roomIds) {
            final ChatRoom room = this.getChatRoom(id);
            if (room != null) {
                Database.chatRoomCache.addChatRoom(room);
            }
        }

        Database.chatRoomCache.unlock();
    }

    /**
     * Retrieve a ChatRoom instance. Note that this method creates a new
     * ChatRoom instance without any users connected to it. This method should
     * therefore ONLY be called during initialization for existing chat rooms.
     *
     * @param chatRoomId
     *            The ID of the room to create.
     * @return An instantiated ChatRoom on success, null on error.
     */
    private ChatRoom getChatRoom(int chatRoomId) {
        // The ChatRoomCache may not be locked at this time. If it is not
        // locked, getChatRoom will return null.
        ChatRoom room = Database.chatRoomCache.getChatRoom(chatRoomId);
        if (room != null) {
            return room;
        }

        String roomName = null;
        boolean hidden = false;

        try {
            final Connection con = this.connectToDatabase();
            final String query = "SELECT name, hidden FROM ChatRoom WHERE id = ?";
            final PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, chatRoomId);

            final ResultSet res = stmt.executeQuery();
            while (res.next()) {
                roomName = res.getString(1);
                hidden = (res.getInt(2) == 0);
            }

            res.close();
            stmt.close();
            con.close();
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to create ChatRoom", sqle);
        }

        if (roomName != null) {
            room = new ChatRoom(chatRoomId, roomName, hidden);
            Database.chatRoomCache.addChatRoom(room);
        }

        return room;
    }

    /**
     * Retrieve a specific scoreboard for a given Player-column (tot_games or
     * tot_wins).
     *
     * @param firstIndex
     *            The first entry to retrieve.
     * @param count
     *            The number of entries to retrieve.
     * @param column
     *            The name of the column in Player to retrieve. Must be
     *            "tot_games" or "tot_wins".
     * @return A Scoreboard containing the desired entries on success, null on
     *         error / out of bounds.
     */
    private Scoreboard getScoreboard(int firstIndex, int count, String column) {
        Scoreboard scoreboard = null;

        if (!"tot_games".equals(column) && !"tot_wins".equals(column)) {
            return null;
        }

        try {
            final Connection con = this.connectToDatabase();

            final String query = String.format("SELECT name, %s\n" + "FROM Player\n" + "ORDER BY %s DESC\n"
                    + "OFFSET %d ROWS FETCH NEXT %s ROWS ONLY", column, column, firstIndex, count);
            final Statement stmt = con.createStatement();
            final ResultSet res = stmt.executeQuery(query);

            scoreboard = new Scoreboard();
            final List<Scoreboard.Entry> entries = new ArrayList<>();

            while (res.next()) {
                final String name = res.getString(1);
                final int score = res.getInt(2);
                entries.add(new Scoreboard.Entry(name, score));
            }

            scoreboard.setEntries(entries, firstIndex);
        } catch (final SQLException sqle) {
            Database.LOGGER.log(Level.SEVERE, "Unable to retrieve the high score lists", sqle);
        }

        return scoreboard;
    }

    /**
     * /** Creates the database and the table if it / they doesn't already
     * exist. Proceeding any changes in LudoDB.sql, the database files
     * themselves must be deleted ($PROJECT_ROOT/LudoDB/).
     *
     * @return True if the DB+tables were created or already existed, false if
     *         any error occurred.
     */
    private boolean createIfNeeded() {
        try {
            final Connection con = this.connectToDatabase();
            if (con == null) {
                return false;
            }

            final ScriptRunner scriptRunner = new ScriptRunner(con);
            scriptRunner.setStopOnError(true);
            scriptRunner.setAutoCommit(true);

            final InputStream is = this.getClass().getResourceAsStream(Database.DB_INIT_SCRIPT_PATH);
            final InputStreamReader reader = new InputStreamReader(is);

            scriptRunner.runScript(reader);
            reader.close();
            con.close();

            Database.LOGGER.log(Level.INFO, "Database and tables created successfully");
        } catch (final SQLException sqle) {
            /*
             * This is some high level bullshit. The SQL state "X0Y32" indicates
             * that an attempted created table already exists. Bar auxiliary
             * files, this is (believe it or not) the best practice to 'CREATE
             * TABLE IF NOT EXISTS' in Derby.
             *
             * See: http://db.apache.org/derby/docs/10.8/ref/rre
             * except71493.html
             * http://stackoverflow.com/questions/5866154/how-to
             * -create-table-if-it-doesnt-exist-using-derby-db
             */
            if ("X0Y32".equals(sqle.getSQLState())) {
                return true;
            }

            Database.LOGGER.log(Level.SEVERE, "SQL Exception", sqle);
            return false;
        } catch (final FileNotFoundException e) {
            Database.LOGGER.log(Level.SEVERE, "Unable to open SQL script file", e);
            return false;
        } catch (final IOException e) {
            Database.LOGGER.log(Level.SEVERE, "Unknown IOException", e);
            return false;
        } catch (final Exception e) {
            Database.LOGGER.log(Level.SEVERE, "Arbitrary fkn exception", e);
        }

        return true;
    }

}
