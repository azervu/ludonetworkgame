package com.enderwolf.ludonet.server;

import java.util.ArrayList;
import java.util.List;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.server.Database.ChatRoomCache;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.Scoreboard;
import com.enderwolf.ludonet.shared.Scoreboard.SCOREBOARD_TYPE;

/**
 * Manages all messages related to the lobby on the server side.
 *
 * @author !Tulingen
 */
public class LobbyManager {

    private static Database database = new Database();

    /**
     *
     * @param player
     * @param message
     */
    public static void onChat(Player player, Message message) {
        final int chatRoomId = Integer.valueOf(message.getEntry(EntryType.CHAT_ROOM_ID));
        final List<Player> players = LobbyManager.database.getPlayersInChatRoom(chatRoomId);
        LobbyManager.database.addChatMessage(player, chatRoomId, message.getEntry(EntryType.CHAT_TEXT));
        players.forEach(p -> p.sendMessage(message));
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onListChatRooms(Player player, Message message) {
        final ChatRoomCache cache = LobbyManager.database.getChatRoomCache();

        cache.lock();
        final List<ChatRoom> rooms = cache.getChatRooms();

        final List<Integer> ids = new ArrayList<>(rooms.size() / 2);
        final List<String> names = new ArrayList<>(rooms.size() / 2);

        for (int i = 0; i < rooms.size(); i++) {
            final ChatRoom room = rooms.get(i);
            if (!room.isHidden()) {
                ids.add(room.getRoomId());
                names.add(room.getRoomName());
            }
        }

        cache.unlock();

        final Message returnMessage = new Message(MessageType.CHAT_ROOMS);
        returnMessage.addArray(EntryType.CHAT_ROOM_ID, ids.toArray());
        returnMessage.addArray(EntryType.CHAT_ROOM_NAME, names.toArray());

        player.sendMessage(returnMessage);
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onChatRoomCreate(Player player, Message message) {
        final int id = LobbyManager.database.createChatRoom(message.getEntry(EntryType.CHAT_ROOM_NAME));

        message.addEntry(EntryType.CHAT_ROOM_ID, id);

        LobbyManager.onChatRoomJoin(player, message);
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onChatRoomJoin(Player player, Message message) {
        final int chatRoomId = Integer.valueOf(message.getEntry(EntryType.CHAT_ROOM_ID));

        final ChatRoomCache cache = LobbyManager.database.getChatRoomCache();

        cache.lock();
        final ChatRoom room = cache.getChatRoom(chatRoomId);
        String chatRoomName = "";
        if (room != null) {
            chatRoomName = room.getRoomName();
        }
        cache.unlock();

        if (room == null) {
            return;
        }

        final List<Player> players = LobbyManager.database.getPlayersInChatRoom(chatRoomId);

        final Integer[] idPlayer = new Integer[1];
        final String[] namePlayer = new String[1];
        final Boolean[] inRoomPlayer = new Boolean[1];

        idPlayer[0] = player.getPlayerId();
        namePlayer[0] = player.getUserName();
        inRoomPlayer[0] = true;

        final Message returnMessage = new Message(MessageType.CHAT_ROOM_MEMBER_UPDATE);
        returnMessage.addEntry(EntryType.CHAT_ROOM_ID, chatRoomId);
        returnMessage.addEntry(EntryType.CHAT_ROOM_NAME, chatRoomName);
        returnMessage.addArray(EntryType.PLAYER_ID, idPlayer);
        returnMessage.addArray(EntryType.USER_NAME, namePlayer);
        returnMessage.addArray(EntryType.IN_CHAT_ROOM, inRoomPlayer);

        final boolean result = LobbyManager.database.joinChatRoom(chatRoomId, player);

        if (!result && chatRoomId != 1) {
            return;
        }

        final Integer[] ids = new Integer[players.size() + 1];
        final String[] names = new String[players.size() + 1];
        final Boolean[] inRoom = new Boolean[players.size() + 1];

        for (int i = 0; i < players.size(); i++) {
            ids[i] = players.get(i).getPlayerId();
            names[i] = players.get(i).getUserName();
            inRoom[i] = true;
        }

        ids[players.size()] = player.getPlayerId();
        names[players.size()] = player.getUserName();
        inRoom[players.size()] = true;

        final Message toJoiner = new Message(MessageType.CHAT_ROOM_MEMBER_UPDATE);
        toJoiner.addEntry(EntryType.CHAT_ROOM_ID, chatRoomId);
        toJoiner.addEntry(EntryType.CHAT_ROOM_NAME, chatRoomName);
        toJoiner.addArray(EntryType.PLAYER_ID, ids);
        toJoiner.addArray(EntryType.USER_NAME, names);
        toJoiner.addArray(EntryType.IN_CHAT_ROOM, inRoom);

        players.forEach(p -> p.sendMessage(returnMessage));
        player.sendMessage(toJoiner);
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onChatRoomLeave(Player player, Message message) {
        final int chatRoomId = Integer.valueOf(message.getEntry(EntryType.CHAT_ROOM_ID));
        LobbyManager.database.leaveChatRoom(chatRoomId, player);
        final List<Player> players = LobbyManager.database.getPlayersInChatRoom(chatRoomId);

        final Integer[] ids = new Integer[1];
        final String[] names = new String[1];
        final Boolean[] inRoom = new Boolean[1];

        ids[0] = player.getPlayerId();
        names[0] = player.getUserName();
        inRoom[0] = false;

        final ChatRoomCache cache = LobbyManager.database.getChatRoomCache();
        cache.lock();
        final String chatRoomName = cache.getChatRoom(chatRoomId).getRoomName();
        cache.unlock();

        final Message returnMessage = new Message(MessageType.CHAT_ROOM_MEMBER_UPDATE);
        returnMessage.addEntry(EntryType.CHAT_ROOM_ID, chatRoomId);
        returnMessage.addEntry(EntryType.CHAT_ROOM_NAME, chatRoomName);
        returnMessage.addArray(EntryType.PLAYER_ID, ids);
        returnMessage.addArray(EntryType.USER_NAME, names);
        returnMessage.addArray(EntryType.IN_CHAT_ROOM, inRoom);

        players.forEach(p -> p.sendMessage(returnMessage));
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onFriendList(Player player, Message message) {
        final List<Player> friends = LobbyManager.database.getFriends(player);

        final Integer[] ids = new Integer[friends.size()];
        final String[] names = new String[friends.size()];

        for (int i = 0; i < friends.size(); i++) {
            ids[i] = friends.get(i).getPlayerId();
            names[i] = friends.get(i).getUserName();
        }

        final Message toJoiner = new Message(MessageType.FRIEND_LIST_UPDATE);
        toJoiner.addArray(EntryType.PLAYER_ID, ids);
        toJoiner.addArray(EntryType.USER_NAME, names);

        player.sendMessage(toJoiner);
    }

    /**
     *
     * @param player
     * @param message
     */
    public static void onAddFriend(Player player, Message message) {
        final Player other = LobbyManager.database.getPlayer(message.getEntry(EntryType.USER_NAME));
        if (other == null) {
            return;
        }

        LobbyManager.database.makeFriends(player, other);

        LobbyManager.onFriendList(player, message);
        LobbyManager.onFriendList(other, message);
    }

    public static void onRemoveFriend(Player player, Message message) {
        final String[] otherNames = message.getArray(EntryType.USER_NAME);
        final Player[] others = new Player[otherNames.length];

        for (int i = 0; i < otherNames.length; i++) {
            others[i] = LobbyManager.database.getPlayer(otherNames[i]);
        }

        for (final Player other : others) {
            LobbyManager.database.unmakeFriends(player, other);
        }

        LobbyManager.onFriendList(player, message);
        for (final Player other : others) {
            LobbyManager.onFriendList(other, message);
        }
    }

    public static void onListScoreboard(Player player, Message message) {
        Scoreboard result;

        final SCOREBOARD_TYPE type = SCOREBOARD_TYPE.valueOf(message.getEntry(EntryType.SCOREBOARD_TYPE));
        final int firstIndex = Integer.valueOf(message.getEntry(EntryType.SCOREBOARD_INDEX));
        final int count = Integer.valueOf(message.getEntry(EntryType.SCOREBOARD_COUNT));

        if (type == SCOREBOARD_TYPE.TOTAL_GAMES) {
            result = LobbyManager.database.getTotalWinsScoreboard(firstIndex, count);
        } else {
            result = LobbyManager.database.getTotalGamesScoreboard(firstIndex, count);
        }

        final Message returnMessage = new Message(MessageType.SCOREBOARD);

        returnMessage.addEntry(EntryType.SCOREBOARD_TYPE, type);
        returnMessage.addEntry(EntryType.SCOREBOARD_INDEX, firstIndex);
        returnMessage.addEntry(EntryType.SCOREBOARD_SERIALIZED, result.serializeToString());

        player.sendMessage(returnMessage);
    }

    private LobbyManager() {
    }
}
