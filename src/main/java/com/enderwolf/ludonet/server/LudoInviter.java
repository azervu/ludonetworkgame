package com.enderwolf.ludonet.server;

import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.server.game.LudoController;
import com.enderwolf.ludonet.shared.ConnectionLostListener;
import com.enderwolf.ludonet.shared.Player;

/**
 *
 * This class handles the serverside of game invites. One is created per game
 * with invites.
 */
public class LudoInviter implements ConnectionLostListener {
    private static Database databaseInterface = new Database();
    private static final Logger LOGGER = Logger.getGlobal();
    private Vector<Player> readyPlayers;
    private Vector<Player> waitingPlayers;
    private final int numberOfPlayers;
    private int numberOfPlayersReady;
    private int numberOfPlayersCancelled;
    boolean gameStarted = false;

    /**
     * The constructor.
     *
     * @param player
     *            the player that is inviting the others
     * @param invitedNames
     *            the names of the other invited
     */
    public LudoInviter(Player player, String[] invitedNames) {

        this.numberOfPlayers = invitedNames.length + 1;
        if (this.numberOfPlayers > 4) {
            LudoInviter.LOGGER.log(Level.WARNING, "more than 3 players invited to game");
            player.sendMessage(new Message(MessageType.INVITE_CANCELLED));
            return;
        }

        this.numberOfPlayersCancelled = 0;
        this.readyPlayers = new Vector<Player>();
        this.waitingPlayers = new Vector<Player>();

        final boolean wasInGame = player.testAndSetInGame(true);
        if (wasInGame) {
            LudoInviter.LOGGER.log(Level.WARNING, "inviter already in game");
            player.sendMessage(new Message(MessageType.INVITE_CANCELLED));
            return;
        }
        this.readyPlayers.add(player);
        this.numberOfPlayersReady = 1;

        for (int i = 0; i < this.numberOfPlayers - 1; i++) {
            final Player temp = LudoInviter.databaseInterface.getPlayer(invitedNames[i]);
            if (!this.invitePlayer(player.getUserName(), temp)) {
                this.waitingPlayers.remove(this.waitingPlayers.lastElement());
                this.numberOfPlayersCancelled++;
                this.checkAndStartIfReady();
            } else {
                this.waitingPlayers.add(temp);
            }
        }

        final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.schedule(() -> {
            while (!LudoInviter.this.waitingPlayers.isEmpty()) {
                final Player p = LudoInviter.this.waitingPlayers.lastElement();

                p.sendMessage(new Message(MessageType.INVITE_CANCELLED));
                LudoInviter.this.waitingPlayers.remove(p);
                LudoInviter.this.numberOfPlayersCancelled++;
                p.testAndSetInGame(false);
            }
            LudoInviter.this.checkAndStartIfReady();

        }, 20000, TimeUnit.MILLISECONDS);

        for (final Player p : this.waitingPlayers) {
            p.setConnectionLostListener(this);
        }

        for (final Player p : this.readyPlayers) {
            p.setConnectionLostListener(this);
        }
    }

    @Override
    public void onPlayerDisconnected(Player player) {
        if (this.waitingPlayers.remove(player)) {
            this.numberOfPlayersCancelled++;
        } else if (this.readyPlayers.remove(player)) {
            this.numberOfPlayersReady--;
        }
        player.testAndSetInGame(false);
        this.checkAndStartIfReady();
    }

    private synchronized void checkAndStartIfReady() {
        if (this.numberOfPlayersCancelled == this.numberOfPlayers - 1) {
            this.readyPlayers.lastElement().sendMessage(new Message(MessageType.INVITE_CANCELLED));
            this.readyPlayers.lastElement().testAndSetInGame(false);
            return;
        }
        if (this.numberOfPlayersReady + this.numberOfPlayersCancelled == this.numberOfPlayers) {
            if (!this.gameStarted) {
                this.gameStarted = true;
                final LudoController lc = new LudoController(this.numberOfPlayersReady);

                while (!this.readyPlayers.isEmpty()) {
                    final Player p = this.readyPlayers.lastElement();
                    this.readyPlayers.remove(p);
                    lc.addPlayer(p, false);
                }
            }

        }
    }

    private void inviteResponse(Player player, Boolean answer) {

        this.waitingPlayers.remove(player);

        if (answer) {
            this.readyPlayers.add(player);
            this.numberOfPlayersReady++;
        } else {
            player.testAndSetInGame(false);
            this.numberOfPlayersCancelled++;
        }
        this.checkAndStartIfReady();

    }

    private boolean invitePlayer(String inviterName, Player player) {
        if (!player.isConnectionGood()) {
            return false;
        }
        final boolean wasInGame = player.testAndSetInGame(true);
        if (wasInGame) {
            LudoInviter.LOGGER.log(Level.WARNING, player.getUserName() + " already in a game");
            return false;
        }
        for (final Player p : this.waitingPlayers) {
            if (player == p) {
                return false;
            }
        }
        player.setMessageListener(MessageType.INVITE_RESPONSE, (p, m) -> {
            this.inviteResponse(p, String.valueOf(true).equals(m.getEntry(EntryType.ACCEPT_INVITE)));
        });
        final Message message = new Message(MessageType.INVITE);
        message.addEntry(EntryType.USER_NAME, player.getUserName(), true);
        player.sendMessage(message);
        return true;
    }
}
