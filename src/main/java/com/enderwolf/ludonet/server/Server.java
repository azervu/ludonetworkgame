/**
 *
 */
package com.enderwolf.ludonet.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.enderwolf.ludonet.client.Client;
import com.enderwolf.ludonet.server.game.LudoController;
import com.enderwolf.ludonet.shared.Player;

/**
 * @author !Tulingen
 *
 */
public class Server {
    private static final Logger LOGGER = Logger.getGlobal();
    private static LudoController queueGame;

    /**
     * Add a player to the queued game.
     *
     * @param player
     *            The player to add to the queued game.
     */
    public static synchronized boolean addToGameQueue(Player player) {
        if (Server.queueGame == null) {
            Server.queueGame = new LudoController(4);
        }

        if (!Server.queueGame.addPlayer(player, false)) {
            Server.LOGGER.log(Level.WARNING, "Unable to add player " + player.getUserName() + " to queue game");
            return false;
        }

        if (Server.queueGame.getPlayerCount() == 4) {
            Server.queueGame = new LudoController(4);
        }

        return true;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Server.LOGGER.setLevel(Level.ALL);

        try (InputStream is = Client.class
                .getResourceAsStream("/com/enderwolf/ludonet/shared/config/logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (final IOException ex) {
            Server.LOGGER.setLevel(Level.INFO);
            Server.LOGGER.log(Level.WARNING, "unable to load config file", ex);
        }

        new Server();
    }

    public Server() {
        new ServerNetworkHandler();
    }
}
