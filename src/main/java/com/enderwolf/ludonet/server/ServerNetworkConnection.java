package com.enderwolf.ludonet.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.NetworkConnection;
import com.enderwolf.ludonet.shared.Player;

/**
 * The server side network interface. This class encapsulates the connection and
 * communication protocol for communicating with the client. It is meant to be
 * handled by a single thread at a time.
 */
public class ServerNetworkConnection extends NetworkConnection {
    private static Logger LOGGER = Logger.getGlobal();

    private static String passwordHash(String password) {
        String hashedPassword = null;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getBytes());
            final byte[] bytes = messageDigest.digest();
            final StringBuilder sb = new StringBuilder();
            for (final byte b : bytes) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            hashedPassword = sb.toString();
        } catch (final NoSuchAlgorithmException e) {
            ServerNetworkConnection.LOGGER.log(Level.SEVERE, "Hashing algorithm invalid", e);
        }

        return hashedPassword;
    }

    /**
     * Creates a SSL connection to the provided socket, to complete the
     * connection establishment use updateLogin
     *
     * @param socket
     *            the socket to use when communicating
     */
    public ServerNetworkConnection(Socket socket) {
        super(false);

        this.connectionStatus = ConnectionStatus.AUTHENTICATION_PENDING;
        this.socket = socket;

        try {
            this.loginSocket = (SSLSocket) ((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket(socket,
                    null, socket.getPort(), false);
            this.loginSocket.setUseClientMode(false);
            this.reader = new BufferedReader(new InputStreamReader(this.loginSocket.getInputStream()));
            this.writer = new BufferedWriter(new OutputStreamWriter(this.loginSocket.getOutputStream()));
        } catch (final IOException e) {
            ServerNetworkConnection.LOGGER.log(Level.WARNING, "Error, IO execption on AUTHENTICATION_PENDING.", e);
        }
    }

    /**
     * updates and returns the network connection login state. should be called
     * until it returns CONNECTION_ESTABLISHED or CONNECTION_CLOSED.
     *
     * @return the status of the connection.
     */
    @Override
    public ConnectionStatus updateLogin() {
        switch (this.connectionStatus) {
            case AUTHENTICATION_PENDING:
                final Message msg = this.receiveMessage();
                if (msg != null && (msg.getType() == MessageType.LOGIN)) {
                    this.remoteConnectionName = msg.getSender();
                    final String receivedPassword = ServerNetworkConnection.passwordHash(msg
                            .getEntry(EntryType.PASSWORD));

                    final Database database = new Database();
                    Player player = database.getPlayer(this.remoteConnectionName);
                    boolean alreadyLoggedIn;

                    if (player == null) {
                        alreadyLoggedIn = false;
                    } else {
                        alreadyLoggedIn = player.isConnectionGood();
                    }

                    if (!alreadyLoggedIn && msg.getEntry(EntryType.CREATE_USER).equals(String.valueOf(true))) {
                        final boolean registered = database.registerUser(this.remoteConnectionName, receivedPassword);

                        if (registered) {
                            ServerNetworkConnection.LOGGER.log(Level.INFO, "new user created: "
                                    + this.remoteConnectionName);
                        } else {
                            ServerNetworkConnection.LOGGER.log(Level.INFO, "user name taken: "
                                    + this.remoteConnectionName);
                            final Message connectionMessage = new Message(MessageType.CONNECTION_STATUS);
                            connectionMessage.addEntry(EntryType.CONNECTION_STATUS,
                                    ConnectionStatus.AUTHENTICATION_FAILED.toString(), false);
                            this.transmitMessage(connectionMessage);
                            this.close();
                            return this.connectionStatus;
                        }

                        player = database.getPlayer(this.remoteConnectionName);
                    }

                    if (alreadyLoggedIn || !database.verifyLogin(this.remoteConnectionName, receivedPassword)) {
                        ServerNetworkConnection.LOGGER.log(Level.INFO, "password authentication failed");
                        final Message connectionMessage = new Message(MessageType.CONNECTION_STATUS);
                        connectionMessage.addEntry(EntryType.CONNECTION_STATUS,
                                ConnectionStatus.AUTHENTICATION_FAILED.toString(), false);
                        this.transmitMessage(connectionMessage);
                        this.close();
                        return this.connectionStatus;
                    }

                    ServerNetworkConnection.LOGGER.log(Level.INFO, "password authentication passed");
                    try {
                        final Message connectionMessage = new Message(MessageType.CONNECTION_STATUS);
                        connectionMessage.addEntry(EntryType.CONNECTION_STATUS,
                                ConnectionStatus.CONNECTION_ESTABLISHED.toString(), false);
                        connectionMessage.addEntry(EntryType.PLAYER_ID, "" + player.getPlayerId(), false);
                        this.transmitMessage(connectionMessage);
                        this.loginSocket.close();
                        this.writer.close();
                        this.reader.close();

                        this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
                        this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                        this.connectionStatus = ConnectionStatus.CONNECTION_ESTABLISHED;

                    } catch (final IOException e) {
                        ServerNetworkConnection.LOGGER.log(Level.WARNING,
                                "Error, IO execption on CONNECTION_ESTABLISHED.", e);
                        this.close();
                    } catch (final NullPointerException e) {
                        ServerNetworkConnection.LOGGER.log(Level.WARNING,
                                "Error, null pointer exception execption on connetcion.", e);
                        this.close();
                    }

                    return this.connectionStatus;

                }
                break;

            case AUTHENTICATION_FAILED:
            case CONNECTION_ESTABLISHED:
            case CONNECTION_CLOSED:
                break;

            default:
                break;
        }
        return this.connectionStatus;
    }
}
