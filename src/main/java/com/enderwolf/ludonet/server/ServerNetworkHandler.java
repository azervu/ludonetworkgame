package com.enderwolf.ludonet.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ServerSocketFactory;

import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.NetworkConnection;
import com.enderwolf.ludonet.shared.Player;

/**
 * The server side network interface. ServerNetworkHandler encapsulates the
 * threads handling the actual communication and provides a thread safe
 * interface for sending and receiving messages across the network.
 */
public class ServerNetworkHandler {
    private static final Logger LOGGER = Logger.getGlobal();

    private final Thread connectionThread;
    private final Thread loginThread;
    private final Database database;

    List<PlayerUpdaterTask> updaterList;
    List<Thread> updaterListThread;

    private final ArrayBlockingQueue<ServerNetworkConnection> waitingConnections;

    /**
     * The constructor sets up the threads handling the actual communication
     */
    public ServerNetworkHandler() {
        System.setProperty("javax.net.ssl.keyStore", "mySrvKeystore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");

        this.waitingConnections = new ArrayBlockingQueue<ServerNetworkConnection>(1000);

        this.updaterList = new ArrayList<PlayerUpdaterTask>();
        this.updaterListThread = new ArrayList<Thread>();

        this.makeNewUpdater();

        this.loginThread = new Thread(new ConnectionLoginTask());
        this.loginThread.setDaemon(true);
        this.loginThread.start();

        this.connectionThread = new Thread(new ConnectionListenerTask());
        this.connectionThread.setDaemon(true);
        this.connectionThread.start();

        this.database = new Database();
    }

    private void makeNewUpdater() {
        final PlayerUpdaterTask put = new PlayerUpdaterTask();
        final Thread thread = new Thread(put);
        thread.start();
        this.updaterList.add(put);
        this.updaterListThread.add(thread);
    }

    private void addConnection(ServerNetworkConnection connection) {
        boolean spaceFound = false;
        int index = 0;
        while (!spaceFound && index < this.updaterList.size()) {
            if (this.updaterList.get(index).isFull()) {
                index++;
            } else {
                spaceFound = true;
            }
        }
        if (index >= this.updaterList.size()) {
            this.makeNewUpdater();
        }
        this.updaterList.get(index).addConnection(connection);
    }

    /**
     * ConnectionUpdaterTask is meant to run on an independent thread and send
     * and receive messages from the main thread. It holds and updates
     * Connections, it gets them from another the main thread and closes them
     * itself when done.
     */
    private class PlayerUpdaterTask implements Runnable {
        private final ArrayBlockingQueue<Player> players;
        private final boolean runThread;

        public PlayerUpdaterTask() {
            this.players = new ArrayBlockingQueue<Player>(1000);
            this.runThread = true;
        }

        public boolean isFull() {
            return this.players.remainingCapacity() != 0;
        }

        public void addConnection(NetworkConnection c) {
            final Player player = ServerNetworkHandler.this.database.getPlayer(c.getRemoteConnectionName());
            player.setNetworkConnection(c);
            player.setMessageListener(MessageType.CHAT, (p, m) -> LobbyManager.onChat(p, m));
            player.setMessageListener(MessageType.LIST_CHAT_ROOMS, (p, m) -> LobbyManager.onListChatRooms(p, m));
            player.setMessageListener(MessageType.CHAT_ROOM_CREATE, (p, m) -> LobbyManager.onChatRoomCreate(p, m));
            player.setMessageListener(MessageType.CHAT_ROOM_JOIN, (p, m) -> LobbyManager.onChatRoomJoin(p, m));
            player.setMessageListener(MessageType.CHAT_ROOM_LEAVE, (p, m) -> LobbyManager.onChatRoomLeave(p, m));
            player.setMessageListener(MessageType.FIND_GAME, (p, m) -> Server.addToGameQueue(p));
            player.setMessageListener(MessageType.INVITE_PLAYERS, (p, m) -> {
                new LudoInviter(p, m.getArray(EntryType.USER_NAME));
            });

            player.setMessageListener(MessageType.LIST_FIRENDS, (p, m) -> LobbyManager.onFriendList(p, m));
            player.setMessageListener(MessageType.ADD_FRIEND, (p, m) -> LobbyManager.onAddFriend(p, m));
            player.setMessageListener(MessageType.REMOVE_FRIEND, (p, m) -> LobbyManager.onRemoveFriend(p, m));

            player.setMessageListener(MessageType.LIST_SCOREBOARD, (p, m) -> LobbyManager.onListScoreboard(p, m));

            try {
                this.players.put(player);
            } catch (final InterruptedException e) {
                ServerNetworkHandler.LOGGER.log(Level.SEVERE, "List full", e);
            }
        }

        @Override
        public void run() {
            long msStart = 0;

            while (this.runThread) {
                msStart = System.currentTimeMillis();

                for (final Player p : this.players) {
                    if (!p.isConnectionGood()) {
                        p.onDisconnect();
                        this.players.remove(p);
                    } else {
                        p.update();
                    }
                }

                // If we can update all the connections in less than 50ms, we're
                // doing great. Let the CPU rest a bit.
                if (System.currentTimeMillis() - msStart < 50) {
                    try {
                        Thread.sleep(100);
                    } catch (final InterruptedException e) {
                        ServerNetworkHandler.LOGGER.log(Level.INFO, "CUT sleep interrupted", e);
                    }
                }
            }
        }
    }

    /**
     * ConnectionLoginTask is meant to be run as a Daemon and handle the server
     * side login protocol.
     */
    private class ConnectionLoginTask implements Runnable {
        @Override
        public void run() {
            while (true) {
                for (final ServerNetworkConnection nwc : ServerNetworkHandler.this.waitingConnections) {
                    switch (nwc.updateLogin()) {

                        case AUTHENTICATION_FAILED:
                        case CONNECTION_CLOSED:
                            ServerNetworkHandler.this.waitingConnections.remove(nwc);
                            break;

                        case AUTHENTICATION_PENDING:
                            break;

                        case CONNECTION_ESTABLISHED:
                            ServerNetworkHandler.this.addConnection(nwc);
                            ServerNetworkHandler.this.waitingConnections.remove(nwc);
                            ServerNetworkHandler.LOGGER.log(Level.INFO, "ServerNetworkHandler: connection established");
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }

    /**
     * ConnectionListenerTask is meant to be run as a Daemon and send all
     * incoming connections to waitingConnections
     */
    private class ConnectionListenerTask implements Runnable {
        @Override
        public void run() {
            try {
                final ServerSocket serverSocket = ServerSocketFactory.getDefault().createServerSocket(23456);
                Socket socket;

                while ((socket = serverSocket.accept()) != null) {
                    final ServerNetworkConnection connection = new ServerNetworkConnection(socket);
                    try {
                        ServerNetworkHandler.this.waitingConnections.put(connection);
                    } catch (final InterruptedException e) {
                        ServerNetworkHandler.LOGGER.log(Level.INFO, "wait interrupted.", e);
                    }
                }
                serverSocket.close();
            } catch (final IOException e) {
                ServerNetworkHandler.LOGGER.log(Level.INFO, "Connection closed", e);
            }
        }
    }

}
