package com.enderwolf.ludonet.server.game;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.server.Database;
import com.enderwolf.ludonet.server.LobbyManager;
import com.enderwolf.ludonet.shared.ConnectionLostListener;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.PlayerColor;

/**
 * LudoController is responsible for receiving player actions, respond to them,
 * and notify the other players of said action. The actual game state is managed
 * by the class GameState.
 *
 * There are two interfaces that controls the game flow: 1.
 * LudoController#makeMove(Player,int,int) 2. LudoController#finishMove(Player)
 * 3. LudoController#removePlayer(Player)
 *
 * All of these methods should be called as a response to the currently active
 * player doing sending game messages. The LudoController responds to these
 * events by (a) notifying all clients about the changes to the game state, (b)
 * moving the turn around the "table" and (c) notifying players of dropped
 * client.
 */
public class LudoController implements ConnectionLostListener {
    public enum LobbyState {
        WAITING_FOR_PLAYERS,
        PLAYING,
        GAME_OVER,
    }

    private static final Logger LOGGER = Logger.getGlobal();
    private final int maxGameSize;

    private LobbyState lobbyState;
    private GameState gameState;
    private final Player[] players;
    private final int gameChatRoomId;

    /**
     * Sole constructor. Create a new LudoController. The default lobbyState is
     * WAITING_FOR_PLAYERS, as at the point of creation, no players are related
     * to the game.
     */
    public LudoController(int maxGameSize) {
        this.maxGameSize = maxGameSize;
        if (maxGameSize > 4) {
            LudoController.LOGGER.log(Level.SEVERE, "More than 4 maxPlayers");
        }

        this.lobbyState = LobbyState.WAITING_FOR_PLAYERS;
        this.players = new Player[4];

        this.gameChatRoomId = (new Database().createChatRoom("Game Chat", true));
        if (this.gameChatRoomId == 0) {
            LudoController.LOGGER.warning("Unable to create game-chat room");
        }
    }

    /**
     * Add a player to this game session. A player can only be added if the game
     * is in the lobbyState 'WAITING_FOR_PLAYERS'.
     *
     * @param player
     *            The player to be added to this game.
     * @return True if the game is in lobbyState WAITING_FOR_PLAYERS and is not
     *         full. False is returned if the lobbyState is not
     *         WAITING_FOR_PLAYERS, the lobby is already full, or the player
     *         already is in the game.
     */
    public boolean addPlayer(Player player, boolean checkIfIngame) {
        if (this.lobbyState != LobbyState.WAITING_FOR_PLAYERS) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to add player in state " + this.lobbyState.toString());
            return false;
        }

        final boolean wasInGame = player.testAndSetInGame(true);
        if (checkIfIngame && wasInGame) {
            return false;
        }

        for (int i = 0; i < 4; i++) {
            if (player == this.players[i]) {
                return false;
            }
        }

        boolean added = false;
        for (int i = 0; i < 4; i++) {
            if (this.players[i] == null) {
                this.players[i] = player;
                player.setPlayerColor(PlayerColor.values()[i]);
                added = true;
                break;
            }
        }

        if (!added) {
            return false;
        }

        // Map callbacks in the Player
        player.setMessageListener(MessageType.GAME_TURN_OVER, (p, m) -> this.onMessageFinishTurn(p));
        player.setMessageListener(MessageType.GAME_MOVE, (p, m) -> this.onMessageMove(p, m));
        player.setConnectionLostListener(this);

        this.joinGameChatRoom(player);

        // Notify all existing players of the new Player.
        for (int i = 0; i < 4; i++) {
            final Player p = this.players[i];
            if (p != null) {
                /* Notify everyone of the addition of the new player */
                final Message msg = new Message(MessageType.GAME_PLAYER_JOIN);
                msg.addEntry(EntryType.PLAYER_ID, String.valueOf(player.getPlayerId()), false);
                msg.addEntry(EntryType.PLAYER_COLOR, player.getPlayerColor().toString(), false);
                msg.addEntry(EntryType.USER_NAME, player.getUserName(), true);
                p.sendMessage(msg);

                /* Notify the new player of everyone else as well */
                if (p != player) {
                    final Message message = new Message(MessageType.GAME_PLAYER_JOIN);
                    message.addEntry(EntryType.PLAYER_ID, String.valueOf(p.getPlayerId()), false);
                    message.addEntry(EntryType.PLAYER_COLOR, p.getPlayerColor().toString(), false);
                    message.addEntry(EntryType.USER_NAME, p.getUserName(), true);
                    player.sendMessage(message);
                }
            }
        }
        LudoController.LOGGER.log(Level.WARNING, "pc: " + this.getPlayerCount() + " max: " + this.maxGameSize);
        if (this.getPlayerCount() == this.maxGameSize) {
            this.startGame();
        }

        return true;
    }

    /**
     * Called when one of the connected players disconnect.
     *
     * @param player
     *            The player that disconnected.
     */
    @Override
    public void onPlayerDisconnected(Player player) {
        player.testAndSetInGame(false);
        if (this.gameState != null && player == this.gameState.getActivePlayer()) {
            this.onMessageFinishTurn(player);
        } else if (this.lobbyState == LobbyState.WAITING_FOR_PLAYERS) {
            boolean removed = false;
            for (int i = 0; i < 4; i++) {
                if (this.players[i] == player) {
                    this.leaveGameChatRoom(player);
                    this.players[i] = null;
                    removed = true;
                }
            }

            if (removed) {
                for (int i = 0; i < 4; i++) {
                    if (this.players[i] != null) {
                        final Message message = new Message(MessageType.GAME_PLAYER_LEFT);
                        message.addEntry(EntryType.PLAYER_ID, String.valueOf(player.getPlayerId()), false);
                        this.players[i].sendMessage(message);
                    }
                }
            } else {
                LudoController.LOGGER.log(Level.WARNING,
                        "LudoController#removePlayer(): Given player is not in the game.");
            }
        }
    }

    /**
     * Get the number of connected players.
     *
     * @return The number of connected players.
     */
    public int getPlayerCount() {
        int count = 0;
        for (final Player player : this.players) {
            if (player != null) {
                count++;
            }
        }

        return count;
    }

    /**
     * Called when a Player sends a GAME_MOVE message.
     *
     * @param player
     *            The player who sendt the message.
     * @param message
     *            The message containing the move-data.
     */
    public void onMessageMove(Player player, Message message) {
        if (this.lobbyState != LobbyState.PLAYING) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to make move: Is in state " + this.lobbyState.toString());
            return;
        }

        if (this.gameState.getActivePlayer() != player) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to make move: not this player's turn");
            return;
        }

        final int gamePiece = Integer.valueOf(message.getEntry(EntryType.GAME_PIECE));
        final int moves = Integer.valueOf(message.getEntry(EntryType.NUM_MOVES));

        if (!this.gameState.isMoveLegal(player, gamePiece, moves)) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to make move: move is illegal");
            return;
        }

        if (!this.gameState.performMove(player, gamePiece, moves)) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to make move: GameState is inconsistent");
            return;
        }

        for (int i = 0; i < 4; i++) {
            if (this.players[i] != null) {
                final Message msg = new Message(MessageType.GAME_MOVE);
                msg.addEntry(EntryType.PLAYER_ID, String.valueOf(player.getPlayerId()), false);
                msg.addEntry(EntryType.NUM_MOVES, String.valueOf(moves), false);
                msg.addEntry(EntryType.GAME_PIECE, String.valueOf(gamePiece), false);
                this.players[i].sendMessage(message);
            }
        }
    }

    /**
     * Called when a player sends a GAME_TURN_OVER.
     *
     * @param player
     *            The player who send the message.
     */
    public void onMessageFinishTurn(Player player) {
        if (this.lobbyState != LobbyState.PLAYING) {
            LudoController.LOGGER
                    .log(Level.WARNING, "Unable to finish move: Is in state " + this.lobbyState.toString());
            return;
        }

        if (this.gameState.getActivePlayer() != player) {
            LudoController.LOGGER.log(Level.WARNING, "Unable to finish move: not this player's turn");
            return;
        }

        // Handle disconnections and notify the lobby of eventual DCs
        this.checkConnections();

        this.gameState.advanceTurn();

        final Player winner = this.gameState.getWinner();
        if (winner != null) {
            this.endGame();

            for (int i = 0; i < 4; i++) {
                if (this.players[i] != null) {
                    final Message message = new Message(MessageType.GAME_OVER);
                    message.addEntry(EntryType.PLAYER_ID, String.valueOf(winner.getPlayerId()), false);
                    this.players[i].sendMessage(message);
                }
            }
        } else {
            this.notifyActivePlayer();
        }
    }

    /**
     * Notify whoever's flagged as active by the GameState that it is his turn.
     * Also notify everyone else about the activeness of said player.
     */
    private void notifyActivePlayer() {
        final Player activePlayer = this.gameState.getActivePlayer();

        final Integer[] diceThrows = new Integer[5];
        final Random randomGenerator = new Random();
        for (int i = 0; i < 5; i++) {
            diceThrows[i] = randomGenerator.nextInt(6) + 1;
        }

        for (int i = 0; i < 4; i++) {
            if (this.players[i] != null) {
                final Message message = new Message(MessageType.GAME_ACTIVE_PLAYER);
                message.addEntry(EntryType.PLAYER_ID, String.valueOf(activePlayer.getPlayerId()), false);
                message.addArray(EntryType.NUM_MOVES, diceThrows, false);
                this.players[i].sendMessage(message);
            }
        }
    }

    /**
     * Perform sanity checks on the connected Players. This method should be
     * frequently called (i.e., frequently enough to properly handle dropped
     * connections).
     */
    private void checkConnections() {
        for (int i = 0; i < 4; i++) {
            if (this.players[i] != null && !this.players[i].isConnectionGood()) {
                for (int j = 0; j < 4; j++) {
                    if (this.players[j] != null) {
                        this.leaveGameChatRoom(this.players[i]);

                        final Message message = new Message(MessageType.GAME_PLAYER_LEFT);
                        message.addEntry(EntryType.PLAYER_ID, this.players[i].getPlayerId(), false);
                        this.players[j].sendMessage(message);
                    }
                }

                this.players[i] = null;
            }
        }
    }

    private void joinGameChatRoom(Player player) {
        final Message fakeMsg = new Message(MessageType.CHAT_ROOM_JOIN);
        fakeMsg.addEntry(EntryType.CHAT_ROOM_ID, this.gameChatRoomId);

        LobbyManager.onChatRoomJoin(player, fakeMsg);
    }

    /**
     * This method notifies all the players that the passed player has left the
     * chat room. The leaving player will also be forced out of the room.
     *
     * @param player
     *            The player that will leave the chat room.
     */
    private void leaveGameChatRoom(Player player) {
        // Force the player out of the room
        final Message msg = new Message(MessageType.CHAT_ROOM_LEAVE);
        msg.addEntry(EntryType.CHAT_ROOM_ID, this.gameChatRoomId);
        player.sendMessage(msg);

        // Fake a message from the leaving player
        LobbyManager.onChatRoomLeave(player, msg);
    }

    /**
     * Start the game by changing the gamestate to PLAYING and notifying the
     * players that we are starting. Method does nothing unless the current
     * lobbyState is WAITING_FOR_PLAYERS.
     */
    private void startGame() {
        if (this.lobbyState != LobbyState.WAITING_FOR_PLAYERS) {
            return;
        }

        this.lobbyState = LobbyState.PLAYING;
        this.gameState = new GameState(this.players);

        for (int i = 0; i < 4; i++) {
            if (this.players[i] != null) {
                final Message message = new Message(MessageType.GAME_STARTING);
                this.players[i].sendMessage(message);
            }
        }

        this.notifyActivePlayer();
    }

    /**
     * Set the LobbyState to GAME_OVER. For all the remaining players in the
     * game, award one "game-count" point. Award one "victory-point" to the
     * winner. The game-session must be in state LobbyState.PLAYING, and
     * GameState must have declared a winner.
     */
    private void endGame() {
        if (this.lobbyState != LobbyState.PLAYING) {
            Logger.getGlobal().log(Level.WARNING,
                    "LudoController#endGame: Invalid lobby state: " + this.lobbyState.toString());
            return;
        }

        final Player winner = this.gameState.getWinner();
        if (winner == null) {
            Logger.getGlobal().log(Level.WARNING, "LudoController#endGame: There is no winner.");
            return;
        }

        this.lobbyState = LobbyState.GAME_OVER;
        final Database db = new Database();

        for (final Player p : this.players) {
            if (p != null) {
                p.setConnectionLostListener(null);
                p.testAndSetInGame(false);
                final boolean didWin = p == winner;
                db.awardPoints(p, didWin);

                this.leaveGameChatRoom(p);
            }
        }
    }
}
