/**
 * This package contains all classes shared between the server and clients codebase.
 *
 * @author !Tulingen
 */
package com.enderwolf.ludonet.server;