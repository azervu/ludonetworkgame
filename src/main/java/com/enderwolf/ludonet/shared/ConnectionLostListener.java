package com.enderwolf.ludonet.shared;

/**
 * ConnectionLostListener is an interface to receive notifications for when a
 * Player loses the connection to the (previously) connected peer.
 */
public interface ConnectionLostListener {
    /**
     * Called as soon as possible after the disconnection occurred.
     */
    void onPlayerDisconnected(Player player);
}
