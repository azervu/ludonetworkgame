package com.enderwolf.ludonet.shared;

public class GamePiece {
    private final PlayerColor color;

    // The tile position of this GamePiece. See the wiki for a detailed visual
    // description of how the indices of
    // the tiles on the game board are laid out.
    private int position;

    /**
     * Create a new GamePiece.
     *
     * @param color
     *            The color of the player this piece belongs to.
     */
    public GamePiece(PlayerColor color) {
        this.color = color;
    }

    /**
     * Set the tile-index position of this game piece.
     *
     * @param position
     *            The new position of this game piece.
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Retrieve the tile-index position of this GamePiece.
     *
     * @return The index of the tile this game piece is standing on.
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * Retrieve the PlayerColor of this GamePiece. The color matches the
     * parent's.
     *
     * @return The color of this game piece.
     */
    public PlayerColor getPlayerColor() {
        return this.color;
    }

    /**
     * Check if the game piece has reached the goal tile.
     *
     * @return True if the piece is standing in the goal tile, false otherwise.
     */
    public boolean isFinished() {
        final int finishTile = GameTrail.getPlayerTrail(this.color).getFinishIndex();
        return this.position == finishTile;
    }
}
