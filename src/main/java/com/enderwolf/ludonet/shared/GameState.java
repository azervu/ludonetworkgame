package com.enderwolf.ludonet.shared;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * GameState is responsible for storing the game-state, validating moves and
 * making sure that all actions taken by the players are coherent with the rules
 * of Ludo. GameState is used as the "law" of what the game looks like on the
 * server side, and as a local validator of possible moves on the client side.
 */
public class GameState {
    // The indices of the array map directly to PlayerColor#getValue().
    protected Player[] players;

    // Relation-mapping between tiles and the pieces occupying them. At most, 16
    // of the entries in this
    // array will be non-null as a value of null indicates that the tile is
    // currently unoccupied.
    protected GamePiece[] tileMap;

    // The index of the currently active player.
    int activeIndex;

    /**
     * Create a GameState with a set of Player objects.
     *
     * @param players
     *            The Players that will participate in this game. The Player
     *            objects must have had their colors assigned to them already.
     */
    public GameState(Player[] players) {
        this.tileMap = new GamePiece[GameTrail.getNumberOfTiles()];
        this.players = players;

        // The players must have their colors assigned and their game pieces
        // instantiated.
        for (int i = 0; i < 4; i++) {
            if (players[i] != null) {
                players[i].instantiateGamePieces();
            }
        }

        this.updateTileMap();
    }

    /**
     * A player has departed from the game.
     *
     * @param color
     *            The color of the player that left.
     */
    public void playerLeft(PlayerColor color) {
        this.players[color.getValue()] = null;
    }

    /**
     * Check if a move is legal, regardless of whether or not it's "color"'s
     * turn
     *
     * @param color
     *            The color of the player to check.
     * @param pieceIndex
     *            The index of the piece to check (must be in range [0..3]).
     * @param moves
     *            The number of moves to take with the game piece.
     * @return True if the move is valid, false otherwise.
     */
    public boolean isMoveLegal(PlayerColor color, int pieceIndex, int moves) {
        final Player player = this.players[color.getValue()];
        return this.isMoveLegal(player, pieceIndex, moves);
    }

    /**
     * Check if a move is legal, regardless of whether or not it's "player"'s
     * turn. Do note that if the selected game piece is in the starting area,
     * "moves" must be 6 for the move to be valid.
     *
     * @param player
     *            The Player who may or may not move.
     * @param pieceIndex
     *            The index of the piece to check (must be in range [0..3]).
     * @param moves
     *            The number of moves to take with the game piece.
     * @return True if the move is valid, false otherwise.
     */
    public boolean isMoveLegal(Player player, int pieceIndex, int moves) {
        // If a winner has been declared, nothing is legal. The game is over.
        // Stop begin bad and try again.
        if (this.getWinner() != null) {
            return false;
        }

        if (moves < 1 || moves > 6) {
            return false;
        }

        final GamePiece piece = player.getGamePiece(pieceIndex);

        // Game pieces at the spawn location can not move unless "moves" is six.
        if (moves != 6 && piece.getPosition() < GameTrail.FIRST_SHARED_TILE) {
            return false;
        }

        final int curPos = piece.getPosition();
        final int newPos = GameTrail.getPlayerTrail(player.getPlayerColor()).getNextPosition(curPos, moves);

        final GamePiece occupying = this.tileMap[newPos];
        if (occupying == null) {
            // Noone is blocking us, jeesus sweetus!
            return true;
        }

        // We are allowed to land on top of enemy pieces - they get sent back
        // home.
        return occupying.getPlayerColor() != player.getPlayerColor();
    }

    /**
     * Check if the specified player has any available moves to him, given a die
     * roll of "moves".
     *
     * @param player
     *            The player to check if can move.
     * @param moves
     *            The number of eyes the die showed.
     * @return True if at least one move is available, false otherwise.
     */
    public boolean anyMoveLegal(Player player, int moves) {
        for (int i = 0; i < 4; i++) {
            final GamePiece gp = player.getGamePiece(i);

            if (gp.isFinished()) {
                continue;
            }

            if (this.isMoveLegal(player, i, moves)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Perform a move. It must be "color"'s turn.
     *
     * @param color
     *            The color of the player that is moving.
     * @param pieceIndex
     *            The index of the piece to move (must be in range [0..3]).
     * @param moves
     *            The number of moves to take.
     * @return False if the move is illegal, true on success.
     */
    public boolean performMove(PlayerColor color, int pieceIndex, int moves) {
        final Player player = this.players[color.getValue()];
        return this.performMove(player, pieceIndex, moves);
    }

    /**
     * Perform a move. It must be "player"'s turn.
     *
     * @param player
     *            The player that is moving.
     * @param pieceIndex
     *            The index of the piece to move (must be in range [0..3]).
     * @param moves
     *            The number of moves to take.
     * @return False if the move is illegal, true on success.
     */
    public boolean performMove(Player player, int pieceIndex, int moves) {
        if (!this.isMoveLegal(player, pieceIndex, moves)) {
            return false;
        }

        if (player.getPlayerColor().getValue() != this.activeIndex) {
            return false;
        }

        final GamePiece piece = player.getGamePiece(pieceIndex);
        final int curPos = piece.getPosition();
        final int newPos = GameTrail.getPlayerTrail(player.getPlayerColor()).getNextPosition(curPos, moves);
        piece.setPosition(newPos);

        this.tileMap[curPos] = null;

        // If there are any pieces blocking the new position, send them back
        // home. We have already established
        // that IF any pieces are blocking, they are hostile. This check is done
        // in isMoveLegal().
        final GamePiece blocker = this.tileMap[newPos];
        if (blocker != null) {
            // Put the piece at the first free spawn position belonging to this
            // player
            final int firstSpawn = GameTrail.getPlayerTrail(blocker.getPlayerColor()).getFirstSpawnIndex();
            boolean sentHome = false;

            for (int i = 0; i < 4; i++) {
                if (this.tileMap[firstSpawn + i] == null) {
                    this.tileMap[newPos] = null;
                    this.tileMap[firstSpawn + i] = blocker;
                    blocker.setPosition(firstSpawn + i);
                    sentHome = true;
                    break;
                }
            }

            if (!sentHome) {
                Logger.getGlobal().warning("GameState#performMove: Unable to send blocking game piece home");
            }
        }

        // Don't keep track of where finished game pieces are. This works around
        // the issue where friendly pieces
        // can't stand on top of each other.
        if (!piece.isFinished()) {
            this.tileMap[newPos] = piece;
        }

        return true;
    }

    /**
     * Retrieve the player whose turn it currently is.
     *
     * @return The player whose turn it currently is.
     */
    public Player getActivePlayer() {
        return this.players[this.activeIndex];
    }

    /**
     * the authority-GameState. All GameState-instances on the clients are
     * considered reactive, and instances on the server side authoritative.
     *
     * @param color
     *            The color of the player to be marked as active. If this
     *            instance is not in the game, no changes are done (i.e., if the
     *            player of this color has left the session).
     */
    public void setActivePlayer(PlayerColor color) {
        final int idx = color.getValue();

        if (this.players[idx] != null) {
            this.activeIndex = idx;
        } else {
            Logger.getGlobal().warning("GameState#setActivePlayer: Player of color is null: " + color.toString());
        }
    }

    /**
     * Move the turn to the next existing player.
     */
    public void advanceTurn() {
        int incs = 0;

        do {
            this.activeIndex = (++this.activeIndex) % 4;
            incs++;
        } while (this.players[this.activeIndex] == null && incs < 4);
    }

    /**
     * Get the winner of the game.
     *
     * @return The winner of the game if the game is finished, null if the game
     *         is still in progress.
     */
    public Player getWinner() {
        Player lastPlayer = null;
        int remainingPlayers = 0;

        for (final Player player : this.players) {
            // If a player has finished the game, he is the winner.
            if (player != null) {
                // "lastPlayer" will hold the last remaining player if
                // "remainingPlayers" is 1. If remainingPlayers is
                // greater than 1, the value of "lastPlayer" should be ignored -
                // there is no winner yet.
                remainingPlayers++;
                lastPlayer = player;

                if (player.hasFinishedGame()) {
                    return player;
                }

            }
        }

        if (remainingPlayers == 1) {
            return lastPlayer;
        }

        return null;
    }

    /**
     * Retrieve the players.
     *
     * @return An array of size 4 containing the players. There is no guarantee
     *         that the array will contain non-null values.
     */
    public Player[] getPlayers() {
        return this.players;
    }

    /**
     * Update the indices of 'tileMap'.
     *
     * @return False on internal inconsistency, if two game pieces share a tile.
     */
    protected boolean updateTileMap() {
        boolean valid = true;

        for (int i = 0; i < this.tileMap.length; i++) {
            this.tileMap[i] = null;
        }

        for (final Player player : this.players) {
            if (player == null) {
                continue;
            }

            for (int j = 0; j < 4; j++) {
                final GamePiece piece = player.getGamePiece(j);
                final int pos = piece.getPosition();

                if (this.tileMap[pos] != null) {
                    Logger.getGlobal().log(Level.SEVERE, "Inconsistency in GameState: two tiles share positions");
                    valid = false;
                }

                // Don't keep track of finished game pieces
                if (!piece.isFinished()) {
                    this.tileMap[pos] = piece;
                }
            }
        }

        return valid;
    }
}
