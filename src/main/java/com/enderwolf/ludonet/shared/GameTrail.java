package com.enderwolf.ludonet.shared;

public class GameTrail {
    public static class Point {
        /**
         * @return the x
         */
        public double getX() {
            return this.x;
        }

        /**
         * @return the y
         */
        public double getY() {
            return this.y;
        }

        private final double x;
        private final double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

    }

    /**
     * Trail defines the path a player of a specific color must take. Trail does
     * NOT track the game state, and is used as a "static" lookup when moving a
     * game piece.
     */
    public static class Trail {
        private final PlayerColor color;
        private final int firstSpawn;
        private final int firstTile;
        private final int lastTile;
        private final int firstFinal;

        /**
         * Define a player-trail. The trail is only valid for a specified color.
         *
         * @param color
         *            The player color this Trail is valid for.
         * @param firstSpawn
         *            The index of the first spawn tile. The first four indices
         *            (starting at this index) define the four spawning
         *            positions of this player.
         * @param firstTile
         *            The first tile the player moves to after spawning.
         * @param lastTile
         *            The last "common" tile this player will occupy before
         *            moving onto the final trail. The indices "wrap" around.
         * @param firstFinal
         *            The first index of the final trail for this player. The
         *            first six indices (starting at this index) define the
         *            final trail. The fifth index after this index define the
         *            finishing tile.
         */
        public Trail(PlayerColor color, int firstSpawn, int firstTile, int lastTile, int firstFinal) {
            this.color = color;
            this.firstSpawn = firstSpawn;
            this.firstTile = firstTile;
            this.lastTile = lastTile;
            this.firstFinal = firstFinal;
        }

        /**
         * Get the index of the next tile following a die roll.
         *
         * @param current
         *            The currently occupied tile.
         * @param toMove
         *            The number of squares to move.
         * @return The final position of this move.
         */
        public int getNextPosition(int current, int toMove) {
            int tile = current;

            for (int i = 0; i < toMove; i++) {
                if (tile == this.lastTile) {
                    /* Move to the final path to glory */
                    tile = this.firstFinal;
                } else if (tile == GameTrail.LAST_SHARED_TILE) {
                    /* Wrap around */
                    tile = GameTrail.FIRST_SHARED_TILE;
                } else if (tile == (this.firstFinal + 5)) {
                    /* Goal has been reached */
                } else if (tile < GameTrail.FIRST_SHARED_TILE) {
                    /*
                     * Initial movement: move straight to the first tile every
                     * time
                     */
                    return this.firstTile;
                } else {
                    /* Move to the next tile */
                    tile++;
                }
            }

            return tile;
        }

        /**
         * Retrieve the first spawn index. The range [retval .. retval+3]
         * (inclusive) contains the four defined spawning positions of the
         * player.
         *
         * @return The first spawn index of the player.
         */
        public int getFirstSpawnIndex() {
            return this.firstSpawn;
        }

        /**
         * Retrieve the goal tile for this PlayerColor.
         *
         * @return The final destination for game pieces of this color.
         */
        public int getFinishIndex() {
            return this.firstFinal + 5;
        }

        /**
         * Retrieve the PlayerColor.
         *
         * @return The PlayerColor
         */
        public PlayerColor getColor() {
            return this.color;
        }
    }

    public static final int FIRST_SPAWN_TILE = 0;
    public static final int LAST_SPAWN_TILE = 15;
    public static final int FIRST_SHARED_TILE = 16;
    public static final int LAST_SHARED_TILE = 67;

    /**
     * tileCoords defines the positions of the tiles in tile-space-coordinates.
     * See the wiki for a visual representation of the indices and their
     * positions.
     */
    private static final Point[] tileCoords = {
        /* Green starting positions */
        new Point(2.5, 1.5),
        new Point(3.5, 2.5),
        new Point(2.5, 3.5),
        new Point(1.5, 2.5),

        /* Red starting positions */
        new Point(11.5, 1.5),
        new Point(12.5, 2.5),
        new Point(11.5, 3.5),
        new Point(10.5, 2.5),

        /* Blue starting positions */
        new Point(11.5, 10.5),
        new Point(12.5, 11.5),
        new Point(11.5, 12.5),
        new Point(10.5, 11.5),

        /* Yellow starting positions */
        new Point(2.5, 10.5),
        new Point(3.5, 11.5),
        new Point(2.5, 12.5),
        new Point(1.5, 11.5),

        /* Main Trail, includes all shared tiles */
        new Point(1, 6),
        new Point(2, 6),
        new Point(3, 6),
        new Point(4, 6),
        new Point(5, 6),
        new Point(6, 5),
        new Point(6, 4),
        new Point(6, 3),
        new Point(6, 2),
        new Point(6, 1),
        new Point(6, 0),
        new Point(7, 0),
        new Point(8, 0),
        new Point(8, 1),
        new Point(8, 2),
        new Point(8, 3),
        new Point(8, 4),
        new Point(8, 5),
        new Point(9, 6),
        new Point(10, 6),
        new Point(11, 6),
        new Point(12, 6),
        new Point(13, 6),
        new Point(14, 6),
        new Point(14, 7),
        new Point(14, 8),
        new Point(13, 8),
        new Point(12, 8),
        new Point(11, 8),
        new Point(10, 8),
        new Point(9, 8),
        new Point(8, 9),
        new Point(8, 10),
        new Point(8, 11),
        new Point(8, 12),
        new Point(8, 13),
        new Point(8, 14),
        new Point(7, 14),
        new Point(6, 14),
        new Point(6, 13),
        new Point(6, 12),
        new Point(6, 11),
        new Point(6, 10),
        new Point(6, 9),
        new Point(5, 8),
        new Point(4, 8),
        new Point(3, 8),
        new Point(2, 8),
        new Point(1, 8),
        new Point(0, 8),
        new Point(0, 7),
        new Point(0, 6),

        /* Green final trail */
        new Point(1, 7),
        new Point(2, 7),
        new Point(3, 7),
        new Point(4, 7),
        new Point(5, 7),
        new Point(6, 7),

        /* Red final trail */
        new Point(7, 1),
        new Point(7, 2),
        new Point(7, 3),
        new Point(7, 4),
        new Point(7, 5),
        new Point(7, 6),

        /* Blue final trail */
        new Point(13, 7),
        new Point(12, 7),
        new Point(11, 7),
        new Point(10, 7),
        new Point(9, 7),
        new Point(8, 7),

        /* Yellow final trail */
        new Point(7, 13),
        new Point(7, 12),
        new Point(7, 11),
        new Point(7, 10),
        new Point(7, 9),
        new Point(7, 8),
    };

    private static final Trail[] PlAYER_TRAILS = {
        new Trail(PlayerColor.GREEN, 0, 16, 66, 68),
        new Trail(PlayerColor.RED, 4, 29, 27, 74),
        new Trail(PlayerColor.BLUE, 8, 42, 40, 80),
        new Trail(PlayerColor.YELLOW, 12, 55, 53, 86)
    };

    /**
     * Retrieve the total number of tiles on the game board.
     *
     * @return The number of tiles on the game board
     */
    public static int getNumberOfTiles() {
        return GameTrail.tileCoords.length;
    }

    /**
     * Retrieve the button coordinate of a specified tile. The returned point is
     * in "tile-space coordinates", meaning that the value must be multiplied
     * with the tile-size to retrieve the pixel position of the tile.
     *
     * @param index
     *            The index of the tile to retrieve. Must be in the range
     *            [0..91].
     * @return A point in tile-space coordinates {x=[0..14], y=[0..14]} or null
     *         if the index is out of range.
     */
    public static Point getTileCoord(int index) {
        if (index >= 0 && index < GameTrail.tileCoords.length) {
            return GameTrail.tileCoords[index];
        }

        return null;
    }

    /**
     * Retrieve the Trail for a player of a given color.
     *
     * @param color
     *            The color of the player whose Trail will be retrieved.
     * @return The Trail of the specified player, null on invalid PlayerColor.
     */
    public static Trail getPlayerTrail(PlayerColor color) {
        final int index = color.getValue();

        if (index >= 0 && index < GameTrail.PlAYER_TRAILS.length) {
            return GameTrail.PlAYER_TRAILS[index];
        }

        return null;
    }

    private GameTrail() {
    }
}
