package com.enderwolf.ludonet.shared;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This singleton provides global access to resources such as strings.
 */
public class I18N {
    private static final String DEFAULT_FILE = "com.enderwolf.ludonet.shared.i18n.messages";
    private static ResourceBundle RESOURCE_BUNDLE;

    /**
     * Private constructor defeats instantiation
     */
    private I18N() {
    }

    /**
     * Provides a string in the relevant language
     *
     * @param the
     *            key to the string loaded from i18n
     * @return the string loaded from i18n
     */
    public static String getString(String key) {
        try {
            return I18N.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            Logger.getGlobal().log(Level.WARNING, "String " + key + " has no translation for current lang", e);
            return '!' + key + '!';
        }
    }

    static {
        final Locale currentLocale = Locale.getDefault();

        try {
            I18N.RESOURCE_BUNDLE = ResourceBundle.getBundle(I18N.DEFAULT_FILE, currentLocale);
        } catch (final java.util.MissingResourceException e) {
            Logger.getGlobal().log(Level.INFO, "Using default lang", e);
            I18N.RESOURCE_BUNDLE = ResourceBundle.getBundle(I18N.DEFAULT_FILE);
        }
    }
}