package com.enderwolf.ludonet.shared;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

public class MD5 {
    /**
     * Hash a string to an MD5-string.
     *
     * @param input
     *            The string to be hashed.
     * @return The MD5 of the input on success, null on error.
     */
    public static String hash(String input) {
        String hash = null;

        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] digest = md.digest(input.getBytes());

            final HexBinaryAdapter hba = new HexBinaryAdapter();
            hash = hba.marshal(digest);
        } catch (final NoSuchAlgorithmException nsa) {
            Logger.getGlobal().log(Level.SEVERE, "Unable to find the MD5 implementation", nsa);
        }

        return hash;
    }

    /**
     * Hash a string with an additional salt. The method will return hash(input
     * + salt).
     *
     * @param input
     *            The string to be hashed.
     * @param salt
     *            The hash-salt. ¡Muy caliente!
     * @return The MD5 of the input on success, null on error.
     */
    public static String hash(String input, String salt) {
        return MD5.hash(input + salt);
    }

    private MD5() {
    }
}
