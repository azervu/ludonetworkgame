package com.enderwolf.ludonet.shared;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;

/**
 * This class encapsulates the basic receiving and sending functions needed by
 * ClientNetworkConnection and ServerNetworkConnection. It is abstract because
 * as it does not have the functionality to establish socket connections it is
 * not useful by itself.
 */
public abstract class NetworkConnection {

    /**
     * This enum gives the state a connection is in.
     */
    public static enum ConnectionStatus {
        IDENTIFICATION_PENDING,
        AUTHENTICATION_PENDING,
        AUTHENTICATION_FAILED,
        CONNECTION_ESTABLISHED,
        CONNECTION_CLOSED;
    }

    private static final Logger LOGGER = Logger.getGlobal();

    protected String remoteConnectionName;
    // TODO still need some proper value
    protected String localConnectionName = "";
    protected ConnectionStatus connectionStatus;
    protected Socket socket = null;
    protected SSLSocket loginSocket = null;
    protected BufferedReader reader = null;
    protected BufferedWriter writer = null;
    private final boolean trustedConnection;

    protected NetworkConnection(boolean trustedConnection) {
        this.trustedConnection = trustedConnection;
    }

    /**
     * Closes the connection, there is no way to restart it other than creating
     * a new one.
     */
    public void close() {
        this.connectionStatus = ConnectionStatus.CONNECTION_CLOSED;
        NetworkConnection.LOGGER.log(Level.INFO, "connection closed");
        try {
            if (this.reader != null) {
                this.reader.close();
            }
            if (this.writer != null) {
                this.writer.close();
            }
            if (this.socket != null) {
                this.socket.close();
            }
            if (this.loginSocket != null) {
                this.loginSocket.close();
            }
        } catch (final IOException e) {
            NetworkConnection.LOGGER.log(Level.WARNING, "Problem on closing connection.", e);
        }
    }

    /**
     * Return the next message from the connection. If the message is improperly
     * formatted or there are no messages left it returns null.
     *
     * @return the next message from the connection, null if empty or malformed.
     */
    public Message getNextMessage() {
        if (this.connectionStatus == ConnectionStatus.CONNECTION_ESTABLISHED) {
            final Message m = this.receiveMessage();

            if (m != null) {
                if (this.trustedConnection) {
                    return m;
                } else if (this.remoteConnectionName.equals(m.getSender())) {
                    return m;
                } else {
                    NetworkConnection.LOGGER.log(Level.SEVERE, "NetworkConnection: |" + this.remoteConnectionName
                            + "| pretended to be |" + m.getSender() + "|");
                }
            }
        } else {
            NetworkConnection.LOGGER.log(Level.SEVERE,
                    "NetworkConnection: attempted to send receive message on unautheniticated connection");
        }
        return null;
    }

    /**
     * Return the status of the connection.
     *
     * @return the status of the connection
     */
    public ConnectionStatus getStatus() {
        if (this.socket == null || !this.socket.isConnected()) {
            this.connectionStatus = ConnectionStatus.CONNECTION_CLOSED;
        }

        return this.connectionStatus;

    }

    /**
     * Sends a message through the socket this class encapsulates. If an error
     * occurs the connection is closed.
     *
     * @param message
     *            the message to be sent
     * @param owerwriteSender
     *            if true overwrites the sender name with the stored name of the
     *            local server or client
     * @see Message
     */
    public void sendMessage(Message message, boolean owerwriteSender) {
        if (this.connectionStatus == ConnectionStatus.CONNECTION_ESTABLISHED) {
            if (owerwriteSender) {
                message.addEntry(EntryType.SENDER, this.localConnectionName, true);
            }
            this.transmitMessage(message);
        } else {
            NetworkConnection.LOGGER.log(Level.SEVERE, "attempted to send " + message.toString()
                    + " on unautheniticated connection");
        }
    }

    protected Message receiveMessage() {
        String messageText;
        try {
            messageText = this.reader.readLine();
            this.reader.readLine();

            if (messageText == null) {
                this.close();
                return null;
            } else if (!"".equals(messageText)) {
                NetworkConnection.LOGGER.log(Level.INFO, "NetworkConnection: message received:" + messageText);
                return Message.createMessage(messageText);
            } else {
                return null;
            }
        } catch (final IOException e) {
            NetworkConnection.LOGGER.log(Level.INFO, "Connection closed.", e);
            this.close();
        }
        return null;
    }

    protected void transmitMessage(Message message) {
        try {
            NetworkConnection.LOGGER.log(Level.INFO, "NetworkConnection: message sent: " + message.toString());
            this.writer.write(message.toString());
            this.writer.newLine();
            this.writer.flush();
        } catch (final IOException e) {
            NetworkConnection.LOGGER.log(Level.INFO, "Connection closed.", e);
            this.close();
        }
    }

    /**
     * This function is meant to implement the relevant side of the login
     * communication protocol. It is meant to be called regularly until it
     * returns CONNECTION_ESTABLISHED (or CONNECTION_CLOSED)
     *
     * @return the status of the connection
     */
    public abstract ConnectionStatus updateLogin();

    public String getRemoteConnectionName() {
        return this.remoteConnectionName;
    }

}