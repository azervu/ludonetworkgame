package com.enderwolf.ludonet.shared;

import java.util.HashMap;
import java.util.Map;

import com.enderwolf.ludonet.message.Message;

/**
 * Player is a representation of player-specific data, such as the player ID,
 * the user name and other related data.
 */
public class Player {
    private final int playerId;
    private final String userName;
    private NetworkConnection networkConnection;
    private PlayerColor color;
    private boolean inGame;

    private ConnectionLostListener dcListener;
    private boolean dcNotified;

    private PlayerMessageListener defaultMsgListener;
    private final Map<Message.MessageType, PlayerMessageListener> msgListeners;

    // The game pieces are not instantiated before the player has been assigned
    // a PlayerColor.
    private GamePiece[] gamePieces;

    /**
     * Default constructor
     *
     * @param playerId
     *            Id of the player.
     * @param userName
     *            UserName of the player.
     */
    public Player(int playerId, String userName) {
        this.playerId = playerId;
        this.userName = userName;
        this.color = PlayerColor.NONE;
        this.msgListeners = new HashMap<>();
    }

    /**
     * Set the default PlayerMessageListener. Unmapped message types will be
     * handled by this listener.
     *
     * @param listener
     *            The default listener.
     */
    void setDefaultMessageListener(PlayerMessageListener listener) {
        this.defaultMsgListener = listener;
    }

    /**
     * Set a PlayerMessageListener that will handle all messages of the
     * specified type.
     *
     * @param type
     *            The type to handle.
     * @param listener
     *            The listener for this message type.
     */
    public void setMessageListener(Message.MessageType type, PlayerMessageListener listener) {
        this.msgListeners.put(type, listener);
    }

    public void setConnectionLostListener(ConnectionLostListener listener) {
        this.dcListener = listener;
    }

    /**
     * Handle incoming messages and dispatch them to someone appropriate. If the
     * connection is bad, the ConnectionLostListener is notified. See
     * Player#isConnectionGood().
     */
    public void update() {
        if (!this.isConnectionGood()) {
            this.testAndSetInGame(false);
            return;
        }

        Message msg;
        while ((msg = this.networkConnection.getNextMessage()) != null) {
            final Message.MessageType type = msg.getType();

            if (this.msgListeners.containsKey(type) && this.msgListeners.get(type) != null) {
                this.msgListeners.get(type).onMessage(this, msg);
            } else if (this.defaultMsgListener != null) {
                this.defaultMsgListener.onMessage(this, msg);
            }
        }
    }

    /**
     * Called when the Player has disconnected and must notify and games or chat
     * rooms he is connected to.
     */
    public void onDisconnect() {
        this.testAndSetInGame(false);
    }

    /**
     * Retrieve the unique PlayerID. This ID related to the PK in the Database.
     *
     * @return The unique player ID of this Player instance.
     */
    public int getPlayerId() {
        return this.playerId;
    }

    /**
     * Retrieve the user name of this player.
     *
     * @return The user name of this player.
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Set the NetworkConnection.
     *
     * @param con
     *            An active NetworkConnection connected to some remote host.
     */
    public void setNetworkConnection(NetworkConnection con) {
        this.networkConnection = con;
    }

    /**
     * Check if the network connection is good. If the connection is encountered
     * as bad (for the first time per connection), the ConnectionLostListener is
     * notified.
     *
     * @return True if the NetworkConnection is in state CONNECTION_ESTABLISHED,
     *         false otherwise.
     */
    public boolean isConnectionGood() {
        boolean good = true;

        if (this.networkConnection == null) {
            good = false;
        } else if (this.networkConnection.getStatus() != NetworkConnection.ConnectionStatus.CONNECTION_ESTABLISHED) {
            good = false;
        }

        if (!good) {
            // Don't notify the ConnectionLostListener if we already have.
            if (!this.dcNotified) {
                this.dcNotified = true;

                if (this.dcListener != null) {
                    this.dcListener.onPlayerDisconnected(this);
                }
            }
        } else {
            /*
             * If we have previously notified the ConnectionLostListener but now
             * have a good connection again, un-flag "dcNotified" so the
             * listener will be notified if something goes sour again.
             */
            this.dcNotified = false;
        }

        return good;
    }

    /**
     * Send a message on the NetworkConnection.
     *
     * @param message
     *            The message to send.
     */
    public void sendMessage(Message message) {
        if (this.networkConnection != null) {
            this.networkConnection.sendMessage(message, true);
        }
    }

    /**
     * Set the PlayerColor. This must be done before instantiating the game
     * pieces.
     *
     * @param color
     *            The PlayerColor this Player will take.
     */
    public void setPlayerColor(PlayerColor color) {
        this.color = color;
    }

    /**
     * @return The PlayerColor of this Player.
     */
    public PlayerColor getPlayerColor() {
        return this.color;
    }

    /**
     * Instantiate the game pieces if the player color has already been
     * assigned.
     */
    public void instantiateGamePieces() {
        final int firstSpawn = GameTrail.getPlayerTrail(this.color).getFirstSpawnIndex();

        this.gamePieces = new GamePiece[4];
        for (int i = 0; i < 4; i++) {
            this.gamePieces[i] = new GamePiece(this.color);
            this.gamePieces[i].setPosition(firstSpawn + i);
        }
    }

    /**
     * Retrieve a GamePiece belonging to this Player object.
     *
     * @param index
     *            The index of the game piece. Must be in range [0..3].
     * @return A valid GamePiece if this player is part of a game, null
     *         otherwise.
     */
    public GamePiece getGamePiece(int index) {
        if (index < 0 || index > 3 || this.gamePieces == null) {
            return null;
        }

        return this.gamePieces[index];
    }

    /**
     * Check if the Player has landed in the finish tile with all of his pieces.
     *
     * @return True if the player is finished.
     */
    public boolean hasFinishedGame() {
        if (this.gamePieces == null) {
            return false;
        }

        for (int i = 0; i < 4; i++) {
            if (this.gamePieces[i] == null || !this.gamePieces[i].isFinished()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set if the player is in a game or not.
     *
     * @param inGame
     *            Whether or not the Player is in a game.
     * @return True if the player was currently part of a game, false otherwise.
     */
    public synchronized boolean testAndSetInGame(boolean inGame) {
        final boolean wasIngame = this.inGame;
        this.inGame = inGame;
        return wasIngame;
    }

    /**
     * Check if the player is in a game.
     *
     * @return True if the player is currently part of a game, false otherwise.
     */
    public boolean getInGame() {
        return this.inGame;
    }

    /**
     * Returns the string representation of this player, the name.
     *
     * @return The string representation of this player.
     */
    @Override
    public String toString() {
        return this.userName;
    }

    /**
     * Checks if another object is equal to this.
     *
     * @return True if the same, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        } else {
            return ((Player) obj).playerId == this.playerId;
        }
    }

    /**
     * "Calculates" the hash of this player.
     *
     * @return The hash as an integer.
     */
    @Override
    public int hashCode() {
        return this.playerId;
    }
}
