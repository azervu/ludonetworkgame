package com.enderwolf.ludonet.shared;

import java.awt.Color;

/**
 * represents a player with a certain color in the game.
 */
public enum PlayerColor {
    GREEN(0),
    RED(1),
    BLUE(2),
    YELLOW(3),
    NONE(-1);

    private int value;

    private PlayerColor(int value) {
        this.value = value;
    }

    /**
     * Convert the enum to int.
     *
     * @return The integral value of this enum.
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Retrieve a Color-instance that has the same color as this instance
     * represents.
     *
     * @return A Color matching the enums-value.
     */
    public Color getColor() {
        switch (this) {
            case GREEN:
                return Color.GREEN;
            case RED:
                return Color.RED;
            case BLUE:
                return Color.BLUE;
            case YELLOW:
                return Color.YELLOW;
            case NONE:
            default:
                return Color.WHITE;
        }
    }

    /**
     * Modified the default colors slightly by making them brighter.
     *
     * @return A brighter color than PlayerColor#getColor().
     */
    public Color getBrightColor() {
        final Color c = this.getColor();

        final float[] comps = c.getColorComponents(null);

        for (int i = 0; i < comps.length; i++) {
            float f = comps[i];
            f += 0.25f;
            f = Math.min(1.f, f);
            comps[i] = f;
        }

        return new Color(comps[0], comps[1], comps[2]);
    }
}
