package com.enderwolf.ludonet.shared;

import com.enderwolf.ludonet.message.Message;

/**
 * PlayerMessageListener is an interface that allows arbitrary objects to be
 * notified whenever a Player receives a specific Message. The Player object
 * itself does not handle incoming Messages, but relies on externals to do so.
 */
public interface PlayerMessageListener {
    /**
     * Called when a Message is received on the Player's NetworkConnection.
     *
     * @param player
     *            The Player that received the message.
     * @param msg
     *            The message that was received.
     */
    void onMessage(Player player, Message msg);
}
