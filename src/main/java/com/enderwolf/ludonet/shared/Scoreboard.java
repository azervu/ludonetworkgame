package com.enderwolf.ludonet.shared;

import java.util.ArrayList;
import java.util.List;

/**
 * Scoreboard contains the scoreboard data which is displayed to the user on the
 * client application. The class is serializable to String for easy
 * Message-integration.
 *
 * To prevent massive bandwidth, Scoreboards should be transmitted from the
 * server to the client in smaller hunks of manageable entries (10-20 should be
 * fine). Scoreboard-instances can "consume" each other; one Scoreboard may
 * append all entries from another Scoreboard to it's own entries.
 *
 * This class commonly refers to the "absolute scoreboard". This is the
 * scoreboard as it exists in it's completeness. The Scoreboard class represents
 * only a subset of this absolute scoreboard.
 *
 * The format of a String-serialized Scoreboard is:
 * "firstIndex;name,score;name,score;"
 *
 * Serialized scoreboards can be verified for validity by checking if they match
 * SERIALIZED_REGEX_PATTERN.
 *
 * At least one name-score pair (Entry) is required. A semi-colon must terminate
 * ALL entries, even if the Scoreboard only contains one. The "firstIndex" value
 * is also always required, and must hold the absolute scoreboard index of the
 * first entry.
 */
public class Scoreboard {
    /**
     * Exception thrown by Scoreboard when invalid String-serialization errors
     * are encountered.
     */
    public class DeserializeException extends Exception {
        private static final long serialVersionUID = -3742933308272475300L;

        public DeserializeException(String desc) {
            super(desc);
        }
    }

    /**
     * Scoreboard.Entry is an entry in the Scoreboard.
     */
    public static class Entry {
        String name;
        int score;

        /**
         * Create a Scoreboard.Entry.
         *
         * @param name
         *            The name of the Player.
         * @param score
         *            The score of the Player.
         */
        public Entry(String name, int score) {
            this.name = name;
            this.score = score;
        }

        /**
         * Retrieve the name of the player.
         *
         * @return The name of the Player this Entry is related to.
         */
        public String getName() {
            return this.name;
        }

        /**
         * Retrieve ths score of the player.
         *
         * @return The score of the player this Entry is related to.
         */
        public int getScore() {
            return this.score;
        }

        @Override
        public String toString() {
            return this.name + " " + this.score;
        }
    }

    private static final String SERIALIZED_REGEX_PATTERN = "^[0-9]+;([^,;]+,[0-9]+;)+$";

    private int firstIndex;
    private List<Entry> entries;

    /**
     * Default constructor.
     */
    public Scoreboard() {
        this.firstIndex = 0;
        this.entries = new ArrayList<>();
    }

    /**
     * Create a Scoreboard from a list of Scoreboard.Entry-objects.
     *
     * @param entries
     *            The list of scoreboard entries.
     * @param firstIndex
     *            The index of the first entry in the Entry-list.
     */
    public Scoreboard(List<Entry> entries, int firstIndex) {
        this.entries = new ArrayList<>(entries);
        this.firstIndex = firstIndex;
    }

    /**
     * Create a Scoreboard from a String-serialized Scoreboard.
     *
     * @param serialized
     *            The String-serialized Scoreboard.
     * @throws com.enderwolf.ludonet.shared.Scoreboard.DeserializeException
     *             If the format of "serialized" is invalid.
     */
    public Scoreboard(String serialized) throws DeserializeException {
        this.entries = new ArrayList<>();

        this.deserializeScoreboard(serialized);
    }

    /**
     * Merge a String-serialized scoreboard into this Scoreboard.
     *
     * @param serialized
     *            The String-serialized Scoreboard.
     * @throws com.enderwolf.ludonet.shared.Scoreboard.DeserializeException
     *             If the format of "serialized" is invalid.
     */
    public void deserializeScoreboard(String serialized) throws DeserializeException {
        if (serialized == null || serialized.isEmpty()) {
            throw new DeserializeException("Unable to parse null or empty String");
        }

        // Check for basic format conditions.
        if (!serialized.matches(Scoreboard.SERIALIZED_REGEX_PATTERN)) {
            throw new DeserializeException("Invalid string given: " + serialized);
        }

        // Get all entries
        final String[] serializedEntries = serialized.split(";");

        // Get the first index
        this.firstIndex = Integer.parseInt(serializedEntries[0]);

        if (this.firstIndex < 0) {
            throw new DeserializeException("Negative first-indices are illegal: " + serialized);
        }

        // Iterate over all entries
        for (int i = 1; i < serializedEntries.length; i++) {
            final String entry = serializedEntries[i];
            final String[] pair = entry.split(",");

            for (final String p : pair) {
                if (p == null || p.isEmpty()) {
                    throw new DeserializeException("Empty value in value-pair: " + entry);
                }
            }

            final String name = pair[0];
            final int score = Integer.parseInt(pair[1]);

            this.entries.add(new Entry(name, score));
        }
    }

    /**
     * Merge another scoreboard onto this scoreboard. If any fragmentation
     * occurs between the two sets of etnries, the missing fields will contain
     * "null". It is then expected that they will be filled at a future time.
     *
     * @param scoreboard
     *            The Scoreboard to merge into this one. This Object will never
     *            be changed (permanently) by this merge operation.
     */
    public void mergeScoreboard(Scoreboard scoreboard) {
        if (scoreboard == this) {
            return;
        }

        // If "scoreboard" begins in the absolute Scoreboard BEFORE "this",
        // merge "this" into
        // "scoreboard" and copy "scoreboard"'s Entry list.
        final int offset = scoreboard.getFirstIndex() - this.getFirstIndex();
        if (offset < 0) {
            // Copy the entries of scoreboard.
            final List<Entry> tempEntries = new ArrayList<>(scoreboard.entries);
            final int tempFirst = scoreboard.firstIndex;

            // Append self onto scoreboard
            scoreboard.mergeScoreboard(this);

            // Copy the entries of scoreboard.
            this.entries = scoreboard.entries;
            this.firstIndex = scoreboard.firstIndex;

            // Revert the changes made to scoreboard
            scoreboard.entries = tempEntries;
            scoreboard.firstIndex = tempFirst;
        } else {
            // Allocate space in the existing list by filling it with nulls.
            // This will fill undefined regions
            // (fragmentation) with (hopefully) temporary null-values and makes
            // the code for overwriting existing
            // entries prettier.
            final int myFinal = this.getFirstIndex() + this.getSize();
            final int otherFinal = scoreboard.getFirstIndex() + scoreboard.getSize();
            final int finalLength = Math.max(myFinal, otherFinal);

            while (this.entries.size() < finalLength) {
                this.entries.add(null);
            }

            for (int i = 0; i < scoreboard.getSize(); i++) {
                // set the new values. This will override existing values and
                this.entries.set(i + offset, scoreboard.entries.get(i));
            }
        }
    }

    /**
     * Override the existing entries. This method will discard all existing
     * content and replace is with the new.
     *
     * @param entries
     *            The list of entries.
     * @param firstIndex
     *            The first absolute scoreboard-index of the first element in
     *            "entries".
     */
    public void setEntries(List<Entry> entries, int firstIndex) {
        this.entries = entries;
        this.firstIndex = firstIndex;
    }

    /**
     * Retrieve the list of entries. The first index of the returned list
     * corresponds to the "first index"-th element in the absolute scoreboard.
     *
     * @return The list of entries.
     */
    public List<Entry> getEntries() {
        return this.entries;
    }

    /**
     * Retrieve the first index of this Entry-set. Scoreboards can hold any
     * chunk anywhere in the high-score list. The "first index" refers to the
     * index in the absolute high-score list where the first object of "entries"
     * can be found.
     *
     * @return The first index of this Entry-set.
     */
    public int getFirstIndex() {
        return this.firstIndex;
    }

    /**
     * Retrieve the total number of score entries. This number includes
     * "null"-Entries hailing from a fragmentation error when merging two
     * Scoreboards.
     *
     * @return The total number of score entries.
     */
    public int getSize() {
        return this.entries.size();
    }

    /**
     * Serialize this Scoreboard to a String. The format is described in the
     * javadoc of the Scoreboard class.
     *
     * @return Serialized Scoreboard, null if there are no entries in this
     *         Scoreboard.
     */
    public String serializeToString() {
        if (this.entries.isEmpty()) {
            return null;
        }

        final StringBuilder sb = new StringBuilder();

        sb.append(this.firstIndex);
        sb.append(';');

        for (final Entry e : this.entries) {
            sb.append(e.getName());
            sb.append(',');
            sb.append(e.getScore());
            sb.append(';');
        }

        return sb.toString();
    }

    public enum SCOREBOARD_TYPE {
        TOTAL_WINS,
        TOTAL_GAMES
    }
}
