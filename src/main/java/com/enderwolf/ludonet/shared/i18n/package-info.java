/**
 * This package contains all I18N translation material and other things that may vary between languages.
 *
 * @author !Tulingen
 */
package com.enderwolf.ludonet.shared.i18n;