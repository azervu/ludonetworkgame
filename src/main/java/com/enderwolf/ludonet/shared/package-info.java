/**
 * This package contains all classes representing the core Server codebase.
 *
 * @author !Tulingen
 */
package com.enderwolf.ludonet.shared;