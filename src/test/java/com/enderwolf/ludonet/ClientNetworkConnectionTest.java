package com.enderwolf.ludonet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.client.ClientNetworkConnection;
import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.NetworkConnection.ConnectionStatus;

public class ClientNetworkConnectionTest extends TestCase {

    ClientNetworkConnection clientConnection;

    Socket serverConnection;
    BufferedWriter serverWriter;
    BufferedReader serverReader;

    @Override
    public void setUp() throws IOException, InterruptedException {
        final Thread clientLoginThread = new Thread(() -> {
            try {
                this.clientConnection = new ClientNetworkConnection(InetAddress.getLocalHost());
                this.clientConnection.login("testUser", "testPassword", false);
                while (this.clientConnection.updateLogin() != ConnectionStatus.CONNECTION_ESTABLISHED) {
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        });
        clientLoginThread.setDaemon(true);
        System.setProperty("javax.net.ssl.keyStore", "mySrvKeystore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");
        System.setProperty("javax.net.ssl.trustStore", "mySrvKeystore");

        final ServerSocket serverSocket = ServerSocketFactory.getDefault().createServerSocket(23456);
        clientLoginThread.start();
        this.serverConnection = serverSocket.accept();
        serverSocket.close();
        final SSLSocket loginSocket = (SSLSocket) ((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket(
                this.serverConnection, null, this.serverConnection.getPort(), false);

        loginSocket.setUseClientMode(false);

        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(loginSocket.getOutputStream()));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(loginSocket.getInputStream()));

        String received;
        received = reader.readLine();

        Assert.assertEquals("MESSAGE_TYPE:LOGIN	SENDER:testUser:ESCAPE	PASSWORD:testPassword:ESCAPE	CREATE_USER:true	",
                received);
        final Message msg = new Message(MessageType.CONNECTION_STATUS, null);
        msg.addEntry(EntryType.CONNECTION_STATUS, ConnectionStatus.CONNECTION_ESTABLISHED.toString(), true);
        writer.write(msg.toString());
        writer.flush();

        writer.close();
        reader.close();
        loginSocket.close();

        this.serverWriter = new BufferedWriter(new OutputStreamWriter(this.serverConnection.getOutputStream()));
        this.serverReader = new BufferedReader(new InputStreamReader(this.serverConnection.getInputStream()));

        clientLoginThread.join();
    }

    public void testSendChatMessage() throws IOException {
        final Message msg = new Message(MessageType.CHAT);
        msg.addEntry(EntryType.CHAT_TEXT, "hello", true);
        this.clientConnection.sendMessage(msg, true);
        String received;
        received = this.serverReader.readLine();
        Assert.assertEquals("CHAT_TEXT:hello:ESCAPE	MESSAGE_TYPE:CHAT	SENDER:testUser:ESCAPE	", received);
    }

    public void testReceiveChatMessage() throws IOException {

        final BufferedWriter serverWriter = new BufferedWriter(new OutputStreamWriter(
                this.serverConnection.getOutputStream()));
        final Message msg = new Message(MessageType.CHAT);
        msg.addEntry(EntryType.CHAT_TEXT, "hello", true);
        serverWriter.write(msg.toString());
        serverWriter.flush();
        Message m = null;
        while (m == null) {
            m = this.clientConnection.getNextMessage();
        }

        Assert.assertEquals("CHAT_TEXT:hello:ESCAPE	MESSAGE_TYPE:CHAT	SENDER:null:ESCAPE	\n", m.toString());
    }

    @Override
    public void tearDown() throws IOException {
        this.clientConnection.close();
        this.serverConnection.close();

    }

}
