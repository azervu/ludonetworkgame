package com.enderwolf.ludonet;

import javax.swing.JTextField;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.client.gui.login.CredentialsVerifier;

public class CredentialsVerifierTests extends TestCase {
    private JTextField userField;
    private JTextField passField;
    private JTextField passRepField;
    private CredentialsVerifier verifier;

    @Override
    public void setUp() {
        this.userField = new JTextField();
        this.passField = new JTextField();
        this.passRepField = new JTextField();
        this.verifier = new CredentialsVerifier(this.userField, this.passField);
    }

    public void testValidStrings() {
        // Usernames must be exclusively alphanumerical.
        // Passwords can be anything as long as it's length is > 4.
        this.userField.setText("userName");
        this.passField.setText("password");
        Assert.assertEquals(CredentialsVerifier.Status.OK, this.verifier.verifyCredentials());

        this.userField.setText("пуссидестроиер69");
        this.passField.setText("      %% && !!");
        Assert.assertEquals(CredentialsVerifier.Status.OK, this.verifier.verifyCredentials());
    }

    public void testInvlaidStrings() {
        this.userField.setText("invalid_username");
        this.passField.setText("valid_password&&&");
        Assert.assertEquals(CredentialsVerifier.Status.INVALID_USERNAME, this.verifier.verifyCredentials());

        this.userField.setText("invalid username");
        Assert.assertEquals(CredentialsVerifier.Status.INVALID_USERNAME, this.verifier.verifyCredentials());

        this.userField.setText("xX_quIcKzScoPPz69_Xx");
        Assert.assertEquals(CredentialsVerifier.Status.INVALID_USERNAME, this.verifier.verifyCredentials());

        this.userField.setText("validUsername");
        this.passField.setText("no");
        Assert.assertEquals(CredentialsVerifier.Status.INVALID_PASSWORD, this.verifier.verifyCredentials());
    }

    public void testInvalidRepetition() {
        this.verifier.setPasswordRepeatField(this.passRepField);

        this.userField.setText("validUsername");
        this.passField.setText("validPassword");
        this.passRepField.setText("invalidRepetition");
        Assert.assertEquals(CredentialsVerifier.Status.UNEQUAL_PASSWORD, this.verifier.verifyCredentials());
    }

    public void testValidRepetition() {
        this.verifier.setPasswordRepeatField(this.passRepField);

        this.userField.setText("validUsername");
        this.passField.setText("validPassword");
        this.passRepField.setText("validPassword");
        Assert.assertEquals(CredentialsVerifier.Status.OK, this.verifier.verifyCredentials());
    }
}
