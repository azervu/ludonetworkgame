package com.enderwolf.ludonet;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.server.ChatRoom;
import com.enderwolf.ludonet.server.Database;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.Scoreboard;

import debug.TestDatabase;

public class DatabaseTests extends TestCase {
    TestDatabase database;

    @Override
    public void setUp() {
        this.database = new TestDatabase();
        Assert.assertTrue(this.database.clearDatabase());
        this.database.initialize();
    }

    @Override
    public void tearDown() {
        Assert.assertTrue(this.database.clearDatabase());
    }

    public void testCreateNewUser() {
        Assert.assertTrue(this.database.registerUser("userName", "password"));
    }

    public void testMultipleUsersWithSameName() {
        Assert.assertTrue(this.database.registerUser("userName", "password"));
        Assert.assertFalse(this.database.registerUser("userName", "alligator123"));
        Assert.assertTrue(this.database.registerUser("someOtherUser", "lion >:3"));
    }

    public void testVerifyLogin() {
        Assert.assertTrue(this.database.registerUser("userName", "password"));

        Assert.assertTrue(this.database.verifyLogin("userName", "password"));
        Assert.assertFalse(this.database.verifyLogin("userName", "password1"));
        Assert.assertFalse(this.database.verifyLogin("userName", "PLSS"));
        Assert.assertFalse(this.database.verifyLogin("fartface", "password"));
    }

    public void testCreatePlayer() {
        Assert.assertTrue(this.database.registerUser("face", "passw00rd"));

        final Player player = this.database.getPlayer("face");
        Assert.assertNotNull(player);
        Assert.assertEquals("face", player.getUserName());
        Assert.assertTrue(player.getPlayerId() > 0);
    }

    public void testCreateChatRoom() {
        Assert.assertTrue(this.database.createChatRoom("myFknChatRoom") > 0);
        Assert.assertTrue(this.database.createChatRoom("myFknChatRoom") > 0);
        Assert.assertTrue(this.database.createChatRoom("anotherChatRoOm") > 0);
    }

    public void testJoinChatRoom() {
        final int chatRoomId = this.database.createChatRoom("myFknChatRoom");
        Assert.assertTrue(chatRoomId > 0);

        Assert.assertTrue(this.database.registerUser("userName", "password"));

        // The Player object can later be constructed
        final Player player = this.database.getPlayer("userName");
        Assert.assertNotNull(player);

        // You are only able to join a test room once. Joining twice results in
        // a false
        // on the second attempt (as no changes were done).
        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, player));
        Assert.assertFalse(this.database.joinChatRoom(chatRoomId, player));
    }

    public void testGetPlayerListFromChatRoom() {
        final int chatRoomId = this.database.createChatRoom("myFknChatRoom");
        Assert.assertTrue(chatRoomId > 0);

        Assert.assertTrue(this.database.registerUser("1", "password"));
        Assert.assertTrue(this.database.registerUser("2", "password"));
        Assert.assertTrue(this.database.registerUser("3", "password"));

        final Player p1 = this.database.getPlayer("1");
        final Player p2 = this.database.getPlayer("2");
        final Player p3 = this.database.getPlayer("3");

        Assert.assertNotNull(p1);
        Assert.assertNotNull(p2);
        Assert.assertNotNull(p3);

        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, p1));
        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, p2));
        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, p3));

        final List<Player> players = this.database.getPlayersInChatRoom(chatRoomId);
        Assert.assertEquals(3, players.size());

        final boolean[] found = new boolean[3];
        for (int i = 0; i < players.size(); i++) {
            final int id = Integer.parseInt(players.get(i).getUserName());
            found[id - 1] = true;
        }

        for (final boolean f : found) {
            Assert.assertTrue(f);
        }
    }

    public void testLeaveChatRoom() {
        final int chatRoomId = this.database.createChatRoom("myFknChatRoom");
        Assert.assertTrue(chatRoomId > 0);

        Assert.assertTrue(this.database.registerUser("1", "password"));
        Assert.assertTrue(this.database.registerUser("2", "password"));

        final Player p1 = this.database.getPlayer("1");
        final Player p2 = this.database.getPlayer("2");

        Assert.assertNotNull(p1);
        Assert.assertNotNull(p2);

        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, p1));
        Assert.assertTrue(this.database.joinChatRoom(chatRoomId, p2));

        // Get the initial list - two players are currently in the chat room
        List<Player> players = this.database.getPlayersInChatRoom(chatRoomId);
        Assert.assertEquals(2, players.size());

        final boolean[] found = new boolean[2];
        for (int i = 0; i < players.size(); i++) {
            final int id = Integer.parseInt(players.get(i).getUserName());
            found[id - 1] = true;
        }

        Assert.assertTrue(found[0]);
        Assert.assertTrue(found[1]);

        // Remove player 2 from the chat room (this method only returns true if
        // the player
        // was actually removed from the chat room).
        Assert.assertTrue(this.database.leaveChatRoom(chatRoomId, p2));
        Assert.assertFalse(this.database.leaveChatRoom(chatRoomId, p2));

        players = this.database.getPlayersInChatRoom(chatRoomId);
        Assert.assertEquals(1, players.size());

        Assert.assertEquals(players.get(0).getPlayerId(), p1.getPlayerId());
        Assert.assertEquals(players.get(0).getUserName(), p1.getUserName());
    }

    public void testMakeFriends() {
        Assert.assertTrue(this.database.registerUser("1", "pass"));
        Assert.assertTrue(this.database.registerUser("2", "pass"));

        final Player p1 = this.database.getPlayer("1");
        final Player p2 = this.database.getPlayer("2");
        Assert.assertNotNull(p1);
        Assert.assertNotNull(p2);

        Assert.assertTrue(this.database.makeFriends(p1, p2));
        Assert.assertFalse(this.database.makeFriends(p1, p2));

        // Ensure that the friends list match mutually
        final List<Player> f1 = this.database.getFriends(p1);
        final List<Player> f2 = this.database.getFriends(p2);

        Assert.assertEquals(1, f1.size());
        Assert.assertEquals(1, f2.size());

        Assert.assertEquals(f1.get(0).getPlayerId(), p2.getPlayerId());
        Assert.assertEquals(f1.get(0).getUserName(), p2.getUserName());

        Assert.assertEquals(f2.get(0).getPlayerId(), p1.getPlayerId());
        Assert.assertEquals(f2.get(0).getUserName(), p1.getUserName());
    }

    public void testUnmakeFriends() {
        Assert.assertTrue(this.database.registerUser("1", "pass"));
        Assert.assertTrue(this.database.registerUser("2", "pass"));

        final Player p1 = this.database.getPlayer("1");
        final Player p2 = this.database.getPlayer("2");
        Assert.assertNotNull(p1);
        Assert.assertNotNull(p2);

        Assert.assertTrue(this.database.makeFriends(p1, p2));
        Assert.assertFalse(this.database.makeFriends(p1, p2));

        Assert.assertEquals(1, this.database.getFriends(p1).size());
        Assert.assertEquals(1, this.database.getFriends(p2).size());

        // Remove "p2" as a friend of "p1". p1 should no longer be friend of p2.
        Assert.assertTrue(this.database.unmakeFriends(p2, p1));
        Assert.assertFalse(this.database.unmakeFriends(p2, p1));

        Assert.assertEquals(0, this.database.getFriends(p1).size());
        Assert.assertEquals(0, this.database.getFriends(p2).size());
    }

    public void testChatRoomCacheUnlocked() {
        final Database.ChatRoomCache cache = this.database.getChatRoomCache();
        Assert.assertNotNull(cache);

        // The ChatRoomCache is unlocked, getChatRooms() should throw exceptions
        boolean except = false;
        try {
            cache.getChatRooms();
        } catch (final Exception e) {
            except = true;
        }

        Assert.assertTrue(except);
    }

    public void testChatRoomCacheLocked() {
        final Database.ChatRoomCache cache = this.database.getChatRoomCache();

        try {
            cache.lock();

            // There should be one chat room in the cache, the "Global Chat" (id
            // 1)
            Assert.assertNotNull(cache.getChatRooms());
            Assert.assertEquals(1, cache.getChatRooms().size());
        } finally {
            cache.unlock();
        }
    }

    public void testGlobalChatExists() {
        final Database.ChatRoomCache cache = this.database.getChatRoomCache();

        try {
            cache.lock();

            // There should be one chat room in the cache, the "Global Chat" (id
            // 1)
            final ChatRoom croom = cache.getChatRoom(1);
            Assert.assertNotNull(croom);
            Assert.assertEquals("Global Chat", croom.getRoomName());
            Assert.assertEquals(1, croom.getRoomId());
        } finally {
            cache.unlock();
        }
    }

    public void testSynchronizedChatRoomCacheList() {
        final Database.ChatRoomCache cache = this.database.getChatRoomCache();

        try {
            cache.lock();
            Assert.assertNotNull(cache.getChatRooms());
            Assert.assertEquals(1, cache.getChatRooms().size());
        } finally {
            cache.unlock();
        }

        // Create the room - this should affect our cache instance.
        final int roomId = this.database.createChatRoom("your mom");
        Assert.assertTrue(roomId > 1);

        try {
            cache.lock();

            // The list of rooms must not be null, and must contain 2 elements
            Assert.assertNotNull(cache.getChatRooms());
            Assert.assertEquals(2, cache.getChatRooms().size());

            // The rooms must be equal
            Assert.assertNotNull(cache.getChatRoom(roomId));
            Assert.assertTrue(cache.getChatRooms().get(1) == cache.getChatRoom(roomId));
        } finally {
            cache.unlock();
        }
    }

    public void testSynchronizedChatRoomMemberList() {
        final Database.ChatRoomCache cache = this.database.getChatRoomCache();

        final int roomId = this.database.createChatRoom("whole lotta fun");
        Assert.assertTrue(roomId > 0);

        // Ensure that the room exists in our cache, and that it has no members.
        try {
            cache.lock();
            Assert.assertNotNull(cache.getChatRoom(roomId).getPlayerList());
            Assert.assertEquals(0, cache.getChatRoom(roomId).getPlayerList().size());
        } finally {
            cache.unlock();
        }

        Assert.assertTrue(this.database.registerUser("user", "pass"));
        final Player player = this.database.getPlayer("user");
        Assert.assertTrue(this.database.joinChatRoom(roomId, player));

        // Ensure that the chat room now contains one player.
        try {
            cache.lock();
            Assert.assertEquals(1, cache.getChatRoom(roomId).getPlayerList().size());
            Assert.assertEquals(player.getPlayerId(), cache.getChatRoom(roomId).getPlayerList().get(0).getPlayerId());
        } finally {
            cache.unlock();
        }
    }

    public void testInsertChatMessageWithInvalidChatRoom() {
        Assert.assertTrue(this.database.registerUser("user", "alligator123"));
        final Player player = this.database.getPlayer("user");
        Assert.assertNotNull(player);

        // No chat rooms exist - this should fail.
        Assert.assertFalse(this.database.addChatMessage(player, 1, "this is message hello"));
    }

    public void testInsertChatMessageWithUnrelatedPlayer() {
        Assert.assertTrue(this.database.registerUser("user", "alligator123"));
        final Player player = this.database.getPlayer("user");
        Assert.assertNotNull(player);

        final int roomId = this.database.createChatRoom("roompls");
        Assert.assertTrue(roomId > 0);

        // The player is not in the chat room. Sending messages should fail.
        Assert.assertFalse(this.database.addChatMessage(player, roomId, "message yo"));
    }

    public void testInsertValidChatMessage() {
        Assert.assertTrue(this.database.registerUser("user", "alligator123"));
        final Player player = this.database.getPlayer("user");
        Assert.assertNotNull(player);

        final int roomId = this.database.createChatRoom("roompls");
        Assert.assertTrue(roomId > 0);

        Assert.assertTrue(this.database.joinChatRoom(roomId, player));

        Assert.assertTrue(this.database.addChatMessage(player, roomId, "HEY GUYS WHATS UP LUDO YEAH?"));
    }

    public void testScoreboardOutOfBounds() {
        this.fillDummyScoreboard();
        Scoreboard sb;

        sb = this.database.getTotalWinsScoreboard(6, 10);
        Assert.assertNotNull(sb);
        Assert.assertEquals(0, sb.getSize());
        Assert.assertEquals(6, sb.getFirstIndex());

        sb = this.database.getTotalGamesScoreboard(6, 10);
        Assert.assertNotNull(sb);
        Assert.assertEquals(0, sb.getSize());
        Assert.assertEquals(6, sb.getFirstIndex());
    }

    public void testScoreboardIndexAndSize() {
        this.fillDummyScoreboard();

        // Verify that the sizes and bounds are correct
        Scoreboard sb = this.database.getTotalGamesScoreboard(0, 6);
        Assert.assertNotNull(sb);
        Assert.assertEquals(4, sb.getSize());
        Assert.assertEquals(0, sb.getFirstIndex());

        sb = this.database.getTotalWinsScoreboard(0, 6);
        Assert.assertNotNull(sb);
        Assert.assertEquals(4, sb.getSize());
        Assert.assertEquals(0, sb.getFirstIndex());

    }

    public void testCorrectScoreboardValues() {
        this.fillDummyScoreboard();

        Scoreboard sb;

        // Verify the following total win scoreboard: user3(10), user1(9),
        // user4(2), user2(1)
        sb = this.database.getTotalWinsScoreboard(0, 4);
        Assert.assertEquals("user3", sb.getEntries().get(0).getName());
        Assert.assertEquals("user1", sb.getEntries().get(1).getName());
        Assert.assertEquals("user4", sb.getEntries().get(2).getName());
        Assert.assertEquals("user2", sb.getEntries().get(3).getName());

        Assert.assertEquals(10, sb.getEntries().get(0).getScore());
        Assert.assertEquals(9, sb.getEntries().get(1).getScore());
        Assert.assertEquals(2, sb.getEntries().get(2).getScore());
        Assert.assertEquals(1, sb.getEntries().get(3).getScore());

        // Verify the following total games scoreboard: user2(30), user1(15),
        // user3(10), user4(2)
        sb = this.database.getTotalGamesScoreboard(0, 4);
        Assert.assertEquals("user2", sb.getEntries().get(0).getName());
        Assert.assertEquals("user1", sb.getEntries().get(1).getName());
        Assert.assertEquals("user3", sb.getEntries().get(2).getName());
        Assert.assertEquals("user4", sb.getEntries().get(3).getName());

        Assert.assertEquals(30, sb.getEntries().get(0).getScore());
        Assert.assertEquals(15, sb.getEntries().get(1).getScore());
        Assert.assertEquals(10, sb.getEntries().get(2).getScore());
        Assert.assertEquals(2, sb.getEntries().get(3).getScore());
    }

    public void testGetPartialScoreboard() {
        this.fillDummyScoreboard();

        Scoreboard sb;

        // Test the Total Wins board
        sb = this.database.getTotalWinsScoreboard(2, 5);
        Assert.assertEquals(2, sb.getFirstIndex());
        Assert.assertEquals(2, sb.getSize());

        Assert.assertEquals("user4", sb.getEntries().get(0).getName());
        Assert.assertEquals("user2", sb.getEntries().get(1).getName());
        Assert.assertEquals(2, sb.getEntries().get(0).getScore());
        Assert.assertEquals(1, sb.getEntries().get(1).getScore());

        // Test the Total Games board
        sb = this.database.getTotalGamesScoreboard(2, 5);
        Assert.assertEquals(2, sb.getFirstIndex());
        Assert.assertEquals(2, sb.getSize());

        Assert.assertEquals("user3", sb.getEntries().get(0).getName());
        Assert.assertEquals("user4", sb.getEntries().get(1).getName());
        Assert.assertEquals(10, sb.getEntries().get(0).getScore());
        Assert.assertEquals(2, sb.getEntries().get(1).getScore());
    }

    /**
     * Fill the database with 4 users and give them various scores.
     */
    private void fillDummyScoreboard() {
        Assert.assertTrue(this.database.registerUser("user1", "pass"));
        Assert.assertTrue(this.database.registerUser("user2", "pass"));
        Assert.assertTrue(this.database.registerUser("user3", "pass"));
        Assert.assertTrue(this.database.registerUser("user4", "pass"));

        this.addPlayerScore(this.database.getPlayer("user1"), 15, 9);
        this.addPlayerScore(this.database.getPlayer("user2"), 30, 1);
        this.addPlayerScore(this.database.getPlayer("user3"), 10, 10);
        this.addPlayerScore(this.database.getPlayer("user4"), 2, 2);
    }

    private void addPlayerScore(Player player, int games, int wins) {
        Assert.assertNotNull(player);
        Assert.assertNotNull(this.database);

        // If this fails, you are doing it wong. Only Taeja can win more than he
        // can play.
        Assert.assertTrue(wins <= games);

        for (int i = 0; i < games; i++) {
            final boolean win = (i < wins);
            Assert.assertTrue(this.database.awardPoints(player, win));
        }
    }

}
