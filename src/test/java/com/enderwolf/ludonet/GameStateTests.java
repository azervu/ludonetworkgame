package com.enderwolf.ludonet;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.shared.GamePiece;
import com.enderwolf.ludonet.shared.GameState;
import com.enderwolf.ludonet.shared.GameTrail;
import com.enderwolf.ludonet.shared.Player;
import com.enderwolf.ludonet.shared.PlayerColor;

class DummyGameState extends GameState {
    public DummyGameState(Player[] players) {
        super(players);
    }

    public GamePiece[] getTileMap() {
        return this.tileMap;
    }

    @Override
    public Player[] getPlayers() {
        return this.players;
    }

    public int getNumDice() {
        return 1;
    }

    /**
     * Forcefully move a game piece belonging to a player to a certain position.
     * The destination tile MUST be unoccupied.
     *
     * @param color
     *            The color of the player to move.
     * @param pieceIdx
     *            The index of the game piece to move.
     * @param absolutePosition
     *            The absolute position the piece will be placed at.
     * @return True on success, false on error.
     */
    public boolean forceMove(PlayerColor color, int pieceIdx, int absolutePosition) {
        final GamePiece piece = this.players[color.getValue()].getGamePiece(pieceIdx);

        if (this.tileMap[absolutePosition] != null) {
            return false;
        }

        final int prevPos = piece.getPosition();
        if (prevPos == absolutePosition) {
            // Pretend we did something useful.
            return true;
        }

        this.tileMap[prevPos] = null;
        this.tileMap[absolutePosition] = piece;

        piece.setPosition(absolutePosition);

        return true;
    }

    @Override
    public boolean updateTileMap() {
        return super.updateTileMap();
    }
}

public class GameStateTests extends TestCase {
    public Player[] players;

    private DummyGameState game;

    @Override
    public void setUp() {
        this.players = new Player[] {
                new Player(1, "player1"), new Player(2, "player2"), new Player(3, "player3"), new Player(4, "player4"),
        };

        int i = 0;
        for (final Player p : this.players) {
            p.setPlayerColor(PlayerColor.values()[i++]);
        }

        this.game = new DummyGameState(this.players);
    }

    public void testInstantiatedGamePieces() {
        final Player[] players = this.game.getPlayers();

        for (final Player player : players) {
            for (int i = 0; i < 4; i++) {
                final GamePiece piece = player.getGamePiece(i);

                // The pieces and the player must be of the same color
                Assert.assertNotNull(piece);
                Assert.assertEquals(player.getPlayerColor(), piece.getPlayerColor());

                // Ensure that the tiles are spawned in the correct positions
                // (firstIndex + i)
                final int firstPos = GameTrail.getPlayerTrail(piece.getPlayerColor()).getFirstSpawnIndex();
                Assert.assertEquals(firstPos + i, piece.getPosition());
            }
        }
    }

    public void testNumberOfOccupiedTiles() {
        final GamePiece[] tileMap = this.game.getTileMap();

        // Count the number of tiles occupied in the tile map
        int occupied = 0;
        for (final GamePiece element : tileMap) {
            if (element != null) {
                occupied++;
            }
        }

        Assert.assertEquals(16, occupied);
    }

    public void testLegalDieRolls() {
        Assert.assertTrue(this.game.getNumDice() > 0);

        final int min = 1 * this.game.getNumDice();
        final int max = 6 * this.game.getNumDice();

        // Move the first green piece onto the board
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 0, 6));

        // Test outside the boundaries of what is legal.
        for (int i = min - 10; i < max + 10; i++) {
            final boolean expected = (i >= min && i <= max);
            Assert.assertEquals(expected, this.game.isMoveLegal(PlayerColor.GREEN, 0, i));
        }
    }

    public void testMapUpdateWhenMoving() {
        final int firstGreen = 16;

        final GamePiece[] tileMap = this.game.getTileMap();
        final Player player = this.game.getPlayers()[PlayerColor.GREEN.getValue()];

        // Ensure that the piece is standing at his spawn
        Assert.assertEquals(player.getGamePiece(0), tileMap[0]);
        Assert.assertNull(tileMap[firstGreen]);

        // Move the first piece of the green player. Since he moved from spawn,
        // his position should now
        // be 16.
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 0, 6));

        // Ensure that the tileMap is updated and that things are OK.
        Assert.assertEquals(firstGreen, player.getGamePiece(0).getPosition());
        Assert.assertEquals(player.getGamePiece(0), tileMap[firstGreen]);
        Assert.assertNull(tileMap[0]);

        // Move 4 positions, ensure that everything has updated neatly and tidy.
        // His position is now "20".
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 0, 4));
        Assert.assertEquals(firstGreen + 4, player.getGamePiece(0).getPosition());
        Assert.assertEquals(player.getGamePiece(0), tileMap[firstGreen + 4]);
        Assert.assertNull(tileMap[0]);
        Assert.assertNull(tileMap[firstGreen]);
    }

    public void testNeed6ToExitSpawn() {
        for (int c = 0; c < 4; c++) {
            for (int p = 0; p < 4; p++) {
                for (int i = 1; i <= 5; i++) {
                    final PlayerColor color = PlayerColor.values()[c];
                    Assert.assertFalse(this.game.isMoveLegal(color, p, i));
                    Assert.assertFalse(this.game.performMove(color, p, i));
                }
            }
        }
    }

    public void testValidityOfFirstMoveWhenBlocked() {
        final int firstGreen = 16;
        final Player player = this.game.getPlayers()[PlayerColor.GREEN.getValue()];
        final GamePiece[] tileMap = this.game.getTileMap();

        // Move the first piece of the green player onto the board
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 0, 6));
        Assert.assertEquals(player.getGamePiece(0), tileMap[firstGreen]);

        // Attempt to move the second piece. It should now be blocked because of
        // the first tile.
        Assert.assertFalse(this.game.performMove(PlayerColor.GREEN, 1, 6));
        Assert.assertEquals(player.getGamePiece(1), tileMap[1]);
        Assert.assertEquals(1, player.getGamePiece(1).getPosition());

        // Move the first tile forward to make room for the second tile.
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 0, 1));
        Assert.assertTrue(this.game.performMove(PlayerColor.GREEN, 1, 6));

        // Ensure that everything is in the right spot
        Assert.assertNull(tileMap[0]);
        Assert.assertNull(tileMap[1]);
        Assert.assertEquals(player.getGamePiece(0), tileMap[firstGreen + 1]);
        Assert.assertEquals(player.getGamePiece(1), tileMap[firstGreen]);
        Assert.assertEquals(firstGreen + 1, player.getGamePiece(0).getPosition());
        Assert.assertEquals(firstGreen, player.getGamePiece(1).getPosition());
    }

    public void testDone() {
        Assert.assertNull(this.game.getWinner());

        // Hack the system slightly: move all the green pieces to the final
        // trail
        final Player player = this.game.getPlayers()[PlayerColor.GREEN.getValue()];

        for (int i = 0; i < 4; i++) {
            final GamePiece piece = player.getGamePiece(i);
            final int pos = GameTrail.getPlayerTrail(PlayerColor.GREEN).getFinishIndex() - 1 - i;
            piece.setPosition(pos);
        }

        // Update the GameState to our new, modified positions.
        Assert.assertTrue(this.game.updateTileMap());

        // Move all the tiles past the goal.
        for (int i = 0; i < 4; i++) {
            Assert.assertTrue(this.game.performMove(player, i, 6));
            Assert.assertTrue(player.getGamePiece(i).isFinished());
        }

        Assert.assertTrue(player.hasFinishedGame());
        Assert.assertEquals(player, this.game.getWinner());

        // No moves should now be legal
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.GREEN, 0, 5));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.GREEN, 1, 5));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.GREEN, 2, 5));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.GREEN, 3, 5));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.BLUE, 0, 6));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.RED, 1, 4));
        Assert.assertFalse(this.game.isMoveLegal(PlayerColor.YELLOW, 3, 2));
    }

    public void testLastRemainingPlayerIsWinner() {
        final PlayerColor[] colors = {
                PlayerColor.GREEN, PlayerColor.RED, PlayerColor.BLUE, PlayerColor.YELLOW
        };

        for (int cur = 0; cur < 4; cur++) {
            this.players = new Player[] {
                    new Player(0, "player0"),
                    new Player(1, "player1"),
                    new Player(2, "player2"),
                    new Player(3, "player3"),
            };

            for (int i = 0; i < 4; i++) {
                this.players[i].setPlayerColor(colors[i]);
            }

            // Create a new game-state where colors[cur] will be the last
            // remaining player
            this.game = new DummyGameState(this.players);

            // Remove all players who are not colors[cur].
            int removed = 0;
            for (int i = 0; i < 4; i++) {
                if (i != cur) {
                    this.game.playerLeft(colors[i]);
                    removed++;

                    final Player winner = this.game.getWinner();
                    Assert.assertEquals(removed == 3, winner != null);

                    if (winner != null) {
                        Assert.assertEquals(colors[cur], winner.getPlayerColor());
                    }
                }
            }
        }
    }

    public void testCannotLandOnOwnPieces() {
        // Move the first green piece onto the second tile in the board
        final PlayerColor color = PlayerColor.GREEN;

        Assert.assertTrue(this.game.performMove(color, 0, 6));
        Assert.assertTrue(this.game.performMove(color, 0, 1));

        // Move the second piece onto the board
        Assert.assertTrue(this.game.performMove(color, 1, 6));

        // Ensure that moving one more tile (where the first piece is) is
        // illegal
        Assert.assertFalse(this.game.isMoveLegal(color, 1, 1));
        Assert.assertFalse(this.game.performMove(color, 1, 1));

        // Moving it two pieces however, is legal.
        Assert.assertTrue(this.game.isMoveLegal(color, 1, 2));
        Assert.assertTrue(this.game.performMove(color, 1, 2));
    }

    public void testToEnsureThatLandingOnEnemiesReturnsThemBackFromWhenceTheyCame() {
        final PlayerColor red = PlayerColor.RED;
        final PlayerColor green = PlayerColor.GREEN;

        // Move the first green tile onto the board
        Assert.assertTrue(this.game.performMove(green, 0, 6));

        final int position = this.game.getPlayers()[0].getGamePiece(0).getPosition();
        Assert.assertTrue(position >= GameTrail.FIRST_SHARED_TILE);

        // Alter the game state maliciously; the red piece hangs out at the
        // second
        // tile after the initial tile of the green player.
        final GamePiece redPiece = this.game.getPlayers()[red.getValue()].getGamePiece(0);
        final int spawnPos = redPiece.getPosition();

        Assert.assertTrue(this.game.forceMove(red, 0, position + 1));
        Assert.assertEquals(position + 1, redPiece.getPosition());
        Assert.assertEquals(this.game.getTileMap()[position + 1], redPiece);

        // If the green piece now moves onto the red piece, the red piece is
        // returned back home.
        Assert.assertTrue(this.game.isMoveLegal(green, 0, 1));
        Assert.assertTrue(this.game.performMove(green, 0, 1));

        // The red piece should now be at home.
        Assert.assertEquals(spawnPos, redPiece.getPosition());

        Assert.assertEquals(PlayerColor.GREEN, this.game.getTileMap()[position + 1].getPlayerColor());
    }
}
