package com.enderwolf.ludonet;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.shared.GameTrail;
import com.enderwolf.ludonet.shared.PlayerColor;

public class GameTrailTests extends TestCase {
    private static final int[] firstTiles = {
        16, 29, 42, 55
    };
    private static final PlayerColor[] colors = {
        PlayerColor.GREEN, PlayerColor.RED, PlayerColor.BLUE, PlayerColor.YELLOW
    };

    public void testFirstMove() {
        for (int i = 0; i < 4; i++) {
            final PlayerColor color = GameTrailTests.colors[i];
            final GameTrail.Trail trail = GameTrail.getPlayerTrail(color);

            // The initial move from the spawn tile should ALWAYS result in the
            // first shared spawn tile, regardless
            // of what the die roll showed.
            for (int toMove = 1; toMove < 6; toMove++) {
                for (int s = 0; s < 4; s++) {
                    final int spawn = trail.getFirstSpawnIndex() + s;
                    Assert.assertEquals(GameTrailTests.firstTiles[i], trail.getNextPosition(spawn, toMove));
                }
            }
        }
    }

    public void testWrapAround() {
        for (int i = 0; i < 4; i++) {
            final PlayerColor color = GameTrailTests.colors[i];
            final GameTrail.Trail trail = GameTrail.getPlayerTrail(color);
            Assert.assertEquals(GameTrail.FIRST_SHARED_TILE, trail.getNextPosition(GameTrail.LAST_SHARED_TILE, 1));
        }
    }

    public void testMoveToFinalTrail() {
        final int[] finalShared = {
                66, 27, 40, 53
        };
        final int[] firstFinal = {
                68, 74, 80, 86
        };

        for (int i = 0; i < 4; i++) {
            final PlayerColor color = GameTrailTests.colors[i];
            final GameTrail.Trail trail = GameTrail.getPlayerTrail(color);

            Assert.assertEquals(firstFinal[i], trail.getNextPosition(finalShared[i], 1));
        }
    }

    public void testFinalCaptures() {
        final int[] finalShared = {
                66, 27, 40, 53
        };
        final int[] goals = {
                73, 79, 85, 91
        };

        for (int i = 0; i < 4; i++) {
            final PlayerColor color = GameTrailTests.colors[i];
            final GameTrail.Trail trail = GameTrail.getPlayerTrail(color);

            // Move from the final shared tile and 20 tiles - the goal should be
            // the destination, as it captures.
            Assert.assertEquals(goals[i], trail.getNextPosition(finalShared[i], 20));

            // Move 1 tile when standing in the goal. The position should be
            // unchanged.
            Assert.assertEquals(goals[i], trail.getNextPosition(goals[i], 1));

            // Move 2 tiles when standing in the tile before the goal
            Assert.assertEquals(goals[i], trail.getNextPosition(goals[i] - 1, 2));

            // Move 100 tiles from each of the first shared tiles. The goal
            // should be the final destination.
            final int first = trail.getNextPosition(trail.getFirstSpawnIndex(), 1);
            Assert.assertEquals(goals[i], trail.getNextPosition(first, 100));
        }
    }
}
