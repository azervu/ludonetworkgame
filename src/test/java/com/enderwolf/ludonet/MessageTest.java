package com.enderwolf.ludonet;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;

public class MessageTest extends TestCase {
    public MessageTest(String name) {
        super(name);

    }

    @Override
    protected void setUp() throws Exception {

        super.setUp();
    }

    public void testMessageTranslation() {

        final Message chatA = new Message(MessageType.CHAT);
        chatA.addEntry(EntryType.CHAT_TEXT, "#", true);
        final Message chatB = Message.createMessage(chatA.toString());
        Assert.assertEquals(chatA.toString(), chatB.toString());
        Assert.assertEquals(chatA.getEntry(EntryType.CHAT_TEXT), chatB.getEntry(EntryType.CHAT_TEXT));

        final Message testA = new Message(MessageType.CHAT);
        final String[] textData = new String[10];
        textData[4] = ",:\t\n";
        textData[6] = "#################";
        textData[8] = "###n#r#c#s#t####";

        testA.addArray(EntryType.CHAT_TEXT, textData, true);
        final Message testB = Message.createMessage(testA.toString());
        Assert.assertEquals(testA.toString(), testB.toString());

        final Message loginA = new Message(MessageType.LOGIN, "testUser");
        chatA.addEntry(EntryType.PASSWORD, "ASmallCatAteAHat", true);
        Assert.assertEquals("MESSAGE_TYPE:LOGIN	SENDER:testUser:ESCAPE	\n", loginA.toString());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
