package com.enderwolf.ludonet;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.shared.Scoreboard;

public class ScoreboardTests extends TestCase {
    public void testInvalidFormats() {
        final String[] formats = {
                "apple;user,59;",
                "15;",
                "15;,59",
                "15;;,59;",
                "15;user,59",
                "-1;user,59;",
                "15;user,59;;",
                "15;user,59;,",
                "15;user,59;,59;",
        };

        for (final String f : formats) {
            System.out.println("Testing " + f);
            Assert.assertFalse(this.validFormat(f));
        }
    }

    public void testValidFormats() {
        final String[] formats = {
                "15;user,59;", "0;0,0;", "1;hurp,1;", "15;user,59;user,59;", "15;user,59;user,59;",
        };

        for (final String f : formats) {
            System.out.println("Testing format: " + f);
            Assert.assertTrue(this.validFormat(f));
        }
    }

    public void testLowerIndexMerging() {
        Scoreboard sbA;
        Scoreboard sbB;

        try {
            sbA = new Scoreboard("2;entry2,2;entry3,3;");
            sbB = new Scoreboard("0;entry0,0;entry1,1;");
        } catch (final Scoreboard.DeserializeException dse) {
            Assert.fail("Scoreboard deserialization failed");
            return;
        }

        // Verify the indices and sizes of the two scoreboards.
        Assert.assertEquals(2, sbA.getFirstIndex());
        Assert.assertEquals(2, sbA.getSize());
        Assert.assertEquals(0, sbB.getFirstIndex());
        Assert.assertEquals(2, sbB.getSize());

        // Verify the contents of the two scoreboards.
        this.verifyContents(sbA);
        this.verifyContents(sbB);

        // Append sbB into sbA. The merge should result in sbA having a
        // first-index of 0 and a size of 4. sbB should
        // remain untouched by the merge.
        sbA.mergeScoreboard(sbB);

        // Re-verify the indices and sizes of the two scoreboards.
        Assert.assertEquals(0, sbA.getFirstIndex());
        Assert.assertEquals(4, sbA.getSize());
        Assert.assertEquals(0, sbB.getFirstIndex());
        Assert.assertEquals(2, sbB.getSize());

        // Re-verify the contents of the two scoreboards.
        this.verifyContents(sbA);
        this.verifyContents(sbB);
    }

    public void testHigherIndexMerging() {
        Scoreboard sbA = null;
        Scoreboard sbB = null;

        try {
            sbA = new Scoreboard("2;entry2,2;entry3,3;");
            sbB = new Scoreboard("0;entry0,0;entry1,1;");
        } catch (final Scoreboard.DeserializeException dse) {
            Assert.fail("Scoreboard deserialization failed");
        }

        // Verify the indices and sizes of the two scoreboards.
        Assert.assertEquals(2, sbA.getFirstIndex());
        Assert.assertEquals(2, sbA.getSize());
        Assert.assertEquals(0, sbB.getFirstIndex());
        Assert.assertEquals(2, sbB.getSize());

        // Verify the contents of the two scoreboards.
        this.verifyContents(sbA);
        this.verifyContents(sbB);

        sbB.mergeScoreboard(sbA);

        // Re-verify the indices and sizes of the two scoreboards.
        Assert.assertEquals(0, sbB.getFirstIndex());
        Assert.assertEquals(4, sbB.getSize());
        Assert.assertEquals(2, sbA.getFirstIndex());
        Assert.assertEquals(2, sbA.getSize());

        // Re-verify the contents of the two scoreboards.
        this.verifyContents(sbA);
        this.verifyContents(sbB);
    }

    public void testFromStringToString() {
        final String original = "0;entry0,0;entry1,1;entry2,2;";
        Scoreboard scoreboard = null;

        try {
            scoreboard = new Scoreboard(original);
        } catch (final Scoreboard.DeserializeException dse) {
            Assert.fail("Unable to deserialize Scoreboard");
        }

        final String serialized = scoreboard.serializeToString();
        Assert.assertEquals(original, serialized);
    }

    public void testFromListToString() {
        final List<Scoreboard.Entry> entries = new ArrayList<>();
        entries.add(new Scoreboard.Entry("entry1", 1));
        entries.add(new Scoreboard.Entry("entry2", 2));
        entries.add(new Scoreboard.Entry("entry3", 3));

        final String expSerialized = "1;entry1,1;entry2,2;entry3,3;";

        final Scoreboard scoreboard = new Scoreboard(entries, 1);

        Assert.assertEquals(expSerialized, scoreboard.serializeToString());
    }

    /**
     * Check if a serialized scoreboard string is valid. A Scoreboard instance
     * is attempted created with the string. The validity of the serialized
     * string is based on whether or not an exception is thrown by the ctor.
     *
     * @param serialized
     *            Potentially valid String-serialized Scoreboard.
     * @return True if the string is valid, false if it is invalid.
     */
    private boolean validFormat(String serialized) {
        boolean valid = true;

        try {
            new Scoreboard(serialized);
        } catch (final Scoreboard.DeserializeException dse) {
            valid = false;
        }

        return valid;
    }

    /**
     * Verify the contents of scoreboard. The verification assumes that the
     * absolute scoreboard contains: { entry0,0;entry1,1; ..., entryN,N; }
     *
     * Fragmentation inside the Scoreboard is accepted, and does not cause the
     * verification to fail under any circumstances.
     *
     * @param scoreboard
     *            The scoreboard to veirfy.
     */
    private void verifyContents(Scoreboard scoreboard) {
        final int fi = scoreboard.getFirstIndex();

        for (int i = 0; i < scoreboard.getSize(); i++) {
            final Scoreboard.Entry entry = scoreboard.getEntries().get(i);
            if (entry != null) {
                final int absIndex = fi + i;

                Assert.assertEquals("entry" + absIndex, entry.getName());
                Assert.assertEquals(absIndex, entry.getScore());
            }
        }
    }
}
