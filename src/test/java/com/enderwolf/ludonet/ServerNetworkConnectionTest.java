package com.enderwolf.ludonet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.server.ServerNetworkConnection;
import com.enderwolf.ludonet.shared.NetworkConnection.ConnectionStatus;

import debug.TestDatabase;

public class ServerNetworkConnectionTest extends TestCase {

    static Logger LOG = Logger.getGlobal();

    ServerNetworkConnection serverTestConnection;

    Socket clientEmulatorConnection;
    BufferedWriter serverWriter;
    BufferedReader serverReader;

    @Override
    public void setUp() throws IOException, InterruptedException {
        // Clear the real database
        new TestDatabase(TestDatabase.DatabaseType.DB_REAL).clearDatabase();

        System.setProperty("javax.net.ssl.keyStore", "mySrvKeystore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");
        System.setProperty("javax.net.ssl.trustStore", "mySrvKeystore");

        ServerSocket serverSocket;
        Socket socket;

        serverSocket = ServerSocketFactory.getDefault().createServerSocket(23456);
        this.clientEmulatorConnection = SocketFactory.getDefault().createSocket(InetAddress.getLocalHost(), 23456);
        socket = serverSocket.accept();
        serverSocket.close();

        final Thread ServerVerificationThread = new Thread(() -> {
            try {
                this.serverTestConnection = new ServerNetworkConnection(socket);
                while (this.serverTestConnection.updateLogin() != ConnectionStatus.CONNECTION_ESTABLISHED) {
                    ServerNetworkConnectionTest.LOG.log(Level.INFO, this.serverTestConnection.getStatus().toString());
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }

        });
        ServerVerificationThread.start();

        final SSLSocket clientSSLSocket = (SSLSocket) ((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket(
                this.clientEmulatorConnection, null, this.clientEmulatorConnection.getPort(), false);
        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSSLSocket.getOutputStream()));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(clientSSLSocket.getInputStream()));
        Message msg;
        msg = new Message(MessageType.LOGIN, "testUser");
        msg.addEntry(EntryType.PASSWORD, "testPassword", true);
        msg.addEntry(EntryType.CREATE_USER, String.valueOf(true), false);
        writer.write(msg.toString());
        writer.write("\n");
        writer.flush();
        final Message m = Message.createMessage(reader.readLine());
        Assert.assertEquals("CONNECTION_STATUS", m.getEntry(EntryType.MESSAGE_TYPE));
        Assert.assertEquals("CONNECTION_ESTABLISHED", m.getEntry(EntryType.CONNECTION_STATUS));
        clientSSLSocket.close();
        ServerVerificationThread.join();

    }

    public void testSendChatMessage() throws IOException {

        final BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.clientEmulatorConnection.getInputStream()));

        final Message msg = new Message(MessageType.CHAT);
        msg.addEntry(EntryType.CHAT_TEXT, "hello", true);
        this.serverTestConnection.sendMessage(msg, false);

        final Message m = Message.createMessage(reader.readLine());
        Assert.assertEquals("hello", m.getEntry(EntryType.CHAT_TEXT));
    }

    public void testReceiveChatMessage() throws IOException {

        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                this.clientEmulatorConnection.getOutputStream()));
        final Message cMessage = new Message(MessageType.CHAT, "testUser");
        cMessage.addEntry(EntryType.CHAT_TEXT, "hello", true);
        writer.write(cMessage.toString());
        writer.write("\n");
        writer.flush();

        Message message = null;
        while (message == null) {
            message = this.serverTestConnection.getNextMessage();
        }

        Assert.assertEquals((cMessage.toString()), message.toString());
    }

    @Override
    public void tearDown() throws IOException {
        this.clientEmulatorConnection.close();
        this.serverTestConnection.close();
    }

}
