package debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.enderwolf.ludonet.client.ClientNetworkConnection;
import com.enderwolf.ludonet.message.Message;
import com.enderwolf.ludonet.message.Message.EntryType;
import com.enderwolf.ludonet.message.Message.MessageType;
import com.enderwolf.ludonet.shared.NetworkConnection.ConnectionStatus;

@SuppressWarnings("serial")
public class DebugClient extends JFrame {
    private static Logger LOG = Logger.getGlobal();

    ClientNetworkConnection cnc;
    JTextArea status;

    public DebugClient() throws IOException {

        super("Client");
        DebugClient.LOG.setLevel(Level.INFO);
        this.status = new JTextArea();
        this.add(new JScrollPane(this.status));
        this.setSize(400, 500);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // JEditorPane pane = new JEditorPane();
        try {

            final InputStreamReader streamReader = new InputStreamReader(this.getClass().getResourceAsStream(
                    "/com/enderwolf/ludonet/shared/i18n/rules.html"), "UTF16");
            final BufferedReader bufferedReader = new BufferedReader(streamReader);
            String rules = "";
            String tmp = null;
            while ((tmp = bufferedReader.readLine()) != null) {
                rules += tmp;
            }
            bufferedReader.close();

            JOptionPane.showMessageDialog(null, rules);

        } catch (final Exception e1) {
            e1.printStackTrace();
        }

        this.cnc = new ClientNetworkConnection(InetAddress.getLocalHost());
        this.cnc.login("testUser", "testPassword", false);
        while (this.cnc.updateLogin() != ConnectionStatus.CONNECTION_ESTABLISHED) {
        }

        final Message msg = new Message(MessageType.CHAT);
        msg.addEntry(EntryType.CHAT_TEXT, "hello", true);
        this.cnc.sendMessage(msg, true);

    }

    public static void main(String[] args) {
        try {
            new DebugClient();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}