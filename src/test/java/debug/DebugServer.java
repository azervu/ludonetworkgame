package debug;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.enderwolf.ludonet.server.ServerNetworkHandler;

@SuppressWarnings("serial")
public class DebugServer extends JFrame {
    private static final Logger LOG = Logger.getGlobal();

    ServerNetworkHandler snh;
    JTextArea status;

    public DebugServer() throws IOException {
        super("Authentication Test Server");
        DebugServer.LOG.setLevel(Level.INFO);
        this.status = new JTextArea();
        this.add(new JScrollPane(this.status));
        this.setSize(400, 500);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /*
         * System.setProperty("javax.net.ssl.keyStore", "mySrvKeystore");
         * System.setProperty("javax.net.ssl.keyStorePassword", "123456");
         *
         * ServerSocket serverSocket; Socket socket;
         *
         * serverSocket =
         * ServerSocketFactory.getDefault().createServerSocket(23456); socket =
         * serverSocket.accept();
         *
         * SSLSocket loginSocket; loginSocket = (SSLSocket) ((SSLSocketFactory)
         * SSLSocketFactory.getDefault()).createSocket(socket,
         * null,socket.getPort(), false); loginSocket.setUseClientMode(false);
         * BufferedReader loginReader = new BufferedReader(new
         * InputStreamReader(loginSocket.getInputStream()));
         *
         * status.append(loginReader.readLine()+"\n"); loginSocket.close();
         * loginReader.close();
         *
         * BufferedReader reader = new BufferedReader(new
         * InputStreamReader(socket.getInputStream()));
         * status.append(reader.readLine()+"\n");
         * status.append(reader.readLine()+"\n");
         *
         * snh = new ServerNetworkHandler();
         * status.append("Server Initialised\n"); while(true) { Message m; m =
         * snh.getMessage(); if(m != null) { status.append(m.toMessageString());
         * } }
         */
    }

    public static void main(String[] args) {
        try {
            new DebugServer();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}