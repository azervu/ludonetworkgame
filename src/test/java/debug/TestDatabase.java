package debug;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.enderwolf.ludonet.server.Database;

/**
 * Database used for test-purposes. The database is identical to
 * *.ludonet.server.Database, but offers an additional interface for clearing
 * all contents of the database. The actual data stored in the TestDatabase is
 * 100% non-overlapping with that stored in a Database instance.
 */
public class TestDatabase extends Database {
    private static final String TEST_DB_NAME = "TestLudoDB";
    private static final String REAL_DB_NAME = Database.DB_NAME;

    public enum DatabaseType {
        DB_REAL,
        DB_TEST,
    }

    String dbName;

    public TestDatabase() {
        this(DatabaseType.DB_TEST);
    }

    public TestDatabase(DatabaseType type) {
        super(false);

        if (type == DatabaseType.DB_TEST) {
            this.dbName = TestDatabase.TEST_DB_NAME;
        } else {
            this.dbName = TestDatabase.REAL_DB_NAME;
        }

        this.initialize();
    }

    /**
     * Attempt to delete all rows in the database.
     *
     * @return True on success, false upon error.
     */
    public boolean clearDatabase() {
        this.getDatabaseMutex().lock();
        boolean success = false;

        this.clearCache();

        try {
            final Connection con = this.connectToDatabase();
            final Statement stmt = con.createStatement();

            /* This order of deletion SHOULD always work */
            final String[] queries = new String[] {
                    "DELETE FROM ChatMessage",
                    "DELETE FROM ChatRoom WHERE id != 1",
                    "DELETE FROM Friends",
                    "DELETE FROM Player",
            };

            for (final String query : queries) {
                stmt.execute(query);
            }

            con.close();

            success = true;
        } catch (final SQLException sqle) {
            Logger.getGlobal().log(Level.SEVERE, "SQLException in TestDatabase#clearDatabase()", sqle);
        }

        this.getDatabaseMutex().unlock();
        return success;
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    protected String getDatabaseName() {
        return this.dbName;
    }
}
